import sys
import time
import logging
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

class CustomFileSystemEventHandler(FileSystemEventHandler):

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)

    def on_any_event(self, event):
        print("Something happed")
if __name__ == "__main__":
    
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    path = r"C:/Users/sanam/IADS/hot_folder"
    event_handler = CustomFileSystemEventHandler()
    observer = Observer()
    observer.schedule(event_handler, path, recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()