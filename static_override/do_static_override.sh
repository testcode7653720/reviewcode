#!/bin/bash

cd "$(dirname "$0")"
echo ${PWD}

cp airflow-override.css ../static/
cp airflow-override.js ../static/

cp main.html ../venv/lib/python3.10/site-packages/airflow/www/templates/airflow/
