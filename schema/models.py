
from django.contrib.gis.db import models
from django.contrib.postgres.fields import ArrayField


class BaseModel(models.Model):

    CreatedOn = models.DateTimeField(auto_now_add=True)
    CreatedBy = models.CharField(max_length=50, blank=True, null=True, default='NA')
    ModifiedOn = models.DateTimeField(auto_now=True)
    ModifiedBy = models.CharField(max_length=50, blank=True, null=True, default='NA')

    class Meta:
        abstract = True


class SourceData(BaseModel):
    """Source Data Information"""

    IADSCoreStatusChoices = [
        ("partial", "Partial"), ("ready", "Ready For Ingestion"), ("ingested", "Ingested")
    ]

    SourceDataID = models.IntegerField(primary_key=True)
    SourceDatasetName = models.CharField(max_length=255)
    SourceDatasetVersion = models.CharField(max_length=50, blank=True, null=True)
    SourceDataLocation = models.CharField(max_length=255)
    DataProviderOrSource = models.CharField(max_length=50, blank=True, null=True)
    DataSchemaOrStructure = models.CharField(max_length=50, blank=True, null=True)
    DataVolume = models.CharField(max_length=50, blank=True, null=True)
    DataFrequency = models.CharField(max_length=50, blank=True, null=True)
    DataExtractionDateTime = models.DateTimeField(blank=True, null=True)
    DataOwnership = models.CharField(max_length=50, blank=True, null=True)
    DataLicensing = models.CharField(max_length=50, blank=True, null=True)
    IngestionType = models.CharField(max_length=50, blank=True, null=True, choices=[
        ("manual", "Manual"), ("automated", "Automated")
    ])
    SourceType = models.CharField(max_length=50, blank=True, null=True, choices=[
        ("image", "Image"), ("satellite", "Satellite")
    ])
    IADSCoreStatus = models.CharField(max_length=50, blank=True, null=True, choices=IADSCoreStatusChoices)

    class Meta:
        db_table = 'SourceData'


class TargetData(BaseModel):
    """Target Data Information"""

    IADSCoreStatusChoices = [
        ("partial", "Partial"), ("ready", "Ready For Ingestion"), ("ingested", "Ingested")
    ]

    TargetDataID = models.IntegerField(primary_key=True)
    SourceDataID = models.OneToOneField(
        SourceData,
        on_delete=models.CASCADE
    )
    TargetDatasetName = models.CharField(max_length=255, blank=True, null=True)
    TargetDataLocation = models.CharField(max_length=255, blank=True, null=True)
    DataSchemaOrStructure = models.CharField(max_length=50, blank=True, null=True)
    DataVolume = models.CharField(max_length=50, blank=True, null=True)
    DataStorageFormat = models.CharField(max_length=50, blank=True, null=True)
    DataRetentionPolicy = models.CharField(max_length=50, blank=True, null=True)
    DataQualityProfile = models.CharField(max_length=50, blank=True, null=True)
    DataSecurityMeasures = models.CharField(max_length=50, blank=True, null=True)

    IADSCoreStatus = models.CharField(max_length=50, blank=True, null=True, choices=IADSCoreStatusChoices)
    MnAStatus = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        db_table = 'TargetData'


class ImageBands(BaseModel):
    """Band information for individual images"""

    BandID = models.IntegerField(primary_key=True)
    TargetDataID = models.ForeignKey(TargetData, on_delete=models.CASCADE)
    SpatialResolution = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        db_table = 'ImageBands'


class BusinessMetadata(BaseModel):

    BusinessMetadataID = models.IntegerField(primary_key=True)
    TargetDataID = models.ForeignKey(TargetData, on_delete=models.CASCADE)

    Extent = models.PolygonField()
    Keywords = ArrayField(models.CharField(max_length=50, blank=True))
    Type = models.CharField(max_length=50, blank=True, null=True)
    Thesaurus = models.CharField(max_length=50, blank=True, null=True)
    ImagePlatforms = models.CharField(max_length=50, blank=True, null=True)
    Organization = models.CharField(max_length=50, blank=True, null=True)
    CRSCoordinateReference = models.CharField(max_length=50, blank=True, null=True)
    HorizontalDatum = models.CharField(max_length=50, blank=True, null=True)
    VerticalDatum = models.CharField(max_length=50, blank=True, null=True)
    SensorType = models.CharField(max_length=50, blank=True, null=True)
    AcquisitionDate = models.CharField(max_length=50, blank=True, null=True)
    SourceName = models.CharField(max_length=50, blank=True, null=True)
    ProductType = models.CharField(max_length=50, blank=True, null=True)
    ProcessingLevel = models.CharField(max_length=50, blank=True, null=True)
    Custodian = models.CharField(max_length=50, blank=True, null=True)
    LicensingLevel = models.CharField(max_length=50, blank=True, null=True)
    Date = models.DateTimeField(blank=True, null=True)
    TileID = models.CharField(max_length=50, blank=True, null=True)
    DownloadURL = models.CharField(max_length=50, blank=True, null=True)
    VisualizationURL = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        db_table = 'BusinessMetadata'


class OperationalMetadata(BaseModel):

    BusinessMetadataID = models.IntegerField(primary_key=True)
    TargetDataID = models.ForeignKey(TargetData, on_delete=models.CASCADE)

    class Meta:
        db_table = 'OperationalMetadata'


class IngestionConfiguration(BaseModel):
    """Ingestion Configuration"""

    ConfigurationID = models.IntegerField(primary_key=True)
    ParameterName = models.CharField(max_length=50, blank=True, null=True)
    ParameterType = models.CharField(max_length=50, blank=True, null=True)
    ParameterValue = models.CharField(max_length=50, blank=True, null=True)
    ParameterDescription = models.CharField(max_length=50, blank=True, null=True)
    ParameterCategory = models.CharField(max_length=50, blank=True, null=True)
    AffectedSourceID = models.ForeignKey(SourceData, on_delete=models.CASCADE, blank=True, null=True)
    AffectedTargetID = models.ForeignKey(TargetData, on_delete=models.CASCADE, blank=True, null=True)
    DataType = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        db_table = 'IngestionConfiguration'


class IngestionLogs(BaseModel):
    """Ingestion Logs and Metrics"""

    LogID = models.IntegerField(primary_key=True)
    Timestamp = models.CharField(max_length=50, blank=True, null=True)
    LogType = models.CharField(max_length=50, blank=True, null=True)
    LogMessage = models.CharField(max_length=50, blank=True, null=True)
    AffectedSourceID = models.ForeignKey(SourceData, on_delete=models.CASCADE, blank=True, null=True)
    AffectedTargetID = models.ForeignKey(TargetData, on_delete=models.CASCADE, blank=True, null=True)
    ResourceUtilization = models.CharField(max_length=50, blank=True, null=True)
    DataVolume = models.CharField(max_length=50, blank=True, null=True)
    DataThroughput = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        db_table = 'IngestionLogs'


class MnAOperationInfo(BaseModel):
    """MnA Operational Information
    For Saving the Clipped Request
    """

    MnAOpID = models.IntegerField(primary_key=True)
    TargetDataID = models.ForeignKey(TargetData, on_delete=models.CASCADE)
    TargetDatasetName = models.CharField(max_length=50, blank=True, null=True)
    TargetDataLocation = models.CharField(max_length=50, blank=True, null=True)
    DataSchemaOrStructure = models.CharField(max_length=50, blank=True, null=True)
    DataVolume = models.CharField(max_length=50, blank=True, null=True)
    DataStorageFormat = models.CharField(max_length=50, blank=True, null=True)
    MnAID = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        db_table = 'MnAOperationInfo'


class NewCollectionInformation(BaseModel):
    """New Collection Information"""

    TargetDataID = models.ForeignKey(TargetData, on_delete=models.CASCADE)

    class Meta:
        db_table = 'NewCollectionInformation'


class SatelliteProviderConfiguration(BaseModel):
    """Satellite Provider Configuration"""
    class Meta:
        db_table = 'SatelliteProviderConfiguration'

