import GeoTIFF from 'ol/source/GeoTIFF.js';
import Map from 'ol/Map.js';
import TileLayer from 'ol/layer/WebGLTile.js';
import OSM from 'ol/source/OSM.js';
import Tile from 'ol/layer/Tile.js';
import XYZ from 'ol/source/XYZ.js';
import View from 'ol/View.js';
import { get } from 'ol/proj';
import $ from "jquery";

export default function HomePage() {

    var fixURL;
    fixURL = 'https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/49/T/FE/2023/9/S2A_49TFE_20230927_0_L2A/TCI.tif'
    fixURL = 'https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/36/Q/WD/2020/7/S2A_36QWD_20200701_0_L2A/TCI.tif'
    // fixURL = 'https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/34/P/GC/2023/9/S2A_34PGC_20230926_0_L2A/TCI.tif'
    fixURL = 'https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/21/L/TC/2023/9/S2A_21LTC_20230926_0_L2A/TCI.tif'


    $('#showImageBtn').on('click', function (event) {
        const selectedPath = $('#imageURLInput').val();
        if (selectedPath) {
            $('#map').empty();
            showMap(selectedPath);
        } else {
            alert("Please select a file path.");
        }
    });



    function showMap(imageURL) {
        const source = new GeoTIFF({
            sources: [{
                url: imageURL,
            }],
        });

        // var worldImagery = new Tile({
        //     source: new XYZ({
        //         url: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
        //         maxZoom: 19
        //     })
        // })

        const map = new Map({
            target: 'map',
            layers: [
                // worldImagery,
                new TileLayer({ source: new OSM(), }),
                new TileLayer({ source: source, }),
            ],
            view: source.getView(),
        });
    }

    function showInitialMap(){
        const map = new Map({
            target: 'map',
            layers: [
                new TileLayer({ source: new OSM() }),
            ],
            view: new View({
                center: [8220350,2091308],
                zoom: 7
              }),
        });
    }

    showInitialMap()

}















