from django.core.management.base import BaseCommand

from CostEstimator.services import CostEstimatorService
from IngestionEngine.models import TargetImage


class Command(BaseCommand):
    help = "Closes the specified poll for voting"

    def handle(self, *args, **options):

        image = TargetImage.objects.get(ID=30)

        service = CostEstimatorService()
        cost = service.calculate_cost_for_image(target_image=image)
        print("Cost", cost)
        return
