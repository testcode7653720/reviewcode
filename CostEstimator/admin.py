from django.contrib import admin
from .models import ImageCost
from .models import ImageCost, CostFactor


class CostFactorsAdmin(admin.ModelAdmin):
    list_display = (
        "FactorType",
        "FactorName",
        "FixedCost",
        "PerKmCost",
    )


admin.site.register(ImageCost)
admin.site.register(CostFactor, CostFactorsAdmin)
