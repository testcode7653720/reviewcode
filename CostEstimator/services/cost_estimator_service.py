from IngestionEngine.models import TargetImage
from CostEstimator.models import CostFactor
from django.contrib.gis.geos import Polygon
import rasterio
from rasterio.warp import transform_bounds


def log(x):
    print(x)


class CostEstimatorService:

    def calculate_cost_for_image(self, target_image: TargetImage, extent: Polygon = None) -> float:

        with rasterio.open(target_image.Path) as dataset:

            # Transform the bounds to EPSG 3857 (Web Mercator)
            bounds_3857 = transform_bounds(dataset.crs, 'EPSG:3857', *dataset.bounds)

            # Calculate the pixel width in EPSG 3857
            pixel_width_3857 = (bounds_3857[2] - bounds_3857[0]) / dataset.width

        print(f"Distance in EPSG 3857 (meters): {pixel_width_3857:.2f}")

        if extent is None:
            extent = target_image.TargetData.BusinessMetadata.Extent
        extent.transform(3857)
        area_in_km2 = extent.area/(1000*1000)

        band = target_image.BandName
        resolution = 15.1

        return self.calculate_cost(area_in_km2=area_in_km2, band=band, resolution=resolution)

    def calculate_cost(self, area_in_km2: int, band: str, resolution: float) -> float:

        total_cost = 0.0

        # Calculate cost for a specific resolution
        resolutions = CostFactor.objects.filter(
            FactorType=CostFactor.COST_FACTOR_RESOLUTION).values_list('FactorName', flat=True)
        log(f"Found resolutions: {list(resolutions)}")
        if resolutions:
            nearest_resolution = min(resolutions, key=lambda x: abs(int(x) - resolution))
            log(f"Nearest resolution: {nearest_resolution}")

            resolution_cost_factor = CostFactor.objects.get(
                FactorType=CostFactor.COST_FACTOR_RESOLUTION, FactorName=str(nearest_resolution))
            additional_cost = resolution_cost_factor.FixedCost + resolution_cost_factor.PerKmCost * area_in_km2
            total_cost += additional_cost
            log(f"Found {resolution_cost_factor.get_FactorType_display()}, value: {resolution_cost_factor.FactorName}," +
                " cost: {additional_cost}, Total cost: {total_cost}")

        for cost_factor in CostFactor.objects.all():
            if cost_factor.FactorType == CostFactor.COST_FACTOR_BAND and cost_factor.FactorName == band:
                additional_cost = cost_factor.FixedCost + cost_factor.PerKmCost * area_in_km2
                total_cost += additional_cost
                log(f"Adding {cost_factor.get_FactorType_display()}, value: {cost_factor.FactorName}," +
                    " cost: {additional_cost}, Total cost: {total_cost}")
            elif cost_factor.FactorType == CostFactor.COST_FACTOR_ADDITIONAL1:
                additional_cost = cost_factor.FixedCost + cost_factor.PerKmCost * area_in_km2
                total_cost += additional_cost
                log(f"Adding {cost_factor.get_FactorType_display()}, value: {cost_factor.FactorName}," +
                    " cost: {additional_cost}, Total cost: {total_cost}")
            elif cost_factor.FactorType == CostFactor.COST_FACTOR_ADDITIONAL2:
                additional_cost = cost_factor.FixedCost + cost_factor.PerKmCost * area_in_km2
                total_cost += additional_cost
                log(f"Adding {cost_factor.get_FactorType_display()}, value: {cost_factor.FactorName}," +
                    " cost: {additional_cost}, Total cost: {total_cost}")

        return round(total_cost, 2)
