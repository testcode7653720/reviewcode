from django.db import models

from CostEstimator.exceptions import CostEstimationException


class ImageCost(models.Model):

    BandName = models.TextField(blank=True, null=True)
    BandBaseCost = models.FloatField(blank=True, null=True)
    Resolution = models.TextField(blank=True, null=True)
    Unit = models.TextField(blank=True, null=True)
    ResolutionPerUnitCost = models.TextField(blank=True, null=True)
    BasePrice = models.TextField(blank=True, null=True)
    AdditionalCostFactors = models.TextField(blank=True, null=True)


class CostFactor(models.Model):
    """
    Example:
    Type	        Name	Fixed Cost	Per km cost
    Band	        Red	    1000	    150
    Band	        NIR	    2000	    200
    Resolution	    10	    1000
    Resolution	    20	    2000
    Additional 1
    Additional 2

    """

    COST_FACTOR_BAND = 'band'
    COST_FACTOR_RESOLUTION = 'resolution'
    COST_FACTOR_ADDITIONAL1 = 'additional1'
    COST_FACTOR_ADDITIONAL2 = 'additional2'
    COST_FACTOR_CHOICES = (
        (COST_FACTOR_BAND, "Band"),
        (COST_FACTOR_RESOLUTION, "Resolution"),
        (COST_FACTOR_ADDITIONAL1, "Additional 1"),
        (COST_FACTOR_ADDITIONAL2, "Additional 2"),
    )

    FactorType = models.CharField(max_length=20, choices=COST_FACTOR_CHOICES)
    FactorName = models.TextField()
    FixedCost = models.FloatField(default=0.0)
    PerKmCost = models.FloatField(default=0.0)

    def save(self, *args, **kwargs):
        if self.FactorType == self.COST_FACTOR_RESOLUTION:
            try:
                float(self.FactorName)
            except ValueError:
                raise CostEstimationException(f"Resolution needs to be a int or float value, not {self.FactorName}")
        return super().save(*args, **kwargs)
