start-celery:
	celery -A IADS worker -l info -P gevent

start:
	gunicorn IADS.wsgi -b 0.0.0.0:8000 --daemon

stop:
	pkill gunicorn

watch:
	npx vite build --watch

build:
	npm run build

watcher:
	python manage.py file_watcher

restart:
	sudo service airflow-webserver-offline stop
	sudo service airflow-scheduler-offline stop
	pkill gunicorn
	sudo service airflow-webserver-offline start
	sudo service airflow-scheduler-offline start
	gunicorn IADS.wsgi -b 0.0.0.0:8000 --daemon
