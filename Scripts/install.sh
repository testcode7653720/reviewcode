#!/bin/bash
WEBAPP_FOLDER_NAME=FGICWebApp
DATABASE_URL=postgres://coderize:coderize@172.31.0.93:5432/iads
AIRFLOW_DATABASE_URL="postgresql+psycopg2://coderize:coderize@172.31.0.93/iads"

# Check if running with root privileges
if [ "$(id -u)" != "0" ]; then
    echo "This script requires sudo access."
    sudo bash "$0" "$@"  # Re-run this script with sudo
    exit $?
fi

export DEBIAN_FRONTEND=noninteractive

# Check python version
PYTHON_VERSION="$(python3 --version | cut -d " " -f 2 | cut -d "." -f 1-2)"
REQUIRED_PYTHON_VERSION="3.10"

if [[ "$PYTHON_VERSION" == "$REQUIRED_PYTHON_VERSION" ]]
then
   echo -e "\e[32m Python version $REQUIRED_PYTHON_VERSION found \e[0m"
else
   echo -e "\e[31m Python version $REQUIRED_PYTHON_VERSION not found \e[0m"
   exit
fi


# Set up some env variables
echo "Setting up some env variables"
AIRFLOW_HOME=./Airflow

# Install GDAL
echo "Enabling GDAL Repository"
sudo add-apt-repository ppa:ubuntugis/ppa -y
sudo apt-get update

# Beyond this point, if the installation failes, script will be aborted
# set -e 

sudo apt-get -qq install gdal-bin python3-dev libpq-dev libgdal-dev git python3.10-venv -y
sudo apt install python3-dev build-essential libssl-dev -y
ogrinfo --version
if [ $? -eq 0 ]; then
    echo -e "\e[32mGDAL Installation succeeded. \e[0m"
else
    echo -e "\e[31mGDAL Installation failed. \e[0m"
    exit
fi

# Install postgis
echo "Installing PostGIS"

sudo apt-get install postgresql postgis -y
if [ $? -eq 0 ]; then
    echo -e "\e[32mPostGIS Installation succeeded. \e[0m"
else
    echo -e "\e[31mPostGIS Installation failed. \e[0m"
    exit
fi

# Clone the repo
rm -rf "$WEBAPP_FOLDER_NAME"
mkdir "$WEBAPP_FOLDER_NAME"
cd "$WEBAPP_FOLDER_NAME"
git clone https://gitlab+deploy-token-3719258:NqbfDYkKoqt_zPD518b6@gitlab.com/coderizers/fgic.git .
if [ $? -eq 0 ]; then
    echo -e "\e[32mRepository cleaned. \e[0m"
else
    echo -e "\e[31mRepository creation failed. \e[0m"
    exit
fi

TARGET_FILE="requirements.txt"
if [ -f "$TARGET_FILE" ]
then
    echo -e "\e[32m$TARGET_FILE exists. \e[0m"
else
    echo -e "\e[31m$TARGET_FILE does not exist. \e[0m"
    exit
fi

# Create Virtual Environment
echo "Creating Virtual environment"
rm -rf venv
python3 -m venv venv
if [ $? -eq 0 ]; then
    echo -e "\e[32mVirtual Environment created. \e[0m"
else
    echo -e "\e[31mVirtual Environment creation failed. \e[0m"
    exit
fi

# Install dependecies
source venv/bin/activate
python -m pip install pip -U
pip install wheel
pip install -r requirements.txt
if [ $? -eq 0 ]; then
    echo -e "\e[32mPython requirements installed. \e[0m"
else
    echo -e "\e[31mPython requirements failed to install. \e[0m"
    exit
fi

# Installing Python GDAL library
GDAL_VERSION="$(ogrinfo --version | cut -d " " -f 2 | cut -d "," -f 1)"
echo "GDAL Version $GDAL_VERSION installed"
export CPLUS_INCLUDE_PATH=/usr/include/gdal
export C_INCLUDE_PATH=/usr/include/gdal
pip install GDAL=="$GDAL_VERSION"
if [ $? -eq 0 ]; then
    echo -e "\e[32mPython GDAL installed. \e[0m"
else
    echo -e "\e[31mPython GDAL failed to install. \e[0m"
    exit
fi

# Creating some necessary folder
mkdir /home/fgic/DummyData
mkdir /home/fgic/DummyData/HotFolder
mkdir /home/fgic/DummyData/IngestedResource
echo -e "\e[32mCreated Dummy Hot Folder \e[0m"

# Copying the production file
cp .env.production .env
echo -e "\e[32mCopied Environment file \e[0m"

sudo chmod -R 777 ../FGICWebApp/
echo -e "DATABASE_URL=$DATABASE_URL" >> .env

# Change DATABASE URL in Airflow
old_text="postgresql+psycopg2://coderize:coderize@localhost/iads"
sed -i "s#$old_text#$AIRFLOW_DATABASE_URL#g" "Airflow/airflow.cfg"

# Run Migrations
python manage.py migrate
if [ $? -eq 0 ]; then
    echo -e "\e[32mDjango Migration Completed. \e[0m"
else
    echo -e "\e[31mDjango Migration failed. \e[0m"
    exit
fi

export AIRFLOW_HOME=/home/fgic/FGICWebApp/Airflow
airflow db migrate
if [ $? -eq 0 ]; then
    echo -e "\e[32mAirflow Migration Completed. \e[0m"
else
    echo -e "\e[31mAirflow Migration failed. \e[0m"
    exit
fi

airflow users  create --role Admin --username admin --email admin --firstname admin --lastname admin --password admin
if [ $? -eq 0 ]; then
    echo -e "\e[32mAirflow User created. \e[0m"
else
    echo -e "\e[31mAirflow User creation failed. \e[0m"
    exit
fi

sudo cp Scripts/airflow-webserver.service /etc/systemd/system/
sudo cp Scripts/airflow-scheduler.service /etc/systemd/system/
if [ $? -eq 0 ]; then
    echo -e "\e[32mService file copied \e[0m"
else
    echo -e "\e[31mService file failed to copy \e[0m"
    exit
fi

echo "Starting service"
sudo systemctl daemon-reload

sudo systemctl start airflow-webserver.service
if [ $? -eq 0 ]; then
    echo -e "\e[32mService Airflow Webserver started \e[0m"
else
    echo -e "\e[31mService Airflow Webserver failed to start \e[0m"
    exit
fi

sudo systemctl start airflow-scheduler.service
if [ $? -eq 0 ]; then
    echo -e "\e[32mService Airflow Schduler started \e[0m"
else
    echo -e "\e[31mService Airflow Schduler failed to start \e[0m"
    exit
fi

