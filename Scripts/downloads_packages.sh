

#!/bin/bash

# Define an array of items
items=("gdal-bin" "python3-dev" "libpq-dev " "libgdal-dev" "python3.10-venv" \
    "python3-dev" "build-essential" "libssl-dev" \
    "postgresql" "postgis")

# Iterate through the array using a for loop
for item in "${items[@]}"
do
    sudo apt clean
    sudo apt-get --allow-unauthenticated --force-reinstall true -y install --print-uris $item | cut -d\' -f2 | grep http > "pkgs/${item}.txt"
    mkdir -p "pkgs/${item}"
    wget -i "pkgs/${item}.txt" -P "pkgs/${item}"
    rm "pkgs/${item}.txt"

done



# sudo apt-get --allow-unauthenticated -y install --print-uris gdal-bin 
# python3-dev 
# libpq-dev 
# libgdal-dev
# python3.10-venv
# python3-dev 
# build-essential 
# libssl-dev 