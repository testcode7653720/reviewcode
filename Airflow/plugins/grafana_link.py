from airflow.plugins_manager import AirflowPlugin


class NPlugin(AirflowPlugin):
    name = "n_plugin"
    appbuilder_menu_items = [{
        "name": "Analytics Dashboard",
        "href": "http://iads.coderize.in:3000",
    }]
