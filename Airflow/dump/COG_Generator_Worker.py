import pendulum
from datetime import timedelta
from airflow.decorators import task
from airflow.exceptions import AirflowSkipException
from django_dag import DjangoDAG


dag =  DjangoDAG(dag_id="COG_Generator_Worker",
               description='Pre Ingestion Service',
               start_date=pendulum.datetime(2021, 1, 1, tz="UTC"),
               schedule_interval=timedelta(minutes=10), 
               catchup=False,
               tags=['Worker'])

@task(dag=dag)
def find_file_to_ingest_sub_worker():
    return


f1 = find_file_to_ingest_sub_worker()

    
