import pendulum
from datetime import timedelta
from airflow.decorators import task

from django_dag import DjangoDAG


@task()
def find_file_to_ingest():

    import logging
    from IngestionEngine.models import HotFile

    logger = logging.getLogger('ingestion-engine')

    hot_files = HotFile.objects.filter(is_ready_for_ingestion=True)

    return [_.id for _ in hot_files]


@task()
def server_path_task(hot_file_id):

    from IngestionEngine.workers import ServerPathReader
    from IngestionEngine.models import HotFile
    hot_file = HotFile.objects.get(id=hot_file_id)

    worker = ServerPathReader()
    worker.start(hot_file=hot_file)


with DjangoDAG(
    dag_id="Get_Server_Path_Worker",
    description='Server Path Worker',
    start_date=pendulum.datetime(2021, 1, 1, tz="UTC"),
    schedule_interval=timedelta(minutes=10),
    catchup=False,
    tags=['Worker']
) as dag:

    f1 = find_file_to_ingest()

    server_path_task.expand(hot_file_id=f1)
