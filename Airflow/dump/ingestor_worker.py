import pendulum
from datetime import timedelta
from airflow.decorators import task
from airflow.operators.python import PythonOperator
from django_dag import DjangoDAG


dag = DjangoDAG(dag_id="Ingestor_Worker", 
               description='Ingestor Service', 
               start_date=pendulum.datetime(2021, 1, 1, tz="UTC"), 
               schedule_interval=timedelta(minutes=10), catchup=False,
               tags=['Worker'])

@task(dag=dag)
def ingestor_sub_worker():

    import logging
    from IngestionEngine.workers import ServerPathReader, IngestorWorker
    from IngestionEngine.models import HotFile

    logger = logging.getLogger('ingestion-engine')

    hot_files = HotFile.objects.filter(is_ready_for_ingestion=True)

    for hot_file in hot_files:
        logger.info(f"Ingestor task started for {hot_file}")
        worker = IngestorWorker()


