import pendulum
from airflow.decorators import task
from django_dag import DjangoDAG


dag = DjangoDAG(dag_id="E_STAC_Ingestor_Worker",
                description='STAC Ingestor Service',
                start_date=pendulum.datetime(2023, 1, 1, 5, 0, 0, tz="UTC"),
                schedule_interval='40 12 * * *',
                catchup=False,
                tags=['Worker'])


@task(dag=dag)
def find_file_for_stac_ingestor_sub_worker():

    from IngestionEngine.workers import IStacIngesterWorker

    worker = IStacIngesterWorker()
    list_of_source_data = worker.find_eligible_items()

    return [_.SourceDataID for _ in list_of_source_data]


@task(dag=dag)
def stac_ingestor_sub_worker(source_data_id):

    import logging
    logging.info("Pre-ingestion worker started its work.")
    from IngestionEngine.workers import IStacIngesterWorker
    from IngestionEngine.models import SourceData

    source_data = SourceData.objects.get(SourceDataID=source_data_id)
    worker = IStacIngesterWorker()
    worker.start(source_data)


f1 = find_file_for_stac_ingestor_sub_worker()

stac_ingestor_sub_worker.expand(source_data_id=f1)
