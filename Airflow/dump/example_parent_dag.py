import pendulum
import random
import time
from airflow import DAG
from airflow.decorators import task
from airflow.operators.trigger_dagrun import TriggerDagRunOperator


@task()
def file_finder():
    import os
    import django
    import sys

    sys.path.append(os.getcwd())
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "IADS.settings")
    django.setup()

    import logging
    logger = logging.getLogger('django')
    logger.info("Something happend in parent dag")

    from IngestionEngine.models import HotFile
    return list([i.id for i in HotFile.objects.all()])
        

    # trigger_dependent_dag = TriggerDagRunOperator(
    #     task_id="trigger_dependent_dag",
    #     trigger_dag_id="child_dag",
    # )

    # trigger_dependent_dag()

@task()
def file_copy_worker(x):
    import os
    import django
    import sys

    sys.path.append(os.getcwd())
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "IADS.settings")
    django.setup()

    from IngestionEngine.models import HotFile
    hot_file = HotFile.objects.get(id=x)
    
    import logging
    logger = logging.getLogger('django')
    logger.info(f"this is child speaking {hot_file}")

    return x


@task()
def grand_child(y):
    import os
    import django
    import sys

    sys.path.append(os.getcwd())
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "IADS.settings")
    django.setup()

    from IngestionEngine.models import HotFile
    hot_file = HotFile.objects.get(id=y)
    
    import logging
    logger = logging.getLogger('django')
    logger.info(f"this is grandchild child speaking {hot_file}")


with DAG(
    dag_id="parent_dg",
    schedule=None,
    start_date=pendulum.datetime(2021, 1, 1, tz="UTC"),
    catchup=False
) as dag:

    f1 = file_finder()

    f2 = file_copy_worker.expand(x=f1)

    import logging
    logger = logging.getLogger('django')
    logger.info(f"this is dag  speaking {f2}")

    grand_child.expand(y=f2)


