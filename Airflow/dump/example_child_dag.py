import pendulum

from airflow import DAG
from airflow.decorators import task
from airflow.sensors.external_task import ExternalTaskSensor



@task()
def child_dag_task():
    import os
    import django
    import sys

    sys.path.append(os.getcwd())
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "IADS.settings")
    django.setup()

    import logging
    logger = logging.getLogger('django')
    logger.info("Something happend in child dag")


with DAG(
    dag_id="child_dag",
    schedule=None,
    start_date=pendulum.datetime(2021, 1, 1, tz="UTC"),
    catchup=False
) as dag:

    # ets_branch_1 = ExternalTaskSensor(
    #     task_id="ets_branch_1",
    #     external_dag_id="parent_dg",
    #     external_task_id="my_task",
    #     allowed_states=["success"],
    #     failed_states=["failed", "skipped"],
    # )

    child_task = child_dag_task()

    # ets_branch_1 >> child_task
