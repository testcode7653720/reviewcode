import os
import json
import rasterio
import urllib.request
import pystac

from datetime import datetime, timezone
from shapely.geometry import Polygon, mapping
from tempfile import TemporaryDirectory

catalog = pystac.Catalog(id='iads-catalog', description='IADS Catalog by CodeRize Tech')
catalog.normalize_hrefs("./stac-catalog")
catalog.save(catalog_type=pystac.CatalogType.SELF_CONTAINED)