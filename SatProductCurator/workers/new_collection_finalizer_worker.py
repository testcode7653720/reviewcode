from IngestionEngine.workers._base_logger import Logger
from IngestionEngine.workers._base_worker import BaseWorker
import logging
from SatProductCurator.models import NewCollectionRequest

log = Logger("NewCollectionFinalizerWorker").get_logger()

WorkerName = "NewCollectionFinalizerWorker"
InProgress = "InProgress"
Completed = "Completed"
Failed = "Failed"
Scheduled = "Scheduled"
Pause = "Pause"


class NewCollectionFinalizerWorker(BaseWorker):
    """SatDownloadWorker is downloading images from Satellite which were searched by SatProductSearchWorker."""

    def __init__(self) -> None:
        super().__init__()

    # Method to find eligible items for processing.
    def find_eligible_items(self):
        return NewCollectionRequest.objects.filter(IsIngestionCompleted=False)

    # Method to start the processing of an eligible item.
    def process_started_worker(self, collection_request: NewCollectionRequest):

        # Throw error if checked for non- serached collection requests
        if not collection_request.IsSearchSuccessful:
            log(f"Collection request {collection_request.NewCollectionID} hasn't been searched yet.",
                level=logging.ERROR)     # Not adding collection id to failure logs to avoid excessive logs
            raise Exception(f"Collection request {collection_request.NewCollectionID} hasn't been searched yet.")

        # Check if download for all the tiles have completed.
        if not collection_request.IsDownloadCompleted:
            tiles = collection_request.ImageTiles.filter(is_downloaded=False)
            if tiles:
                log(f"Collection request {collection_request.NewCollectionID} Download hasn't completed",
                    level=logging.WARNING)
                return
            else:
                collection_request.IsDownloadCompleted = True
                collection_request.save()
                log("Collection request's download has completed.", collection_request=collection_request)

        # Check if all images have been ingested to CTPI
        if not collection_request.IsIngestionCompleted:
            tiles = collection_request.ImageTiles.all()

            if collection_request.ImageTiles.exclude(
                    SourceData__IADSCoreStatus__in=('CTPIIngesterWorker_Completed',)
            ).exists():
                log(f"Collection request's {collection_request.NewCollectionID} ingestion has not been completed.",
                    level=logging.WARNING)
            else:
                collection_request.IsIngestionCompleted = True
                collection_request.save()
                log("Collection request's ingestion has completed.", collection_request=collection_request)

    def start(self, collection_request: NewCollectionRequest):
        self.process_started_worker(collection_request)
