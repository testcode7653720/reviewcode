import requests
from django.conf import settings
from IngestionEngine.workers._base_logger import Logger
from IngestionEngine.workers._base_worker import BaseWorker
import logging
from SatProductCurator.models.satellite_usage_details import SatelliteUsageDetails
from SatProductCurator.serializers.sat_usage_details_serializer import (
    SatelliteUsageDetailsSerializer,
)


log = Logger("SatUsageIngesterWorker").get_logger()

WorkerName = "SatUsageIngesterWorker"
InProgress = "InProgress"
Completed = "Completed"
Failed = "Failed"
Scheduled = "Scheduled"
Pause = "Pause"
IngestionInProgress = "Ingestion_InProgress"


class SatUsageIngesterWorker(BaseWorker):
    """SatUsageIngesterWorker is added Satellite Usage Details in MnASystems Database Table."""

    def __init__(self) -> None:
        super().__init__()

    # Method to find eligible items for processing.
    def find_eligible_items(self):
        try:
            log(
                "Searching Satellite Usage Details items which is not inseted into the MnA system.",
                level=logging.DEBUG,
            )
            sat_items = SatelliteUsageDetails.objects.filter(MnASatID="NA")

            return sat_items
        except Exception as e:
            log(
                f"Error while finding eligible items to start worker, {str(e)}.",
                level=logging.ERROR,
            )
            raise e

    def get_access_token(self, sat_item: SatelliteUsageDetails):
        url = settings.KEYCLOAK_TOKEN_URL

        # Request payload
        payload = {
            "grant_type": settings.PENTA_GRANT_TYPE,
            "client_id": settings.PENTA_CLIENT_ID,
            "client_secret": settings.PENTA_CLIENT_SECRET,
            "username": settings.PENTA_API_USERNAME,
            "password": settings.PENTA_AUTH_PASSWORD,
        }

        # Request Headers
        headers = {"Content-Type": settings.CONTENT_TYPE}

        try:
            log(
                "Making request to get access token.",
                sat_item=sat_item,
                level=logging.DEBUG,
            )
            response = requests.post(
                url,
                data=payload,
                headers=headers,
                verify=False,
            )
            response.raise_for_status()  # Raise an exception for 4xx/5xx status codes

            # Extracting access token from the response
            log(
                "Extracting access token from the response.",
                sat_item=sat_item,
                level=logging.DEBUG,
            )
            access_token = response.json().get("access_token")

            if access_token:
                return access_token
            else:
                # If access token is not found in the response
                log(
                    "Access token not found in response.",
                    sat_item=sat_item,
                    level=logging.WARNING,
                )
                return None

        except requests.exceptions.RequestException as e:
            log(f"Error: {str(e)}", sat_item=sat_item, level=logging.ERROR)
            return None

    # Method to insert data into the MNA API.
    def insert_sat_details_into_mna(self, sat_item: SatelliteUsageDetails):
        access_token = self.get_access_token(sat_item)
        if access_token:
            log(
                f"Access Token is: {access_token}",
                sat_item=sat_item,
                level=logging.DEBUG,
            )

            url_from_mna = settings.MNA_SAT_DETAILS_INSERT_URL

            headers = {
                "Authorization": access_token,
                "PentaOrgID": settings.PENTA_ORG_ID,
                "PentaUserRole": settings.PENTA_USER_ROLE,
                "PentaSelectedLocale": settings.PENTA_SELECTED_LOCALE,
            }

            # Serialize the Satellite Data.
            log(
                "Serializing the Satellite Data.",
                sat_item=sat_item,
                level=logging.DEBUG,
            )

            serializer = SatelliteUsageDetailsSerializer(sat_item)

            log(
                "Extracting Data from Serializer.",
                sat_item=sat_item,
                level=logging.DEBUG,
            )
            data_to_send = serializer.data
            log(
                f"Data extraction successful and Data to send is: {data_to_send}",
                sat_item=sat_item,
                level=logging.DEBUG,
            )
            log(
                "Data Serialized Successfully.",
                sat_item=sat_item,
                level=logging.DEBUG,
            )

            try:
                # Make API Requests.
                log(
                    "Creating API request to MnA.",
                    sat_item=sat_item,
                    level=logging.DEBUG,
                )

                response = requests.post(
                    url_from_mna,
                    headers=headers,
                    json=data_to_send,
                    verify=False,
                )

                log(
                    f"Request url is: {response.request.url}",
                    sat_item=sat_item,
                    level=logging.DEBUG,
                )
                log(
                    f"Request body is: {response.request.body}",
                    sat_item=sat_item,
                    level=logging.DEBUG,
                )
                log(
                    f"Request headers is: {response.request.headers}",
                    sat_item=sat_item,
                    level=logging.DEBUG,
                )

                log(
                    f"Response from MnA in text format is : {response.text}",
                    sat_item=sat_item,
                    level=logging.DEBUG,
                )

                log(
                    f"Response from MnA is: {response.json()}",
                    sat_item=sat_item,
                    level=logging.DEBUG,
                )
                log(
                    f"Response code from MnA is: {response.status_code}",
                    sat_item=sat_item,
                    level=logging.DEBUG,
                )
            except requests.RequestException as e:
                log(
                    f"Request failed: {e}",
                    sat_item=sat_item,
                    level=logging.DEBUG,
                )

            if response.status_code == 201:
                # Get MnAID from the response.
                log(
                    "Getting SatID from response.",
                    sat_item=sat_item,
                    level=logging.DEBUG,
                )
                mna_id = response.json().get("SatID")

                log(
                    f"MnA SatID is: {mna_id}",
                    sat_item=sat_item,
                    level=logging.DEBUG,
                )

                # Update MnAID in TargetData.
                log(
                    f"Updating MnASatID is: {mna_id}",
                    sat_item=sat_item,
                    level=logging.DEBUG,
                )
                sat_item.MnASatID = mna_id
                sat_item.save()
                log(
                    "Updated MnAID in SatelliteusageDetails table Successfully.",
                    sat_item=sat_item,
                    level=logging.DEBUG,
                )

                return True, response.json()
            else:
                return False, response.json()
        else:
            log("Failed to fetch Access Token.", sat_item=sat_item, level=logging.DEBUG)
            return False

    def process_started_worker(self, sat_item: SatelliteUsageDetails):
        log("SatUsageIngesterWorker Started.", sat_item=sat_item, level=logging.INFO)

        # calling method insert_into_mna to insert metadata into mna system.

        log(
            "Reached at the 'insert_sat_details_into_mna' Method & About to Execute.",
            sat_item=sat_item,
            level=logging.DEBUG,
        )
        insert_into_mna, response_status = self.insert_sat_details_into_mna(sat_item)
        log(
            "insert_sat_details_into_mna Method executed Sucessfully.",
            sat_item=sat_item,
            level=logging.DEBUG,
        )

        if insert_into_mna:
            log(
                "Well Done!! SatUsageIngesterWorker Completed Successfully.",
                sat_item=sat_item,
                level=logging.INFO,
            )
            log(
                f"Operation Sucessful and response is: {response_status}",
                sat_item=sat_item,
                level=logging.DEBUG,
            )

        else:
            log(
                "Oops!! SatUsageIngesterWorker Failed.",
                sat_item=sat_item,
                level=logging.INFO,
            )
            log(
                f"Operation Failed and response is: {response_status}",
                sat_item=sat_item,
                level=logging.DEBUG,
            )

    def start(self, sat_item: SatelliteUsageDetails):
        log(
            "Calling 'process_started_worker' of SatUsageIngesterWorker.",
            sat_item=sat_item,
            level=logging.DEBUG,
        )

        # Calling process_started_worker method.
        self.process_started_worker(sat_item)
