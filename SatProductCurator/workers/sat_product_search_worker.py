import logging
import traceback
from typing import List
from IngestionEngine.workers._base_logger import Logger
from IngestionEngine.workers._base_worker import BaseWorker

from SatProductCurator.models import NewCollectionRequest
from SatProductCurator.services import NewCollectionRequestService

log = Logger("SatProductSearchWorker").get_logger()

WorkerName = "SatProductSearchWorker"
InProgress = "InProgress"
Completed = "Completed"
Failed = "Failed"
Scheduled = "Scheduled"
Pause = "Pause"


# Define the SatProductSearchWorker class inheriting from BaseWorker
class SatProductSearchWorker(BaseWorker):
    """SatProductSearchWorker is searching images from Satellite."""

    def __init__(self) -> None:
        super().__init__()

    # Method to find eligible items for processing.
    def find_eligible_items(self) -> List[NewCollectionRequest]:
        """Find items that are ready for SatProductSearchWorker."""

        try:
            log(
                "Searching eligible items to Search from Satellite Provider.",
                level=logging.DEBUG,
            )
            # Getting NewCollectionRequest objects which is having IsAccepted status True
            # and IsSearchSuccessful status is False.
            return NewCollectionRequest.objects.filter(
                IsAccepted=True, IsSearchSuccessful=False
            )
        except Exception as e:
            log(
                f"Error while finding eligible items to start worker:{WorkerName}:{e} and {traceback.format_exc()}",
                level=logging.ERROR,
            )
            raise e

    # Method to check eligibility of an item for processing.
    def eligible_item_reader(self, new_collection_request: NewCollectionRequest):
        """Find eligibility of item to start SatProductSearchWorker."""
        log(
            "Checking if search is already completed or not for an item.",
            collection_request=new_collection_request,
            level=logging.DEBUG,
        )
        if new_collection_request.IsSearchSuccessful:
            # Raise an exception if the item has already been searched.
            raise Exception("Already Searched.")
        else:
            return True

    # Method to start the processing of an eligible item.
    def process_started_worker(self, new_collection_request: NewCollectionRequest):
        log(
            "Reached at the 'process_started_worker' Method & About to Execute.",
            collection_request=new_collection_request,
            level=logging.DEBUG,
        )

        log(
            "Well Done!! SatProductSearchWorker Started.",
            collection_request=new_collection_request,
            level=logging.INFO,
        )

        log(
            "Create an instance of NewCollectionRequestService.",
            collection_request=new_collection_request,
            level=logging.DEBUG,
        )

        # instantiate the NewCollectionRequestService instance
        service = NewCollectionRequestService()
        log(
            "'search' method of the service is about to execute to perform the search operation.",
            collection_request=new_collection_request,
            level=logging.DEBUG,
        )

        # search method of NewCollectionRequestService called
        service.search(new_collection_request)
        log(
            "'search' method of service executed Successfully.",
            collection_request=new_collection_request,
            level=logging.DEBUG,
        )

        log(
            "Well Done!! SatProductSearchWorker Completed Successfully.",
            collection_request=new_collection_request,
            level=logging.INFO,
        )

    def start(self, new_collection_request: NewCollectionRequest):
        log(
            "Checking if Item is Eligible to Start SatProductSearchWorker.",
            collection_request=new_collection_request,
            level=logging.DEBUG,
        )
        # Calling eligible_item_reader to find is item is eligible to start or not.
        is_eligible = self.eligible_item_reader(new_collection_request)

        if is_eligible:
            log(
                "Now Starting SatProductSearchWorker.",
                collection_request=new_collection_request,
                level=logging.INFO,
            )
            # Calling process_started_worker method.
            self.process_started_worker(new_collection_request)
