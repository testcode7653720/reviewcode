import os
from django.conf import settings
from IngestionEngine.workers._base_logger import Logger
from IngestionEngine.workers._base_worker import BaseWorker
from IngestionEngine.models import SourceData
import logging
from SatProductCurator.models import SatelliteImageTile
from SatProductCurator.services import SatelliteImageTileService

log = Logger("SatDownloadWorker").get_logger()

WorkerName = "SatDownloadWorker"
InProgress = "InProgress"
Completed = "Completed"
Failed = "Failed"
Scheduled = "Scheduled"
Pause = "Pause"

# Define the SatDownloadWorker class inheriting from BaseWorker.

source_data_type = "Hot Folder 2"


class SatDownloadWorker(BaseWorker):
    """SatDownloadWorker is downloading images from Satellite which were searched by SatProductSearchWorker."""

    def __init__(self) -> None:
        super().__init__()

    # Method to find eligible items for processing.
    def find_eligible_items(self):
        """Find items that are ready for SatDownloadWorker."""

        try:
            # Query SatelliteImageTile objects that are not downloaded and order by ID.
            log(
                "Searching SatelliteImageTile objects that are not downloaded and order by ID.",
                level=logging.DEBUG,
            )
            return SatelliteImageTile.objects.filter(is_downloaded=False).order_by(
                "-id"
            )
        except Exception as e:
            log(
                f"Error while finding eligible items to start worker {WorkerName}. {e}",
                level=logging.ERROR,
            )
            raise e

    def eligible_item_reader(self, sat_image_tile: SatelliteImageTile):
        """Find eligibility of item to start SatDownloadWorker."""

        log(
            "Checking if the item is already downloaded or not.",
            sat_image_tile=sat_image_tile,
            level=logging.DEBUG,
        )
        if sat_image_tile.is_downloaded:
            log(
                "Item is already downloaded.",
                sat_image_tile=sat_image_tile,
                level=logging.ERROR,
            )
            # Raise an exception if the item has already been downloaded
            raise Exception("Already downloaded.")
        else:
            # Return True if the item is eligible for processing.
            return True

    # Method to start the processing of an eligible item.
    def process_started_worker(self, sat_image_tile: SatelliteImageTile):
        log(
            "Reached at the 'process_started_worker' Method & About to Execute.",
            sat_image_tile=sat_image_tile,
            level=logging.DEBUG,
        )

        log(
            "Well Done!! SatDownloadWorker Started.",
            sat_image_tile=sat_image_tile,
            level=logging.INFO,
        )

        # Download the image
        log(
            "Create an instance of SatelliteImageTileService.",
            sat_image_tile=sat_image_tile,
            level=logging.DEBUG,
        )
        service = SatelliteImageTileService()

        # Checking if source data exists for the satellite tile image.
        log(
            "Checking if Source Data exists for the Satellite Tile image.",
            sat_image_tile=sat_image_tile,
            level=logging.DEBUG,
        )
        if sat_image_tile.SourceData:
            source_data: SourceData = sat_image_tile.SourceData

        else:
            log(
                "Calling get_file_name method from service class to get source dataset name.",
                sat_image_tile=sat_image_tile,
                level=logging.DEBUG,
            )
            file_name = service.get_file_name(sat_image_tile)
            log(
                "get_file_name method from service class executed Successfully.",
                sat_image_tile=sat_image_tile,
                level=logging.DEBUG,
            )

            log(
                "Generating Source Dataset Path.",
                sat_image_tile=sat_image_tile,
                level=logging.DEBUG,
            )

            sat_download_path = settings.SAT_DOWNLOAD_FOLDER

            image_type = sat_download_path.split("/")[-1]

            source_data = SourceData(
                SourceDatasetName=file_name,
                SourceDatasetPath=os.path.join(sat_download_path, file_name),
                SourceDatasetType=source_data_type,
                IsZippedFile=True,
                IADSIngestionStatus="Ingestion_InProgress",
                IADSCoreStatus=f"{WorkerName}_{InProgress}",
            )
            source_data.save()
            log(
                "Source Data Created Successfully.",
                source_data=source_data,
                sat_image_tile=sat_image_tile,
                level=logging.DEBUG,
            )

            # Set the SourceData_id of sat_image_tile and save.
            sat_image_tile.SourceData_id = source_data.pk
            sat_image_tile.save()
            log(
                f"Source Data ID set to {source_data.pk} for sat_image_tile and Saved Successfully.",
                source_data=source_data,
                sat_image_tile=sat_image_tile,
                level=logging.DEBUG,
            )

        # Set IADSCoreStatus SatDownloadWorker InProgress.
        source_data.IADSCoreStatus = f"{WorkerName}_{InProgress}"
        source_data.save()

        log(
            "Downloading Satellite image tile.",
            source_data=source_data,
            sat_image_tile=sat_image_tile,
            level=logging.INFO,
        )
        service.download(sat_image_tile, source_data)
        log(
            "Satellite image tile downloaded successfully.",
            sat_image_tile=sat_image_tile,
            source_data=source_data,
            level=logging.INFO,
        )

        # Set IADSCoreStatus SatDownloadWorkerCompleted.

        source_data.IADSCoreStatus = f"{WorkerName}_{Completed}"
        source_data.IADSIngestionStatus = "Ready_For_Ingestion"
        source_data.save()
        log(
            "Well Done!! SatDownloadWorker Completed Successfully.",
            level=logging.INFO,
            source_data=source_data,
            sat_image_tile=sat_image_tile,
        )

    def start(self, sat_image_tile: SatelliteImageTile):
        is_eligible = self.eligible_item_reader(sat_image_tile)
        if is_eligible:
            log(
                "Now Starting SatDownloadWorker.",
                sat_image_tile=sat_image_tile,
                level=logging.INFO,
            )
            # Calling process_started_worker method.
            self.process_started_worker(sat_image_tile)
