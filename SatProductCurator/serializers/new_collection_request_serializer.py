from rest_framework import serializers
from rest_framework_gis.serializers import GeometryField

from SatProductCurator.models import NewCollectionRequest, SatelliteUsageDetails
# from SatProductCurator.models.constants import PRODUCTS


class NewCollectionRequestSerializer(serializers.ModelSerializer):
    Extent = GeometryField()
    EndDate = serializers.DateTimeField(required=False)
    MnANewCollectionID = serializers.UUIDField(required=True)
    MnASatID = serializers.UUIDField(required=True)
    SatID = serializers.UUIDField(required=True)

    class Meta:
        model = NewCollectionRequest
        fields = (
            "MnANewCollectionID",
            "Extent",
            "StartDate",
            "EndDate",
            "MnASatID",
            "SatID"
        )

    def create(self, validated_data):
        SatID = validated_data.pop('SatID')  # Remove SatID since it doesn't exists on this model
        product = SatelliteUsageDetails.objects.get(SatID=SatID).SatelliteProductName
        validated_data['Product'] = product
        return super().create(validated_data)

    def validate_MnANewCollectionID(self, value):
        if NewCollectionRequest.objects.filter(MnANewCollectionID=value).exists():
            raise serializers.ValidationError("MnANewCollectionID already exists.")
        return value

    def validate_SatID(self, value):
        if not SatelliteUsageDetails.objects.filter(SatID=value).exists():
            raise serializers.ValidationError("Invalid SatID.")
        return value
