from rest_framework import serializers

from SatProductCurator.models import SatelliteUsageDetails


class SatelliteUsageDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = SatelliteUsageDetails
        # fields = '__all__'
        fields = (
            "SatID",
            "MnASatID",
            "SatelliteProductName",
            "SpatialResolutionRange",
            "Resolution",
            "ImageType",
            "SpectralBands",
            "Usage",
            "Industry",
            "SubscriptionStatus",
        )
