
from rest_framework import serializers

from SatProductCurator.models import SatelliteProviderConfiguration


class SatProviderSerializer(serializers.ModelSerializer):

    class Meta:
        model = SatelliteProviderConfiguration
        fields = '__all__'
