from django.urls import path, include
from rest_framework import routers
from .views import SatProviderViewSet
from .viewsets import NewCollectionRequestViewSet, SatelliteUsageDetailsViewSet


router = routers.SimpleRouter()
router.register("sat-product", SatProviderViewSet)
router.register("new-collection", NewCollectionRequestViewSet, basename='new-collection')
router.register("sat-usage-details", SatelliteUsageDetailsViewSet)

urlpatterns = [
    path("", include(router.urls)),
]
