from rest_framework.viewsets import ReadOnlyModelViewSet
from SatProductCurator.serializers import SatelliteUsageDetailsSerializer
from SatProductCurator.models import SatelliteUsageDetails


class SatelliteUsageDetailsViewSet(ReadOnlyModelViewSet):
    queryset = SatelliteUsageDetails.objects.all()
    serializer_class = SatelliteUsageDetailsSerializer
