import os
import shutil
import traceback
import zipfile
from IngestionEngine.models import SourceData
from SatProductCurator.models import SatelliteImageTile
from SatProductCurator.models.constants import (
    PRODUCT_SENTINEL1,
    PRODUCT_SENTINEL2,
    PRODUCT_SENTINEL3,
    PRODUCT_LANDSAT8,
    PRODUCT_LANDSAT9,
)

from .sentinel_tile_service import SentinelTileService
from .landsat_tile_service import LandsatTileService
import logging
from IngestionEngine.workers._base_logger import Logger

log = Logger("SentinelImageTileService").get_logger()


class SatelliteImageTileService:
    def __init__(self) -> None:
        pass

    def fetch_metadata(self, tile: SatelliteImageTile, source_data: SourceData):
        # If Product is Sentinel, call the SentinelTileService
        if tile.Product in (PRODUCT_SENTINEL1, PRODUCT_SENTINEL2, PRODUCT_SENTINEL3):
            sentinel_service = SentinelTileService()
            # Calling fetch_metadata for sentinel tile
            metadata = sentinel_service.fetch_metadata(
                sat_image_tile=tile, source_data=source_data
            )
            return metadata

        elif tile.Product in (PRODUCT_LANDSAT8, PRODUCT_LANDSAT9):
            landsat_service = LandsatTileService()
            # Calling fetch_metadata for landsat tile
            metadata = landsat_service.fetch_metadata(
                sat_image_tile=tile, source_data=source_data
            )
            return metadata
        # If Product is something else, we need to write the logic for that
        else:
            raise Exception(f"Unknown provider to tile {tile}.")

    def download(self, tile: SatelliteImageTile, source_data: SourceData):
        log(
            "Verifying whether the satellite image tile corresponds to the Sentinel or Landsat product type.",
            sat_image_tile=tile,
            source_data=source_data,
            level=logging.DEBUG,
        )
        # Check if the product type of the tile is Sentinel
        if tile.Product in (PRODUCT_SENTINEL1, PRODUCT_SENTINEL2, PRODUCT_SENTINEL3):
            log(
                "Product type is Sentinel hence calling sentinel service download.",
                sat_image_tile=tile,
                source_data=source_data,
                level=logging.DEBUG,
            )
            sentinel_service = SentinelTileService()
            # Call the Sentinel service download method and return the result
            is_downloaded = sentinel_service.download(
                sat_image_tile=tile, source_data=source_data
            )
            return is_downloaded
        # Check if the product type of the tile is Landsat
        elif tile.Product in (PRODUCT_LANDSAT8, PRODUCT_LANDSAT9):
            log(
                "Product type is Landsat hence calling Landsat service download.",
                sat_image_tile=tile,
                source_data=source_data,
                level=logging.DEBUG,
            )
            landsat_service = LandsatTileService()
            is_downloaded = landsat_service.download(
                sat_image_tile=tile, source_data=source_data
            )
            return is_downloaded
        else:
            raise Exception(f"Unknown Product while downloading: {tile.Product}")

    def get_file_name(self, tile: SatelliteImageTile):
        # generate file name for satellite image tile.
        log(
            "Verifying whether the satellite image tile corresponds to the Sentinel or Landsat product type.",
            sat_image_tile=tile,
            level=logging.DEBUG,
        )
        # Checking if satellite image tile product type is sentinel1 or sentinel2 or sentinel3
        if tile.Product in (PRODUCT_SENTINEL1, PRODUCT_SENTINEL2, PRODUCT_SENTINEL3):
            log(
                "return file name for Sentinel image tile.",
                sat_image_tile=tile,
                level=logging.DEBUG,
            )
            return tile.eodag_data["id"] + ".zip"

        # Checking if satellite image tile product type is landsat8 or landsat9
        elif tile.Product in (PRODUCT_LANDSAT8, PRODUCT_LANDSAT9):
            log(
                "return file name for Landsat image tile.",
                sat_image_tile=tile,
                level=logging.DEBUG,
            )
            return tile.eodag_data["id"] + ".zip"
        else:
            raise Exception(f"Unknown Product while fetching file name: {tile.Product}")

    def find_img_data_folder(self, source_folder):
        for root, dirs, files in os.walk(source_folder):
            if "IMG_DATA" in dirs:
                return os.path.join(root, "IMG_DATA")
        return None

    def move_files(
        self, source_folder, tile: SatelliteImageTile, source_data: SourceData
    ):
        log(
            "Calling 'find_img_data_folder' to check if IMG_DATA folder exists or not.",
            sat_image_tile=tile,
            source_data=source_data,
            level=logging.DEBUG,
        )
        # Find the IMG_DATA folder in the source folder
        img_data_folder = self.find_img_data_folder(source_folder)
        if img_data_folder is None:
            log(
                "IMG_DATA folder not found in the source folder.",
                sat_image_tile=tile,
                source_data=source_data,
                level=logging.DEBUG,
            )
            log(
                "This is landsat image.",
                sat_image_tile=tile,
                source_data=source_data,
                level=logging.DEBUG,
            )

        else:
            log(
                "IMG_DATA folder exists hence This is Sentinel image.",
                sat_image_tile=tile,
                source_data=source_data,
                level=logging.DEBUG,
            )
            log(
                "To shorten file path moving files from IMG_DATA folder to root folder.",
                sat_image_tile=tile,
                source_data=source_data,
                level=logging.DEBUG,
            )

            # To shorten file path moving files from IMG_DATA folder to root folder.
            for file in os.listdir(img_data_folder):
                source_file = os.path.join(img_data_folder, file)
                destination_file = os.path.join(source_folder, file)
                try:
                    # Attempt to move the file to the root folder
                    shutil.move(source_file, destination_file)
                    log(
                        f"Moved Successfully '{source_file}' to '{destination_file}'",
                        sat_image_tile=tile,
                        source_data=source_data,
                        level=logging.DEBUG,
                    )
                except Exception as e:
                    log(
                        f"Error while moving '{source_file}': {str(e)} and {traceback.format_exc()}",
                        sat_image_tile=tile,
                        source_data=source_data,
                        level=logging.DEBUG,
                    )

    def extract(self, tile: SatelliteImageTile, source_data: SourceData):
        """Extract Satellite tiles."""
        log(
            "Extracting satellite tiles.",
            sat_image_tile=tile,
            source_data=source_data,
            level=logging.DEBUG,
        )
        source_data = tile.SourceData
        # zip_path = source_data.target_data.TargetDatasetPath
        zip_path = source_data.target_data.LocalTargetDatasetPath
        output_folder_path = os.path.splitext(zip_path)[0]

        # Open the zip file
        log(
            f"Opening zip file: {zip_path}",
            sat_image_tile=tile,
            source_data=source_data,
            level=logging.DEBUG,
        )
        with zipfile.ZipFile(zip_path, "r") as zip_ref:
            # Extract all contents into the specified directory
            zip_ref.extractall(output_folder_path)
            log(
                f"Extracting contents into directory: {output_folder_path}",
                sat_image_tile=tile,
                source_data=source_data,
                level=logging.DEBUG,
            )

        tile.is_extracted = True
        log(
            "Set IsExtracted flag True.",
            sat_image_tile=tile,
            source_data=source_data,
            level=logging.DEBUG,
        )
        tile.extracted_path = output_folder_path
        log(
            f"Satellite tiles extracted successfully to: {output_folder_path}",
            sat_image_tile=tile,
            source_data=source_data,
            level=logging.DEBUG,
        )
        log(
            "Reached at 'move_files' method.",
            sat_image_tile=tile,
            source_data=source_data,
            level=logging.DEBUG,
        )
        self.move_files(output_folder_path, tile, source_data)
        tile.save()
        return output_folder_path

    def fetch_target_images(
        self, sat_image_tile: SatelliteImageTile, source_data: SourceData
    ):
        log(
            "Fetching target images for satellite image tile.",
            sat_image_tile=sat_image_tile,
            source_data=source_data,
            level=logging.DEBUG,
        )
        # Check if the product type of the tile is Sentinel
        if sat_image_tile.Product in (
            PRODUCT_SENTINEL1,
            PRODUCT_SENTINEL2,
            PRODUCT_SENTINEL3,
        ):
            sentinel_service = SentinelTileService()
            log(
                "Fetching target images using SentinelTileService.",
                sat_image_tile=sat_image_tile,
                source_data=source_data,
                level=logging.DEBUG,
            )
            # Call the fetch_target_images method of SentinelTileService and store the result
            target_images = sentinel_service.fetch_target_images(
                sat_image_tile=sat_image_tile, source_data=source_data
            )

        # Check if the product type of the tile is Landsat
        elif sat_image_tile.Product in (PRODUCT_LANDSAT8, PRODUCT_LANDSAT9):
            landsat_service = LandsatTileService()
            log(
                "Fetching target images using LandsatTileService.",
                sat_image_tile=sat_image_tile,
                source_data=source_data,
                level=logging.DEBUG,
            )
            # Call the fetch_target_images method of LandsatTileService and store the result
            target_images = landsat_service.fetch_target_images(
                sat_image_tile=sat_image_tile, source_data=source_data
            )

        else:
            raise Exception(
                f"Unknown Product while downloading: {sat_image_tile.Product}",
                sat_image_tile=sat_image_tile,
                source_data=source_data,
                level=logging.DEBUG,
            )

        return target_images

    def find_epsg_for_sentinel(self, tile: SatelliteImageTile, target_image_path):
        """This is for a hot fix where for the sentinel image,
        we can't get epsg code until extracted. To remove this and
        do this properly. move image extraction logic before metadata reading."""

        if tile.Product in (PRODUCT_SENTINEL2,):
            sentinel_service = SentinelTileService()
            return sentinel_service.find_epsg(target_image_path)
        else:
            raise Exception(
                f"find_epsg_for_sentinel is not applicable to product type {tile.Product}"
            )
