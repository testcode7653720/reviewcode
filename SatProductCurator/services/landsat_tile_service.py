import shutil
import tempfile
import json
import os
import traceback
from typing import List
import uuid
import zipfile
from eodag import EODataAccessGateway
import datetime
from datetime import date
import geojson
from django.conf import settings
from django.contrib.gis.geos import Polygon
from django.utils import timezone
import logging
from IngestionEngine.models import SourceData
from IngestionEngine.workers._base_logger import Logger

log = Logger("LandsatTileService").get_logger()

from IngestionEngine.models import SourceData
from SatProductCurator.models import SatelliteProviderConfiguration, SatelliteImageTile
from SatProductCurator.models.constants import (
    PRODUCT_LANDSAT8,
    PRODUCT_LANDSAT9,
    PROVIDER_PLANETARY_COMPUTER,
)
from eodag import setup_logging

setup_logging(verbose=3)


class LandsatTileService:
    def __init__(self) -> None:
        config = SatelliteProviderConfiguration.objects.get(
            SATProviderName=PROVIDER_PLANETARY_COMPUTER
        )

        os.environ["EODAG__PLANETARY_COMPUTER__AUTH__CREDENTIALS__USERNAME"] = (
            config.Username or ""
        )
        os.environ["EODAG__PLANETARY_COMPUTER__AUTH__CREDENTIALS__PASSWORD"] = (
            config.Password or ""
        )

    def search_by_polygon(
        self, product: str, start_date: date, end_date: date, polygon: Polygon
    ):
        # Initialize EODataAccessGateway instance
        dag = EODataAccessGateway()

        # Determine product type based on input product
        if product == PRODUCT_LANDSAT8:
            productType = "LANDSAT_C2L2"
        elif product == PRODUCT_LANDSAT9:
            productType = "LANDSAT_C2L2"
        else:
            raise Exception(f"Unknown product: {product}")

        # min_x, min_y, max_x, max_y

        # Search for data within the given polygon extent and time range
        extent = polygon.envelope.extent
        search_results, total_count = dag.search(
            productType=productType,
            start=str(start_date),
            end=str(end_date),
            geom={
                "lonmin": extent[0],
                "latmin": extent[1],
                "lonmax": extent[2],
                "latmax": extent[3],
            },
        )

        return json.loads(geojson.dumps(search_results))

    def filter_results_by_satellite(self, result_dict: dict, satellite: str):
        """Filter only landsat 8 or 9 results"""

        # Ensure satellite input is valid
        assert satellite in {"landsat-8", "landsat-9"}, f"Unknown satellite {satellite}"

        # Initialize an empty list to store filtered features
        filtered_features = []

        # Iterate through each feature in the result dictionary
        for feature in result_dict["features"]:
            # Check if the platformSerialIdentifier matches the specified satellite
            if feature["properties"]["platformSerialIdentifier"] == satellite:
                # If the satellite matches, add the feature to the filtered list
                filtered_features.append(feature)

        # Return the filtered features as a new FeatureCollection
        return {"type": "FeatureCollection", "features": filtered_features}

    def filter_results(self, search_results):
        pass

    def update_to_database(
        self, search_results: dict, product
    ) -> List[SatelliteImageTile]:
        # Initialize a list to store SatelliteImageTile objects
        tiles = []
        # Iterate through each tile JSON object in the search results
        for tile_json in search_results["features"]:
            try:
                # Attempt to retrieve the SatelliteImageTile from the database
                tile = SatelliteImageTile.objects.get(tile_id=tile_json["id"])
            except SatelliteImageTile.DoesNotExist:
                # If the tile does not exist in the database, create a new SatelliteImageTile object
                tile_date = datetime.datetime.strptime(
                    tile_json["properties"]["startTimeFromAscendingNode"],
                    "%Y-%m-%dT%H:%M:%S.%fZ",
                ).date()
                tile = SatelliteImageTile(
                    Product=product,
                    tile_id=tile_json["id"],
                    date=tile_date,
                    boundary=Polygon(tile_json["geometry"]["coordinates"][0]),
                    to_be_downloaded=True,
                    eodag_data=tile_json,
                )
                tile.save()

                log(
                    "Landsat image tile created Successfully.",
                    sat_image_tile=tile,
                    level=logging.DEBUG,
                )
            # Add the SatelliteImageTile object to the list of tiles
            tiles.append(tile)
        return tiles

    def download(self, sat_image_tile: SatelliteImageTile, source_data: SourceData):
        """This method uses a hack since for landsat8 and landsat9, there is
        some issue with serialization and deserialization of data.
        Hence we are again searching for this tile and then downloading from the
        search results directly."""

        log(
            "Downloading Landsat image tile.",
            sat_image_tile=sat_image_tile,
            source_data=source_data,
            level=logging.INFO,
        ),

        # Retrieve the first collection request associated with the satellite image tile
        log(
            "Retrieving the first collection request associated with the landsat image tile",
            sat_image_tile=sat_image_tile,
            source_data=source_data,
            level=logging.DEBUG,
        ),

        collection_request = sat_image_tile.newcollectionrequest_set.first()
        if collection_request is None:
            raise Exception("No new collection request for this tile was found.")

        # Initialize EODataAccessGateway instance
        dag = EODataAccessGateway()

        # Determine the product type based on the satellite image tile's product
        product = collection_request.Product

        if product == PRODUCT_LANDSAT8:
            productType = "LANDSAT_C2L2"
        elif product == PRODUCT_LANDSAT9:
            productType = "LANDSAT_C2L2"
        else:
            raise Exception(f"Unknown product: {product}")

        # Retrieve the extent information from the collection request
        log(
            "Getting the extent information from the collection request.",
            sat_image_tile=sat_image_tile,
            source_data=source_data,
            level=logging.DEBUG,
        ),
        extent = collection_request.Extent.envelope.extent

        # Perform a search using EODataAccessGateway

        log(
            "Performing a search using EODataAccessGateway.",
            sat_image_tile=sat_image_tile,
            source_data=source_data,
            level=logging.DEBUG,
        ),
        search_results, total_count = dag.search(
            productType=productType,
            start=str(collection_request.StartDate.date()),
            end=str(collection_request.EndDate.date()),
            geom={
                "lonmin": extent[0],
                "latmin": extent[1],
                "lonmax": extent[2],
                "latmax": extent[3],
            },
        )

        # Find the search result matching the tile ID
        log(
            "Finding the search result matching the tile ID.",
            sat_image_tile=sat_image_tile,
            source_data=source_data,
            level=logging.DEBUG,
        ),
        for search_result in search_results:
            if search_result.properties["id"] == sat_image_tile.tile_id:
                selected_search_result = search_result
                break
        else:
            raise Exception("No result found for this tile in the collection request.")

        # Update download attempts and start time for the satellite image tile
        sat_image_tile.dl_attempts += 1
        sat_image_tile.dl_start_time = timezone.now()
        sat_image_tile.save()
        log(
            "Updated download attempts and start time for the landsat image tile and starting download.",
            sat_image_tile=sat_image_tile,
            source_data=source_data,
            level=logging.DEBUG,
        ),

        # Download the selected search result using EODataAccessGateway
        dag.download(selected_search_result)

        log(
            "Checking if the file is downloaded.",
            sat_image_tile=sat_image_tile,
            source_data=source_data,
            level=logging.DEBUG,
        ),
        if not selected_search_result.location.startswith(r"file:///"):
            log(
                "File download hasn't completed by the API, some error must have occurred.",
                sat_image_tile=sat_image_tile,
                source_data=source_data,
                level=logging.DEBUG,
            )

            raise Exception("API says, File not downloaded")

        # Check if the downloaded file exists
        download_folder = os.path.join(
            settings.SAT_DOWNLOAD_FOLDER, sat_image_tile.tile_id
        )
        download_success = False

        log(
            "Checking if the downloaded file exists.",
            sat_image_tile=sat_image_tile,
            source_data=source_data,
            level=logging.DEBUG,
        ),
        if os.path.exists(download_folder):
            for filepath in os.listdir(download_folder):
                if filepath.endswith(".TIF"):
                    download_success = True
                    break

        if download_success:
            log(
                "file exists.",
                sat_image_tile=sat_image_tile,
                source_data=source_data,
                level=logging.DEBUG,
            ),
        else:
            raise Exception(
                "API says file downloaded, but file not present in the folder."
            )

        # Compress the downloaded folder into a zip file and delete the original folder

        log(
            "Compressing the downloaded folder into a zip file and delete the original folder",
            sat_image_tile=sat_image_tile,
            source_data=source_data,
            level=logging.DEBUG,
        ),
        shutil.make_archive(download_folder, "zip", download_folder)
        shutil.rmtree(download_folder)

        # Update satellite image tile properties to indicate successful download
        sat_image_tile.is_downloaded = True
        sat_image_tile.dl_end_time = timezone.now()
        sat_image_tile.save()
        log(
            "Update satellite image tile status is_downloaded and dl_end_time to indicate successful download.",
            sat_image_tile=sat_image_tile,
            source_data=source_data,
            level=logging.DEBUG,
        ),

        log(
            "File Downloaded Successfully.",
            sat_image_tile=sat_image_tile,
            source_data=source_data,
            level=logging.DEBUG,
        ),
        return True

    def fetch_metadata(
        self, sat_image_tile: SatelliteImageTile, source_data: SourceData
    ):
        """Fetch metadata for the sentinel tile
        Make sure to have the same format as BusinessMeta to avoid confusion."""

        log(
            "Fetching metadata for Landsat image tile.",
            sat_image_tile=sat_image_tile,
            source_data=source_data,
            level=logging.DEBUG,
        ),

        log(
            "Checking if satellite image tile has downloaded from Landsat 8 or 9",
            sat_image_tile=sat_image_tile,
            source_data=source_data,
            level=logging.DEBUG,
        )
        if sat_image_tile.Product in (PRODUCT_LANDSAT8, PRODUCT_LANDSAT9):
            metadata = {}
            properties = sat_image_tile.eodag_data.get("properties", {})
            metadata["EpsgCode"] = sat_image_tile.eodag_data["properties"]["proj:epsg"]
            metadata["Extent"] = Polygon(
                sat_image_tile.eodag_data["geometry"]["coordinates"][0]
            )
            metadata["Keywords"] = properties.get("keywords", None)
            metadata["CloudCover"] = properties.get("cloudCover", None)
            metadata["OrganizationName"] = properties.get("organisationName", None)
            metadata["ProcessingLevel"] = properties.get("processingLevel", None)
            metadata["Abstract"] = properties.get("abstract", None)
            metadata["SensorMode"] = properties.get("sensorMode", None)
            metadata["SensorType"] = properties.get("sensorType", None)
            metadata["ProductType"] = properties.get("productType", None)
            metadata["PlatformIdentifier"] = properties.get(
                "platformSerialIdentifier", None
            )
            metadata["Identifier"] = properties.get("parentIdentifier", None)
            metadata["LicenseBasedConstraints"] = properties.get("license", None)
            metadata["PlatformName"] = properties.get("platform", None)
            metadata["Title"] = properties.get("title", None)
            metadata["Resolution"] = properties.get("resolution", None)

            log(
                f"Successfully fetched metadata for Landsat tile image and metadata is: {metadata}",
                sat_image_tile=sat_image_tile,
                source_data=source_data,
                level=logging.DEBUG,
            )
            return metadata

        else:
            raise Exception(
                f"No fetching logic found for fetching metadata {sat_image_tile.Product}",
                sat_image_tile=sat_image_tile,
                source_data=source_data,
                level=logging.DEBUG,
            )

    def fetch_target_images(
        self, sat_image_tile: SatelliteImageTile, source_data: SourceData
    ):
        # Fetching target images for sentinel image tile.
        log(
            "Fetching target images for landsat image tile.",
            sat_image_tile=sat_image_tile,
            source_data=source_data,
            level=logging.DEBUG,
        ),

        folder_path = sat_image_tile.extracted_path
        if sat_image_tile.Product in (PRODUCT_LANDSAT8, PRODUCT_LANDSAT9):
            log(
                "Fetching images for Landsat tile.",
                sat_image_tile=sat_image_tile,
                source_data=source_data,
                level=logging.DEBUG,
            ),

            target_images = []

            log(
                "Checking every image in landsat tile image folder.",
                sat_image_tile=sat_image_tile,
                source_data=source_data,
                level=logging.DEBUG,
            ),
            for image in os.listdir(folder_path):
                item_path = os.path.join(folder_path, image)

                # Check if it's a file and has .tif or .tiff extension
                if os.path.isfile(item_path) and (
                    image.lower().endswith(".tif") or image.lower().endswith(".tiff")
                ):
                    # Generate a unique identifier (UUID)
                    unique_identifier = str(uuid.uuid4())

                    # Get the band name from the original filename
                    band_name = os.path.splitext(image)[0].split("_")[-1]

                    # Extract the extension from the original filename
                    extension = os.path.splitext(image)[1]

                    # Construct the new filename
                    new_filename = f"{unique_identifier}_{band_name}{extension.lower()}"

                    # Create the new file path
                    new_item_path = os.path.join(folder_path, new_filename)

                    # Rename the file
                    try:
                        os.rename(item_path, new_item_path)

                    except Exception as e:
                        log(
                            f"Error in renaming '{item_path}': {e} and {traceback.format_exc()}",
                            sat_image_tile=sat_image_tile,
                            source_data=source_data,
                            level=logging.DEBUG,
                        )

                    # Add the renamed file to target_images
                    image_data = {
                        "Path": new_item_path,
                        "BandName": band_name,
                    }
                    target_images.append(image_data)
        else:
            raise Exception(
                f"No fetching logic found for product type {sat_image_tile.Product}"
            )

        return target_images
