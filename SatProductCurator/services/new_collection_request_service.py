import traceback
from SatProductCurator.models import NewCollectionRequest
from SatProductCurator.models.constants import (
    PRODUCT_SENTINEL1,
    PRODUCT_SENTINEL2,
    PRODUCT_SENTINEL3,
    PRODUCT_LANDSAT8,
    PRODUCT_LANDSAT9,
)
from .sentinel_tile_service import SentinelTileService
from .landsat_tile_service import LandsatTileService
from IngestionEngine.workers._base_logger import Logger
import logging

log = Logger("NewCollectionRequestService").get_logger()


class NewCollectionRequestService:
    def __init__(self) -> None:
        pass

    def search(self, collection_request: NewCollectionRequest):
        log(
            "Inside 'search' method of NewCollectionRequestService.",
            level=logging.DEBUG,
            collection_request=collection_request,
        )
        try:
            # Extracting the product type from the collection request
            log(
                "Extracting Product from new collection request.",
                level=logging.DEBUG,
                collection_request=collection_request,
            )
            product = collection_request.Product
        except Exception as e:
            log(
                f"Error while Extracting Product from new collection request:{str(e)} and {traceback.format_exc()}",
                level=logging.DEBUG,
                collection_request=collection_request,
            )
            raise e

        # Checking the type of product and selecting appropriate service for search
        log(
            "Checking the type of product and selecting appropriate service for search.",
            level=logging.DEBUG,
            collection_request=collection_request,
        )
        if product in (PRODUCT_SENTINEL1, PRODUCT_SENTINEL2, PRODUCT_SENTINEL3):
            log(
                "Initializing SentinelTileService.",
                level=logging.DEBUG,
                collection_request=collection_request,
            )

            # Initializing SentinelTileService
            service = SentinelTileService()

            # Searching for tiles within the specified polygon and time range
            log(
                "Searching for Tiles within the specified Polygon and Time Range.",
                level=logging.DEBUG,
                collection_request=collection_request,
            )
            results = service.search_by_polygon(
                product=product,
                start_date=collection_request.StartDate.date(),
                end_date=collection_request.EndDate.date(),
                polygon=collection_request.Extent,
            )
            log(
                "Search Tiles Successfully within the specified Polygon and Time Range.",
                level=logging.DEBUG,
                collection_request=collection_request,
            )

        elif product in (PRODUCT_LANDSAT8, PRODUCT_LANDSAT9):
            log(
                "Initializing Landsat Tile Service.",
                level=logging.DEBUG,
                collection_request=collection_request,
            )
            service = LandsatTileService()

            # Searching for Landsat tiles within the specified polygon and time range
            log(
                "Searching for Landsat Tiles within the specified polygon and time range.",
                level=logging.DEBUG,
                collection_request=collection_request,
            )
            results = service.search_by_polygon(
                product=product,
                start_date=collection_request.StartDate.date(),
                end_date=collection_request.EndDate.date(),
                polygon=collection_request.Extent,
            )
            # Determining the satellite based on the product type
            satellite = "landsat-8" if product == PRODUCT_LANDSAT8 else "landsat-9"
            # Filtering search results based on the satellite
            results = service.filter_results_by_satellite(
                result_dict=results, satellite=satellite
            )
        else:
            raise Exception(f"Unknown product: {product}")

        # Filtering out only last day image
        # results = service.filter_results(results)

        # Updating search results to the database and obtaining created tiles
        log(
            "Updating search results to the database and obtaining created tiles.",
            level=logging.DEBUG,
            collection_request=collection_request,
        )
        created_tiles = service.update_to_database(
            search_results=results, product=collection_request.Product
        )
        log(
            "Successfully Updated search results to the database and obtaining created tiles.",
            level=logging.DEBUG,
            collection_request=collection_request,
        )

        # Adding created tiles to the collection request
        log(
            "Adding Tiles to the collection request.",
            level=logging.DEBUG,
            collection_request=collection_request,
        )

        for created_tile in created_tiles:
            collection_request.ImageTiles.add(created_tile)

        log(
            "Tiles added Successfully to the collection request.",
            level=logging.DEBUG,
            collection_request=collection_request,
        )

        # Marking the search as successful if results are obtained
        if results:
            collection_request.IsSearchSuccessful = True
            log(
                "Set IsSearchSuccessful flag to True.",
                level=logging.DEBUG,
                collection_request=collection_request,
            )
            collection_request.save()

        return results
