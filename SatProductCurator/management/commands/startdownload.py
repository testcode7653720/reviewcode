from django.core.management.base import BaseCommand, CommandError

from SatProductCurator.services import SatelliteImageTileService
from SatProductCurator.models import SatelliteImageTile


class Command(BaseCommand):
    help = "Closes the specified poll for voting"

    def handle(self, *args, **options):

        tile = SatelliteImageTile.objects.get(id=43)

        service = SatelliteImageTileService()
        service.fetch_target_images(tile)
        return
