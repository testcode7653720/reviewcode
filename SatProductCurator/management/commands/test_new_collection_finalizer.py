from django.core.management.base import BaseCommand

from SatProductCurator.workers import NewCollectionFinalizerWorker


class Command(BaseCommand):
    help = "Closes the specified poll for voting"

    def handle(self, *args, **options):

        worker = NewCollectionFinalizerWorker()
        items = worker.find_eligible_items()

        for item in items:
            worker.start(item)
