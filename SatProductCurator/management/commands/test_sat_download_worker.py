from django.core.management.base import BaseCommand, CommandError

from SatProductCurator.workers import SatDownloadWorker


class Command(BaseCommand):
    help = "Closes the specified poll for voting"

    def handle(self, *args, **options):

        worker = SatDownloadWorker()
        items = worker.find_eligible_items()
        

        for item in items:
            worker.start(item)
