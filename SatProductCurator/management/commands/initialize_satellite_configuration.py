from django.core.management.base import BaseCommand

from SatProductCurator.models import SatelliteProviderConfiguration, SatelliteUsageDetails
from SatProductCurator.models.constants import PROVIDERS, PRODUCTS, PROVIDER_TO_PRODUCT


class Command(BaseCommand):
    help = "Closes the specified poll for voting"

    def handle(self, *args, **options):

        for provider in PROVIDERS:
            if not SatelliteProviderConfiguration.objects.filter(SATProviderName=provider[0]).exists():
                SatelliteProviderConfiguration.objects.create(
                    SATProviderName=provider[0]
                )
                print(f"Added SatelliteProviderConfiguration: {provider[0]}")
            else:
                print(f"Already exists SatelliteProviderConfiguration: {provider[0]}")

        for product in PRODUCTS:

            if not SatelliteUsageDetails.objects.filter(SatelliteProductName=product[0]).exists():
                SatelliteUsageDetails.objects.create(
                    SATProviderID=SatelliteProviderConfiguration.objects.get(SATProviderName=PROVIDER_TO_PRODUCT[product[0]]),
                    SatelliteProductName=product[0]
                )
                print(f"Added SatelliteUsageDetails: {product[0]}")
            else:
                print(f"Already exists SatelliteUsageDetails: {product[0]}")
