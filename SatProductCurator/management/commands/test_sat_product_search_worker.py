from django.core.management.base import BaseCommand, CommandError

from SatProductCurator.workers import SatProductSearchWorker

class Command(BaseCommand):
    help = "Closes the specified poll for voting"

    def handle(self, *args, **options):

        worker = SatProductSearchWorker()
        items = worker.find_eligible_items()
        print(items)

        for item in items:
            worker.start(item)
        
