from django.contrib import admin

from SatProductCurator.models.new_clip_request import NewClipRequest

from .models import (
    NewCollectionRequest,
    SatelliteImageTile,
    SatelliteProviderConfiguration,
    SatelliteUsageDetails,
)


class SatelliteImageTileAdmin(admin.ModelAdmin):
    list_display = (
        "tile_id",
        "SourceData",
        "to_be_downloaded",
        "is_downloaded",
        "is_extracted",
    )


class NewCollectionRequestAdmin(admin.ModelAdmin):
    list_display = (
        'StartDate',
        'EndDate',
        'Product',
        'IsSearchSuccessful',
        'IsDownloadCompleted',
        'IsIngestionCompleted'
    )


admin.site.register(NewCollectionRequest, NewCollectionRequestAdmin)
admin.site.register(SatelliteImageTile, SatelliteImageTileAdmin)
admin.site.register(SatelliteProviderConfiguration)
admin.site.register(SatelliteUsageDetails)
admin.site.register(NewClipRequest)
