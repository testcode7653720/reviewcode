def search_availability():
    pass


def start_download():
    pass


def request_to_be_available():
    pass


def ingest_to_database():
    pass


def start_sat_image_download():
    # Background process
    """Start downloading of images that are marked to be downloaded
    """

    get_service_provider()  # Polymorphism: We'll find the proper service provider and download accordingly
    search_availability()
    if available:
        start_download()
        ingest_to_database()
    else:
        request_to_be_available()
