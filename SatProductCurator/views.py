from rest_framework import viewsets
from .models import SatelliteProviderConfiguration
from SatProductCurator.serializers import SatProviderSerializer


class SatProviderViewSet(viewsets.ModelViewSet):

    serializer_class = SatProviderSerializer
    queryset = SatelliteProviderConfiguration.objects.all()

    def list(self, request, *args, **kwargs):
        """ View all the files."""
        return super().list(request, *args, **kwargs)
    '''
    @action(['GET'], url_path='search', detail=False)
    def search(self, request):
        service = SatProductService()
        service.search([''])
        return Response('Hello')

    @action(['GET'], url_path='search-by-boundary', detail=False)
    def search_by_boundary(self, request, boundary):
        pass

    @action(['GET'], url_path='search-by-id', detail=False)
    def search_by_id(self, request, id):
        pass

    # TODO Some more search methods as required

    @action(['GET'], url_path='search-by-id', detail=True)
    def request_to_download(self, request, id):
        """ Request the satellite product to be downloaded"""
        pass

    def approve_to_downloade(self, request, id):
        """ Approve the satellite product to be downloaded"""
        SatProduct.to_be_downloaded = True
        pass
      '''  
