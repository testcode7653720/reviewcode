from django.db import models


class BaseModel(models.Model):
    CreatedOn = models.DateTimeField(auto_now_add=True)
    CreatedBy = models.CharField(
        max_length=50, blank=True, null=True, default="NA")
    ModifiedOn = models.DateTimeField(auto_now=True)
    ModifiedBy = models.CharField(
        max_length=50, blank=True, null=True, default="NA")

    class Meta:
        abstract = True
