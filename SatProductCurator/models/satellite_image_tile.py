from django.contrib.gis.db import models

from IngestionEngine.models import TargetData, SourceData


class SatelliteImageTile(models.Model):
    Product = models.TextField()
    SourceData = models.OneToOneField(
        SourceData,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name="SatelliteImageTile",
    )
    TargetData = models.OneToOneField(
        TargetData,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name="SatelliteImageTile",
    )

    boundary = models.PolygonField()
    tile_id = models.TextField(unique=True)
    date = models.DateField()

    to_be_downloaded = models.BooleanField(default=False)
    is_downloaded = models.BooleanField(default=False)
    is_extracted = models.BooleanField(default=False)

    dl_attempts = models.IntegerField(default=0)
    dl_start_time = models.DateTimeField(null=True, blank=True)
    dl_end_time = models.DateTimeField(null=True, blank=True)

    filename = models.TextField(null=True, blank=True)
    download_path = models.TextField(null=True, blank=True)
    extracted_path = models.TextField(null=True, blank=True)

    eodag_data = models.JSONField(default=dict)
