from typing import Iterable
from django.db import models

from ._base_model import BaseModel
from .constants import PROVIDERS


class SatelliteProviderConfiguration(BaseModel):
    """Satellite Provider Configuration"""

    APIKey = models.TextField(blank=True, null=True)
    SATProviderID = models.AutoField(primary_key=True)
    SATProviderName = models.CharField(max_length=300, unique=True)
    SATProductName = models.CharField(max_length=300, blank=True, null=True)
    APIUrl = models.URLField(blank=True, null=True)
    Username = models.CharField(max_length=100, blank=True, null=True)
    Password = models.CharField(max_length=100, blank=True, null=True)
    SubscriptionStatus = models.TextField(blank=True, null=True)
    SubscriptionExpiryDate = models.DateTimeField(blank=True, null=True)
    Description = models.TextField(blank=True, null=True)

    def __str__(self):
        return str(self.SATProviderName)

    def save(self, *args, **kwargs) -> None:
        if self.SATProviderName not in {_[0] for _ in PROVIDERS}:
            raise Exception(f"Provider is not yet registered: {self.SATProviderName}")
        return super().save(*args, **kwargs)

    class Meta:
        db_table = "SatelliteProviderConfiguration"
