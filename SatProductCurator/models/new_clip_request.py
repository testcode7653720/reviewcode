import uuid
from django.contrib.gis.db import models

from IngestionEngine.models import TargetData, TargetImage

from ._base_model import BaseModel


class NewClipRequest(BaseModel):
    """New Collection Request"""

    ID = models.AutoField(auto_created=True, primary_key=True)
    # This key represents the external ID generated on the Penta B side for ClipRequestTable.
    NewClipID = models.UUIDField(default=uuid.uuid4, unique=True)

    MnAClipRequestID = models.TextField(null=True, blank=True, unique=True)
    # This key corresponds to the primary Key of TargetData in the MnA DB.
    MnAID = models.TextField(null=True, blank=True)
    Extent = models.PolygonField(null=True, blank=True)
    StartDate = models.DateTimeField(null=True, blank=True)
    EndDate = models.DateTimeField(null=True, blank=True)
    IsClipCompleted = models.BooleanField(default=False)
    ClipIngestionStatus = models.CharField(
        max_length=100, blank=True, null=True, default="NA"
    )
    ClippedDownloadURL = models.TextField(null=True, blank=True)
    ClippedVisualizationURL = models.TextField(null=True, blank=True)
    TargetImageID = models.ForeignKey(
        TargetImage, on_delete=models.CASCADE, blank=True, null=True
    )
    TargetDataID = models.ForeignKey(
        TargetData, on_delete=models.CASCADE, blank=True, null=True
    )
    ClipIngestedDate = models.DateTimeField(null=True, blank=True)
    Is_deleted_from_nas = models.BooleanField(default=False)
    ClippedTiffPath = models.TextField(null=True, blank=True)
    ClippedCOGPath = models.TextField(null=True, blank=True)

    class Meta:
        db_table = "NewClipRequest"
