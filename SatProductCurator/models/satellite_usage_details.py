import uuid
from django.db import models

from .satellite_provider_configuration import SatelliteProviderConfiguration


class SatelliteUsageDetails(models.Model):
    SATUsageID = models.AutoField(auto_created=True, primary_key=True)
    SatID = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)

    # This is PK from mna table for satellite usage.
    MnASatID = models.TextField(default="NA", null=True, blank=True)
    SATProviderID = models.ForeignKey(SatelliteProviderConfiguration, on_delete=models.CASCADE)
    SatelliteProvider = models.TextField(blank=True, null=True)
    SatelliteProductName = models.TextField(null=True, blank=True)
    SpatialResolutionRange = models.TextField(null=True, blank=True)
    Resolution = models.TextField(null=True, blank=True)
    ImageType = models.TextField(null=True, blank=True)
    SpectralBands = models.TextField(null=True, blank=True)
    Usage = models.TextField(null=True, blank=True)
    Industry = models.TextField(null=True, blank=True)
    SubscriptionStatus = models.TextField(null=True, blank=True)

    def __str__(self):
        return str(self.SatelliteProductName)

    class Meta:
        db_table = "SatelliteUsageDetails"
