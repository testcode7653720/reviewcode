import uuid
from django.contrib.gis.db import models

from IngestionEngine.models import TargetData

from ._base_model import BaseModel
from .satellite_image_tile import SatelliteImageTile


class NewCollectionRequest(BaseModel):
    """New Collection Request"""

    ID = models.AutoField(auto_created=True, primary_key=True)

    NewCollectionID = models.UUIDField(default=uuid.uuid4, unique=True)

    # This is the Primary Key of NewCollection of MnA DB.
    MnANewCollectionID = models.TextField(null=True, blank=True, unique=True)

    # This is the MnASatID of MnADB.
    MnASatID = models.TextField(null=True, blank=True)

    Product = models.TextField()
    Extent = models.PolygonField()
    StartDate = models.DateTimeField()
    EndDate = models.DateTimeField()

    IsAccepted = models.BooleanField(default=False)

    IsSearchSuccessful = models.BooleanField(default=False)
    IsMultipleDateImage = models.BooleanField(default=False)
    IsDownloadCompleted = models.BooleanField(default=False)
    IsIngestionCompleted = models.BooleanField(default=False)

    TargetDataID = models.ManyToManyField(TargetData, blank=True)
    ImageTiles = models.ManyToManyField(SatelliteImageTile, blank=True)

    class Meta:
        db_table = "NewCollectionRequest"
