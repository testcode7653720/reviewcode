# Generated by Django 4.2.4 on 2024-02-04 08:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('SatProductCurator', '0009_rename_ismultipleimages_newcollectionrequest_ismultipledateimage'),
    ]

    operations = [
        migrations.AlterField(
            model_name='satelliteproviderconfiguration',
            name='SubscriptionStatus',
            field=models.TextField(blank=True, null=True),
        ),
    ]
