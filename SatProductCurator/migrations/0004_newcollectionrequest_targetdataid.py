# Generated by Django 4.2.4 on 2023-12-04 10:20

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        (
            "IngestionEngine",
            "0006_remove_newcollectioninformation_targetdataid_and_more",
        ),
        (
            "SatProductCurator",
            "0003_rename_newcollectioninformation_newcollectionrequest_and_more",
        ),
    ]

    operations = [
        migrations.AddField(
            model_name="newcollectionrequest",
            name="TargetDataID",
            field=models.ManyToManyField(to="IngestionEngine.targetdata"),
        ),
    ]
