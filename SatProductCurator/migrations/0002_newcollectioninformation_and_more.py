# Generated by Django 4.2.4 on 2023-11-30 12:58

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("SatProductCurator", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="NewCollectionInformation",
            fields=[
                (
                    "NewCollectionID",
                    models.AutoField(
                        auto_created=True, primary_key=True, serialize=False
                    ),
                ),
                ("CreatedOn", models.DateTimeField(auto_now_add=True)),
                (
                    "CreatedBy",
                    models.CharField(
                        blank=True, default="NA", max_length=50, null=True
                    ),
                ),
                ("ModifiedOn", models.DateTimeField(auto_now=True)),
                (
                    "ModifiedBy",
                    models.CharField(
                        blank=True, default="NA", max_length=50, null=True
                    ),
                ),
                ("Extent", models.TextField(blank=True, null=True)),
                ("StartDate", models.DateTimeField(blank=True, null=True)),
                ("EndDate", models.DateTimeField(blank=True, null=True)),
                ("IsMultipleImages", models.BooleanField(default=False)),
                ("IsCompleted", models.BooleanField(default=False)),
                ("IsIngested", models.BooleanField(default=False)),
            ],
            options={
                "db_table": "NewCollectionInformation",
            },
        ),
        migrations.CreateModel(
            name="SatelliteProviderConfiguration",
            fields=[
                ("CreatedOn", models.DateTimeField(auto_now_add=True)),
                (
                    "CreatedBy",
                    models.CharField(
                        blank=True, default="NA", max_length=50, null=True
                    ),
                ),
                ("ModifiedOn", models.DateTimeField(auto_now=True)),
                (
                    "ModifiedBy",
                    models.CharField(
                        blank=True, default="NA", max_length=50, null=True
                    ),
                ),
                ("APIKey", models.TextField(blank=True, null=True)),
                ("SATProviderID", models.AutoField(primary_key=True, serialize=False)),
                ("SATProviderName", models.CharField(max_length=300)),
                (
                    "SATProductName",
                    models.CharField(blank=True, max_length=300, null=True),
                ),
                ("APIUrl", models.URLField()),
                ("Username", models.CharField(max_length=100)),
                ("Password", models.CharField(max_length=100)),
                ("SubscriptionStatus", models.CharField(max_length=100)),
                ("SubscriptionExpiryDate", models.DateTimeField(blank=True, null=True)),
                ("Description", models.TextField(blank=True, null=True)),
            ],
            options={
                "db_table": "SatelliteProviderConfiguration",
            },
        ),
        migrations.CreateModel(
            name="SatelliteUsageDetails",
            fields=[
                (
                    "SATUsageID",
                    models.AutoField(
                        auto_created=True, primary_key=True, serialize=False
                    ),
                ),
                ("SatelliteProductName", models.CharField(max_length=255)),
                ("SpatialResolution", models.TextField(blank=True, null=True)),
                ("ResolutioRange", models.TextField(blank=True, null=True)),
                ("ImageType", models.CharField(blank=True, max_length=255, null=True)),
                ("SpectralBands", models.TextField(blank=True, null=True)),
                ("Usage", models.TextField(blank=True, null=True)),
                ("Industry", models.TextField(blank=True, null=True)),
                ("SubscriptionStatus", models.CharField(max_length=100)),
                (
                    "SATProviderID",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="SatProductCurator.satelliteproviderconfiguration",
                    ),
                ),
            ],
            options={
                "db_table": "SatelliteUsageDetails",
            },
        ),
        migrations.DeleteModel(
            name="SatProduct",
        ),
        migrations.AddField(
            model_name="newcollectioninformation",
            name="SATUsageID",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                to="SatProductCurator.satelliteusagedetails",
            ),
        ),
    ]
