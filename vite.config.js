// vite.config.js
import path from 'path';


export default {
    build: {
        rollupOptions: {
            input: {
                'entry-point-a': path.resolve(__dirname, 'src/main.js'),
            },
            output: {
                dir: 'dist/',
                entryFileNames: 'index.js',
                chunkFileNames: "chunk.js",
                manualChunks: undefined,
                assetFileNames: (assetInfo) => {
                    return "assets/" + assetInfo.name;
                },
            }
        }
    },
}