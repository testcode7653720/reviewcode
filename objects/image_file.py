class ImageFile:

    def __init__(self, hot_path=None, path=None) -> None:
        self.hot_path = hot_path
        self.path = path
    
    def upload(self, to_path=''):
        if to_path:
            self.path = to_path
        return
    
    