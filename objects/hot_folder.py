from .image_file import ImageFile

class HotFolder:

    def __init__(self, path: str) -> None:
        self.path = path
        self.connection = None

    def connect(self):
        pass

    @classmethod
    def _connect_with_ftp(cls, ftp_path):
        pass

    @classmethod
    def _connect_with_local(cls, folder_location):
        pass

    @classmethod
    def _connect_with_vpn(cls, vpn_attrs):
        pass
    
    def search_images(self) -> list(ImageFile):
        return