import logging
import uuid
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import viewsets
from IngestionEngine.models import (
    SourceData,
    TargetData,
    BusinessMetadata,
    TargetImage,
)
from MnaApp.serializers.clip_request_serializer import NewClipRequestSerializer
from MnaApp.serializers.hot_files_serializers import SourceDataSerializer
from MnaApp.serializers.targetdata_serializer import TargetDataSerializer
from MnaApp.serializers.metadata_serializer import BusinessMetadataSerializer
from MnaApp.serializers import (
    TargetDataMnAUpdateSerializer,
    TargetDataCTPISerializer,
    CostEstimationByImageSerializer,
    CostEstimationByParamsSerializer,
)
from rest_framework.decorators import (
    api_view,
    authentication_classes,
    permission_classes,
)
from rest_framework.response import Response
import requests
from django.db.models import Q
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token
from rest_framework.decorators import action
from django.contrib.auth import authenticate
from MnaApp.base_logger import Logger
from shapely.geometry import Polygon
from shapely import wkt
from shapely.wkt import loads


from CostEstimator.services import CostEstimatorService
from CostEstimator.models import CostFactor

log = Logger("mna-app").get_logger()


def is_valid_uuid(uuid_string):
    try:
        uuid.UUID(uuid_string)
        return True
    except ValueError:
        return False


def validate_extent(extent):
    if "type" not in extent:
        return False

    coordinates = extent["coordinates"]

    if "coordinates" not in extent or not coordinates or not coordinates[0]:
        return False

    # Check if coordinates is a list
    if not isinstance(coordinates, list):
        return False

    # Check if coordinates is a list of lists
    for coord_list in coordinates:
        if not isinstance(coord_list, list):
            return False
        for coord in coord_list:
            if not isinstance(coord, list) or len(coord) != 2:
                return False
    else:
        return True


class SourceDataViewSet(viewsets.ModelViewSet):
    serializer_class = SourceDataSerializer
    queryset = SourceData.objects.all()


class BusinessMetadataViewSet(viewsets.ModelViewSet):
    serializer_class = BusinessMetadataSerializer
    queryset = BusinessMetadata.objects.all()

    def list(self, request, *args, **kwargs):
        """View all the files."""
        return super().list(request, *args, **kwargs)


class TargetDataViewSet(viewsets.ModelViewSet):
    serializer_class = TargetDataSerializer
    queryset = TargetData.objects.all()

    def list(self, request, *args, **kwargs):
        """View all the ingested files."""
        return super().list(request, *args, **kwargs)


class CostEstimationView(viewsets.ViewSet):
    authentication_classes = []
    permission_classes = []

    @action(["POST"], detail=False, url_path="by-image-and-extent")
    def by_image_and_extent(self, request):
        """This API will calculate cost for clipping image."""

        log("Calculating image cost initiated.", level=logging.DEBUG)

        serializer = CostEstimationByImageSerializer(data=request.data)
        if not serializer.is_valid():
            log(f"Request failed because: {serializer.errors}", level=logging.ERROR)

            response_data = {
                "code": 400,
                "message": "Bad request",
                "error": serializer.errors,
                "success": False,
            }

            return Response(response_data, status=status.HTTP_400_BAD_REQUEST)

        log("Retrieving TargetData and target image", level=logging.DEBUG)

        try:
            target_data = TargetData.objects.get(
                TargetDataID=request.data["TargetDataID"], MnAID=request.data["MnAID"]
            )
            target_image = TargetImage.objects.get(
                ImageID=request.data["TargetImageID"][0], TargetData=target_data
            )
        except ObjectDoesNotExist as e:
            response_data = {
                "code": 404,
                "message": "Data not found",
                "error": str(e),
                "success": False,
            }
            log(f"Request failed because: {response_data}", level=logging.ERROR)
            return Response(response_data, status=status.HTTP_404_NOT_FOUND)

        if not target_image.TargetData.BusinessMetadata.Extent.intersects(
            serializer.validated_data["Extent"]
        ):
            response_data = {
                "code": 400,
                "message": "Extent doesn't overlaps with the given image",
                "error": "Extent doesn't overlaps with the given image",
                "success": False,
            }
            log(f"Request failed because: {response_data}", level=logging.ERROR)
            return Response(response_data, status=response_data["code"])

        cost_estimator_service = CostEstimatorService()
        cost_estimate = cost_estimator_service.calculate_cost_for_image(
            target_image=target_image
        )

        # Return the cost estimate and MnA Image primary key (for demonstration purposes)
        log(
            "Return the cost estimate and MnA Image primary key in response.",
            target_data=target_data,
            level=logging.DEBUG,
        )

        response_data = {
            "Total_Cost": cost_estimate,
            "MnAID": target_data.MnAID,
            "success": "true",
            "code": 201,
        }
        log(
            f"Image cost calculated successfully. {response_data}",
            target_data=target_data,
            level=logging.DEBUG,
        )
        return Response(response_data, status=201)

    @action(["POST"], detail=False, url_path="by-params")
    def by_params(self, request):
        """API to get the cost using some parameters."""

        serializer = CostEstimationByParamsSerializer(data=request.data)
        if not serializer.is_valid():
            log(f"Request failed because: {serializer.errors}", level=logging.ERROR)
            response_data = {
                "code": 400,
                "message": "Bad request",
                "error": serializer.errors,
                "success": False,
            }
            return Response(response_data, status=status.HTTP_400_BAD_REQUEST)

        try:
            CostFactor.objects.get(
                FactorType=CostFactor.COST_FACTOR_BAND,
                FactorName=serializer.data["band"],
            )
        except CostFactor.DoesNotExist as e:
            log(f"Request failed because: {e}", level=logging.ERROR)
            response_data = {
                "code": 400,
                "message": f"Band {serializer.data['band']} not found.",
                "error": str(e),
                "success": False,
            }
            return Response(response_data, status=status.HTTP_400_BAD_REQUEST)

        service = CostEstimatorService()
        cost_estimate = service.calculate_cost(
            area_in_km2=serializer.data["area_in_km2"],
            band=serializer.data["band"],
            resolution=serializer.data["resolution"],
        )

        response_data = {
            "Total_Cost": cost_estimate,
            "success": "true",
            "code": 201,
        }
        log(
            f"Image cost calculated successfully. {response_data}",
            level=logging.DEBUG,
        )
        return Response(response_data, status=201)


# This method consume mna-api and push metadata into mna system.


def insert_into_mna(request):
    try:
        log("Method Started successfully", level=logging.DEBUG)
        target_data_records = TargetData.objects.filter(
            Q(IADSIngestionStatus="IngestionCompleted")
            & Q(CTPIIngestionStatus="ReadyForCTPIIngestion")
        )

        errors = []
        success_count = 0

        for record in target_data_records:
            record.CTPIIngestionStatus = "CTPIIngestionInProgress"
            record.save()
            business_metadata = BusinessMetadata.objects.get(TargetData=record)
            data_to_send = {
                "MnADatasetName": record.TargetDatasetName,
                "extent": business_metadata.extent,
                "epsg_code": business_metadata.epsg_code,
                "Keywords": business_metadata.Keywords,
                "Type": business_metadata.Type,
            }
            try:
                # Make API Requests
                response = requests.post(
                    "http://127.0.0.1:8000/mna-app/mna-target-data/",
                    json=data_to_send,
                )
            except requests.RequestException as e:
                logger.error(f"Request failed: {e}")

            if response.status_code == 201:
                mna_id = response.json().get("MnAID")

                # Update TargetData Records
                record.MnAID = mna_id
                record.CTPIIngestionStatus = "CTPIIngestionCompleted"
                record.SourceData.CTPIIngestionStatus = "CTPIIngestionCompleted"
                record.SourceData.save()
                record.save()

                success_count += 1
            else:
                errors.append(f"Failed to add data to MNA: {response.content}")

        if errors:
            return JsonResponse({"errors": errors}, status=500)
        else:
            return JsonResponse(
                {"message": f"{success_count} records added to MNA successfully."},
                status=200,
            )

    except Exception as e:
        return JsonResponse({"error": str(e)}, status=500)


# This is Update API from IADS Coderize which will consume by MnA system to update Target Metadata.
@api_view(["PUT"])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def update_target_metadata(request, MnAID):
    log("Inside the update_target_metadata API.", level=logging.DEBUG)
    try:
        log(
            "Trying to retrieve TargetData associated with MnAID and TargetDataID.",
            level=logging.DEBUG,
        )
        log("Getting TargetData ID from request.", level=logging.DEBUG)
        TargetDataID = request.data.get("TargetDataID")
        log(f"TargetDataID from request is:{TargetDataID}", level=logging.DEBUG)

        target_data = TargetData.objects.get(MnAID=MnAID, TargetDataID=TargetDataID)
    except TargetData.DoesNotExist:
        log(
            f"TargetData associated with MnAID {MnAID} does not exist.",
            level=logging.ERROR,
        )
        return Response(
            {"BadRequest": "TargetData Associated with provided MnAID does not Exist."},
            status=status.HTTP_404_NOT_FOUND,
        )

    try:
        # Retrieving the BusinessMetadata instance associated with the TargetData
        log(
            f"Retrieving BusinessMetadata for TargetDataID: {target_data.TargetDataID}",
            target_data=target_data,
            level=logging.DEBUG,
        )

        business_metadata = BusinessMetadata.objects.get(TargetData=target_data)
        log(
            f"Retrieved BusinessMetadata for TargetDataID: {target_data.TargetDataID}",
            target_data=target_data,
        )
    except BusinessMetadata.DoesNotExist:
        log(
            f"BusinessMetadata associated with MnAID {MnAID} does not exist.",
        )
        return Response(
            {
                "BadRequest": "BusinessMetadata associated with provided MnAID does not Exist."
            },
            status=status.HTTP_400_BAD_REQUEST,
        )

    log(
        "Initializing serializer with retrieved TargetData and request data.",
        target_data=target_data,
        level=logging.DEBUG,
    )
    serializer = TargetDataMnAUpdateSerializer(target_data, data=request.data)
    if serializer.is_valid(raise_exception=True):
        log(
            "Serializer data is valid. Saving serializer data.",
            target_data=target_data,
            level=logging.DEBUG,
        )

        serializer.save()
        data = {
            "code": 200,
            "message": "Image Metadata Updated Successfully in IADS Business Metadata Table.",
            "success": "true",
        }

        return Response(data, status=200)


# View for initiating clip request.
@api_view(["POST"])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def initiate_new_clip_request(request):
    log("API Started Successfully.", level=logging.DEBUG)

    # Check if MnaClipRequestID and MNAID are present in the request data
    if "MnAClipRequestID" not in request.data or not is_valid_uuid(
        request.data.get("MnAClipRequestID")
    ):
        error_message = (
            "Either MnAClipRequestID not present in request or not a valid UUID."
        )
        log(error_message, level=logging.ERROR)
        response_data = {
            "code": 400,
            "message": "Invalid request data.",
            "error": error_message,
            "success": False,
        }
        return Response(response_data, status=status.HTTP_400_BAD_REQUEST)
    elif "MnAID" not in request.data or not is_valid_uuid(request.data.get("MnAID")):
        error_message = "Either MnAID not present in request or not a valid UUID."
        log(error_message, level=logging.ERROR)
        response_data = {
            "code": 400,
            "message": "Invalid request data.",
            "error": error_message,
            "success": False,
        }
        return Response(response_data, status=status.HTTP_400_BAD_REQUEST)

    else:
        try:
            extent_request = request.data.get("Extent")

            if (
                "coordinates" not in extent_request
                or not extent_request["coordinates"]
                or not extent_request["coordinates"][0]
            ):
                response_data = {
                    "code": 400,
                    "message": "Invalid Extent.",
                    "error": "Extent has empty coordinates.",
                    "success": False,
                }
                return Response(response_data, status=status.HTTP_400_BAD_REQUEST)

            target_data = TargetData.objects.get(
                TargetDataID=request.data["TargetDataID"]
            )
            log(
                "Retrieved Target Data from Target Data table",
                target_data=target_data,
                level=logging.DEBUG,
            )

            target_image = TargetImage.objects.get(
                ImageID=request.data["TargetImageID"][0]
            )

            # Extract the UUID from the list if it's wrapped in a list
            request_target_image_id = (
                request.data["TargetImageID"][0]
                if isinstance(request.data["TargetImageID"], list)
                else request.data["TargetImageID"]
            )

            if str(target_image.ImageID) != str(request_target_image_id):
                response_data = {
                    "code": 400,
                    "message": "Invalid request data.",
                    "error": "TargetImageID must be related to TargetDataID.",
                    "success": False,
                }
                return Response(response_data, status=status.HTTP_400_BAD_REQUEST)

            extent_request = request.data.get("Extent")

            log(
                f"Extent value from request is: {extent_request}",
                target_data=target_data,
                level=logging.DEBUG,
            )

            target_extent = target_data.BusinessMetadata.Extent

            log(
                f"Target extent is:{target_extent}",
                target_data=target_data,
                level=logging.DEBUG,
            )

            is_valid_extent = validate_extent(extent_request)

            if not is_valid_extent:
                response_data = {
                    "code": 400,
                    "message": "Invalid Extent.",
                    "error": "Either Extent has empty coordinates or Extent is out of boundries of target image.",
                    "success": False,
                }
                return Response(response_data, status=status.HTTP_400_BAD_REQUEST)

            request.data["TargetDataID"] = target_data.ID

            request.data["TargetImageID"] = target_image.ID

        except Exception as e:
            error_message = str(e)
            log(f"Error is: {error_message}", level=logging.ERROR)
            response_data = {
                "code": 400,
                "message": "Invalid request data.",
                "error": "Please enter a valid TargetDataID and TargetImageID both.",
                "success": "false",
            }
            return Response(response_data, status=status.HTTP_400_BAD_REQUEST)

    if target_data.MnAID != request.data["MnAID"]:
        response_data = {
            "code": 400,
            "message": "Invalid request data.",
            "error": "Incorrect MnAID for TargetDataID in request.",
            "success": "false",
        }
        return Response(response_data, status=status.HTTP_400_BAD_REQUEST)

    # Extracting necessary information from the request
    log("Initiating Clip Request.", target_data=target_data, level=logging.DEBUG)
    serializer = NewClipRequestSerializer(data=request.data)

    if serializer.is_valid(raise_exception=True):
        log(
            "Serializer data is valid. Saving data.",
            target_data=target_data,
            level=logging.DEBUG,
        )
        serializer.save()
        new_clip_id = serializer.instance.NewClipID
        response_data = {
            "code": 201,
            "message": "New Clip Request from MnA initiated successfully.",
            "new_clip_id": new_clip_id,
            "success": "true",
        }
        log(
            "Clip request initiated successfully.",
            target_data=target_data,
            level=logging.DEBUG,
        )
        return Response(response_data, status=status.HTTP_201_CREATED)

    else:
        log("Clip request failed.", target_data=target_data, level=logging.DEBUG)
        log(
            f"Invalid request data: {serializer.errors}",
            target_data=target_data,
            level=logging.DEBUG,
        )
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(["GET"])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def mna_ingestion_example(request, MnAID):
    try:
        # get the TargetData based on mna_id
        log("API started successfully.", level=logging.DEBUG)
        log(f"Retrieving TargetData for MnAID: {MnAID}", level=logging.DEBUG)
        target_data = TargetData.objects.get(MnAID=MnAID)
        log(f"Retrieved TargetData for MnAID: {MnAID}", level=logging.DEBUG)

    except TargetData.DoesNotExist:
        log(
            f"TargetData associated with MnAID {MnAID} does not exist.",
            level=logging.DEBUG,
        )
        return Response(
            {"BadRequest": "TargetData Associated with provided MnAID does not exist."},
            status=status.HTTP_400_BAD_REQUEST,
        )
    serializer = TargetDataCTPISerializer(target_data)
    return Response(serializer.data)


class ObtainAuthToken(APIView):
    def post(self, request, *args, **kwargs):
        username = request.data.get("username")
        password = request.data.get("password")

        if username is None or password is None:
            return Response(
                {"error": "Please provide both username and password"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        user = authenticate(username=username, password=password)

        if not user:
            return Response(
                {"error": "Invalid Credentials"}, status=status.HTTP_401_UNAUTHORIZED
            )

        token, created = Token.objects.get_or_create(user=user)
        return Response({"token": token.key})
