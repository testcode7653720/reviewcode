from rest_framework import serializers
from IngestionEngine.models import TargetData
from .metadata_serializer import (
    BusinessMetadataMnAUpdateSerializer,
    BusinessMetadataCTPISerializer,
)
from .target_images_serializer import TargetImageCTPISerializer


class TargetDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = TargetData
        fields = "__all__"


class TargetDataMnAUpdateSerializer(serializers.ModelSerializer):
    """When MnA sends data, use this"""

    BusinessMetadata = BusinessMetadataMnAUpdateSerializer()

    class Meta:
        model = TargetData
        fields = (
            "TargetDataID",
            "MnAID",
            "TargetDatasetName",
            "TargetDatasetPath",
            "BusinessMetadata",
        )
        depth = 1

    def update(self, instance, validated_data):
        business_metadata = instance.BusinessMetadata
        business_metadata_data = validated_data.pop("BusinessMetadata")
        serializer = BusinessMetadataMnAUpdateSerializer(
            business_metadata, data=business_metadata_data
        )
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return super().update(instance, validated_data)


class TargetDataCTPISerializer(serializers.ModelSerializer):
    """When we ingest data into MnA, use this"""

    BusinessMetadata = BusinessMetadataCTPISerializer()
    TargetImages = TargetImageCTPISerializer(many=True)

    class Meta:
        model = TargetData
        fields = (
            "TargetDataID",
            "TargetDatasetName",
            "BusinessMetadata",
            "TargetImages",
        )
        depth = 1
