from rest_framework import serializers
from IngestionEngine.models import TargetImage


class TargetImageCTPISerializer(serializers.ModelSerializer):
    class Meta:
        model = TargetImage
        fields = (
            "SpatialResolution",
            "ImageID",
            "DownloadURL",
            "VisualizationURL",
            "BandName",
            "SalePrice",
        )
