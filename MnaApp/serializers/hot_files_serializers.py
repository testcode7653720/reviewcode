from rest_framework import serializers

from IngestionEngine.models import SourceData


class SourceDataSerializer(serializers.ModelSerializer):

    class Meta:
        model = SourceData
        fields = '__all__'
