from rest_framework import serializers
from SatProductCurator.models import NewClipRequest
from rest_framework_gis.serializers import GeometryField


class NewClipRequestSerializer(serializers.ModelSerializer):
    Extent = GeometryField()
    # MnAClipRequestID = serializers.UUIDField(required=True)

    class Meta:
        model = NewClipRequest
        fields = (
            "MnAClipRequestID",
            "Extent",
            "TargetImageID",
            "TargetDataID",
            "MnAID",
        )


class ClipRequestInsertMnASerializer(serializers.ModelSerializer):
    class Meta:
        model = NewClipRequest
        fields = (
            "NewClipID",
            "MnAClipRequestID",
            "MnAID",
            "TargetDataID",
            "TargetImageID",
            "ClippedDownloadURL",
            "ClippedVisualizationURL",
        )
