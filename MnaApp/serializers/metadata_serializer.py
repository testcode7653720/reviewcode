from rest_framework import serializers

from IngestionEngine.models import BusinessMetadata


class BusinessMetadataSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessMetadata
        fields = "__all__"


class BusinessMetadataMnAUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessMetadata
        fields = (
            "Keywords",
            "Type",
            "Thesaurus",
            "OrganizationName",
            "Abstract",
            "AccessConstraints",
            "AlternateTitle",
            "Capabilities",
            "ContactPersonName",
            "DataSets",
            "Industries",
            "IndustryUsage",
            "LicenseBasedConstraints",
            "OtherConstraints",
            "PlatformIdentifier",
            "SpecializedConstraints",
            "Title",
            "UseConstraints",
        )


class BusinessMetadataCTPISerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessMetadata
        fields = (
            "Extent",
            "EpsgCode",
            "NumPixels",
            "NumLayers",
            "HorizontalResolution",
            "Keywords",
            "Type",
            "Date",
            "PlatformType",
            "PlatformName",
            "Thesaurus",
            "ImagePlatforms",
            "CRSCoordinateReference",
            "HorizontalDatum",
            "VerticalDatum",
            "SensorType",
            "AcquisitionDate",
            "SourceName",
            "Description",
            "ProductType",
            "ProcessingLevel",
            "Custodian",
            "LicensingLevel",
            "TileID",
            "DownloadURL",
            "VisualizationURL",
            "Abstract",
            "AccessConstraints",
            "AlternateTitle",
            "Capabilities",
            "CloudCover",
            "ContactPersonName",
            "DataThemes",
            "DataSets",
            "PlatformDescription",
            "DistributionFormat",
            "DistributionLimitations",
            "ProviderOrganizationName",
            "Distributor",
            "EmailAddress",
            "ImageAcquisitionDateRange",
            "ImageFormat",
            "ImageType",
            "Industries",
            "IndustryUsage",
            "LicenseBasedConstraints",
            "OrganizationName",
            "OtherConstraints",
            "Identifier",
            "PlatformIdentifier",
            "Resolution",
            "ResolutionRange",
            "SensorMode",
            "SpecializedConstraints",
            "Title",
            "TransferSizeUnit",
            "UseConstraints",
            "LayerName",
            "LayerDisplayName",
            "DataLicensing",
            "RasterDataType",
            "Sensors",
            "ContactNumber",
            "Scale",
        )
