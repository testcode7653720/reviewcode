from rest_framework import serializers
from rest_framework_gis.serializers import GeometryField


class CostEstimationByImageSerializer(serializers.Serializer):

    MnAID = serializers.UUIDField()
    Extent = GeometryField()
    TargetDataID = serializers.UUIDField()
    TargetImageID = serializers.ListField(child=serializers.UUIDField())

    def validate(self, data):
        allowed_fields = set(self.fields.keys())
        received_fields = set(self.initial_data.keys())
        extra_fields = received_fields - allowed_fields

        if extra_fields:
            raise serializers.ValidationError(f"Extra fields not allowed: {', '.join(extra_fields)}")

        return super().validate(data)


class CostEstimationByParamsSerializer(serializers.Serializer):

    area_in_km2 = serializers.IntegerField()
    band = serializers.CharField()
    resolution = serializers.FloatField()
