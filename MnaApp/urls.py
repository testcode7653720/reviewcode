from django.urls import path, include
from rest_framework import routers

from .views import BusinessMetadataViewSet, SourceDataViewSet, mna_ingestion_example, CostEstimationView
from MnaApp import views
from .views import ObtainAuthToken


router = routers.SimpleRouter()
router.register(r"source_data", SourceDataViewSet)
router.register(r"metadata", BusinessMetadataViewSet)
router.register('estimate-cost', CostEstimationView, basename='estimate-cost')


urlpatterns = [
    path("", include(router.urls)),
    path("insert-into-mna/", views.insert_into_mna),
    path(
        "update-target-metadata/<str:MnAID>",
        views.update_target_metadata,
        name="update_target_metadata",
    ),
    path(
        "initiate-new-clip-request/",
        views.initiate_new_clip_request,
        name="initiate_new_clip_request",
    ),
    path("mna-example/<str:MnAID>", mna_ingestion_example),
    path('token/', ObtainAuthToken.as_view(), name='api-token'),
]
