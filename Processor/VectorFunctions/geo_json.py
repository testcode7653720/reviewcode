import geopandas
import logging

logger = logging.getLogger('django')


class GeoJSON:

    def __init__(self, geo_json: dict) -> None:
        self.geo_json = geo_json

    def to_dataframe(self):
        """ Covert GeoJSON to dataframe."""

        try:
            crs = self.geo_json['crs']['properties']['name']
        except KeyError:
            logger.warn("No CRS found in GeoJSON, setting 4326 be default.")
            crs = 4326

        return geopandas.GeoDataFrame.from_features(self.geo_json["features"], crs=crs)
