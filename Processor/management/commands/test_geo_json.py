import os
import pystac
import shutil
import geojson
from django.core.management.base import BaseCommand

from Processor.services import ClipperService
from Processor.VectorFunctions import GeoJSON


class Command(BaseCommand):
    help = "Closes the specified poll for voting"

    def handle(self, *args, **options):

        geo_json = {"type2": "Feature"}

        data  = geojson.Feature(**geo_json)
        print(data)
        print(data.is_valid) 

        # geo_json = GeoJSON(geo_json=geo_json)
        # validation = geo_json.validate()
        # print(validation)
