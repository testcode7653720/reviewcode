import os
import pystac
import shutil

from django.core.management.base import BaseCommand

from Processor.services import ClipperService
from Processor.VectorFunctions import GeoJSON


class Command(BaseCommand):
    help = "Closes the specified poll for voting"

    def handle(self, *args, **options):

        input_path = r"/home/nitishwsl/temp/sdfsdf.jp2"
        output_path = r'/home/nitishwsl/temp/test_data.tif'

        geo_json = GeoJSON(geo_json={
            "type": "FeatureCollection",
            "name": "temp_geojson",
            "crs": {
                "type": "name",
                "properties": {
                    "name": "urn:ogc:def:crs:OGC:1.3:CRS84"
                }
            },
            "features": [
                {
                    "type": "Feature",
                    "properties": {
                        "id": 1
                    },
                    "geometry": {
                        "type": "MultiPolygon",
                        "coordinates": [
                            [
                                [
                                    [
                                        74.28398125843043,
                                        19.265918652465025
                                    ],
                                    [
                                        74.486329169658816,
                                        19.247886817518115
                                    ],
                                    [
                                        74.536818400888492,
                                        19.040935338494421
                                    ],
                                    [
                                        74.287535719246662,
                                        19.050121669993494
                                    ],
                                    [
                                        74.218929036852813,
                                        19.133423612520609
                                    ],
                                    [
                                        74.28398125843043,
                                        19.265918652465025
                                    ]
                                ]
                            ]
                        ]
                    }
                }
            ]
        })

        input_gdf = geo_json.to_dataframe()
        print(input_gdf.crs)

        service = ClipperService()
        service.clip_using_dataframe(
            input_path=input_path,
            input_gdf=input_gdf,
            output_path=output_path
        )
