import rasterio
from shapely.geometry import mapping
from shapely import wkt
from geopandas import GeoDataFrame
from rasterio.mask import mask

from django.contrib.gis.geos import Polygon

from SatProductCurator.models.new_clip_request import NewClipRequest
import logging
from IngestionEngine.workers._base_logger import Logger

log = Logger("ClipperService").get_logger()


class ClipperService:
    def __init__(self) -> None:
        pass

    def clip_using_dataframe(
        self,
        input_path: str,
        input_gdf: GeoDataFrame,
        output_path: str,
        clip_request: NewClipRequest,
    ):
        """Clip the image using geo dataframe"""

        # Open the input raster file
        log(
            "Open the file using rasterio.",
            clip_request=clip_request,
            level=logging.DEBUG,
        )
        with rasterio.open(input_path) as src:
            # Re-project the GeoJSON to match the raster CRS if needed

            log(
                "Re-projecting the GeoJSON to match the raster CRS if needed.",
                clip_request=clip_request,
                level=logging.DEBUG,
            )
            input_gdf = input_gdf.to_crs(src.crs)

            # Convert GeoDataFrame to GeoJSON format
            log(
                "Converting GeoDataFrame to GeoJSON format.",
                clip_request=clip_request,
                level=logging.DEBUG,
            )
            geoms = input_gdf.geometry.values

            # Convert the GeoJSON geometries to a format compatible with rasterio
            log(
                "Converting the GeoJSON geometries to a format compatible with rasterio.",
                clip_request=clip_request,
                level=logging.DEBUG,
            )

            # Clip the input raster using the provided shapes
            log(
                "Cliping the input raster using the provided shapes.",
                clip_request=clip_request,
                level=logging.DEBUG,
            )
            shapes = [mapping(geom) for geom in geoms]

            out_image, out_transform = mask(src, shapes, crop=True, invert=False)
            out_meta = src.meta

        # Update metadata of the output raster
        out_meta.update(
            {
                "driver": "GTiff",
                "height": out_image.shape[1],
                "width": out_image.shape[2],
                "transform": out_transform,
            }
        )

        with rasterio.open(output_path, "w", **out_meta) as dest:
            dest.write(out_image)

        # Return the path to the clipped raster
        log(
            "Returning clipped image path.",
            clip_request=clip_request,
            level=logging.DEBUG,
        )
        return output_path

    def polygon_to_dataframe(self, polygon: Polygon, clip_request: NewClipRequest):
        """Convert Django polygon to Geo Dataframe"""
        log(
            "Converting Django polygon to Geo Dataframe.",
            clip_request=clip_request,
            level=logging.DEBUG,
        )
        return GeoDataFrame(
            [{"geometry": wkt.loads(polygon.wkt)}],
            geometry="geometry",
            crs=polygon.srs.srid,
        )
