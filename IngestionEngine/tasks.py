import os
import logging

from django.conf import settings

from IngestionEngine.services import SourceDataService

from .models import SourceData

logger = logging.getLogger('ingestion-engine')
from celery import shared_task


def generate_uuid():
    pass


def get_cog_path():
    pass


def insert_into_catalog():
    pass


def read_metadata():
    pass


def start_cog_conversion():
    pass


def look_file_changes():
    # Background task
    """This code executes every 10 minutes to check if any new files have been added
    At 4 PM
    """
    logger.info("Looking for file changes")

    for filename in os.listdir(SourceData.HOT_FOLDER_PATH):
        file_path = os.path.join(SourceData.HOT_FOLDER_PATH, filename)
        if os.path.isfile(file_path):

            if SourceData(raw_path=file_path).exists:
                source_data.size = 10
            else:
                source_data = SourceData(raw_path=file_path, size=10)
            source_data.save()


def start_ingestion_engine():
    # Background task
    """This starts the ingestion engine, starts looking for files in the folder.
    This doesn't convert to COG on its own, but stores the upcoming path.
    At 5 PM
    """

    logger.info("Starting Ingestion Engine")

    catalog_path = settings.CATALOG_PATH
    for source_data in source_data:
        # Check if hot file is ready to process
        if True:
            source_data.ingestion_started = True
            source_data.uuid = generate_uuid()
            cog_path = get_cog_path()   # Not actual conversion, just get the path
            source_data.metadata = read_metadata()
            insert_into_catalog(catalog_path, cog_path)
            source_data.save()

            start_cog_conversion()  # This may or may no be background task
        else:
            pass


def start_data_copying():
    # Background task
    """This will start converting and copying data to destination path"""

    logger.info("Start copyign data")
    if completed:
        source_data.ingestion_complete = True
        source_data.ingestion_completed_at = now()
        source_data.save()
    else:
        source_data.ingestion_completed = False
        source_data.ingestion_attempts += 1

@shared_task
def ingest_source_data_async(source_data_id):
    
    source_data = SourceData.objects.get(id=source_data_id)
    source_data_service = SourceDataService()
    source_data_service.ingest(source_data)
        
    return f"Ingestion of source_data {source_data.path} completed successfully."
    
