import time
from django.core.management.base import BaseCommand, CommandError
from IngestionEngine.models import SourceData, TargetData
from IngestionEngine.workers import FileWatcherWorker


class Command(BaseCommand):
    help = "Start Ingesting Files"

    def handle(self, *args, **options):
        start_process_worker = FileWatcherWorker()
        start_process_worker.start()
        time.sleep(10)
