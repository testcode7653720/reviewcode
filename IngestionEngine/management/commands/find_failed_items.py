import logging
from django.core.management.base import BaseCommand

from IngestionEngine.workers.find_failed_item_worker import FindFailedItemWorker


logger = logging.getLogger("ingestion-engine")


class Command(BaseCommand):
    def handle(self, *args, **options):
        worker = FindFailedItemWorker()
        in_progress_items = worker.find_eligible_items()

        for source_data in in_progress_items:
            worker.start(source_data)
