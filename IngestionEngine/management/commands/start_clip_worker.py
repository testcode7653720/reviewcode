import logging
from django.core.management.base import BaseCommand
from IngestionEngine.workers.ctpi_clipper_worker import CTPIClipperWorker


logger = logging.getLogger("ingestion-engine")


class Command(BaseCommand):
    help = "Start Pre ingestion to source data"

    def handle(self, *args, **options):
        
        ctpi_worker = CTPIClipperWorker()
        clip_requests = ctpi_worker.find_eligible_items()
        for clip_request in clip_requests:
            ctpi_worker.start(clip_request)
