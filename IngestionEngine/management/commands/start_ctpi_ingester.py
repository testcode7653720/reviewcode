import logging
from django.core.management.base import BaseCommand
from IngestionEngine.workers.ctpi_metadata_ingester import CTPIIngesterWorker


logger = logging.getLogger("ingestion-engine")


class Command(BaseCommand):
    help = "Start Pre ingestion to source data"

    def handle(self, *args, **options):
        
        ctpi_worker = CTPIIngesterWorker()
        new_requests = ctpi_worker.find_eligible_items()
        for new_request in new_requests:
            ctpi_worker.start(new_request)
