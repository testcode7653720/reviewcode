from SatProductCurator.workers.sat_usage_details_ingester import SatUsageIngesterWorker
import logging
from django.core.management.base import BaseCommand

logger = logging.getLogger("ingestion-engine")


class Command(BaseCommand):
    help = "Start Pre ingestion to source data"

    def handle(self, *args, **options):
        sat_ingester_worker = SatUsageIngesterWorker()
        sat_items = sat_ingester_worker.find_eligible_items()
        for sat_item in sat_items:
            sat_ingester_worker.start(sat_item)
