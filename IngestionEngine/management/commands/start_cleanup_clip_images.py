import logging
from django.core.management.base import BaseCommand
from IngestionEngine.workers.cleanup_clip_images_worker import CleanupClipImagesWorker


logger = logging.getLogger("ingestion-engine")


class Command(BaseCommand):
    help = "Start Pre ingestion to source data"

    def handle(self, *args, **options):
        ctpi_worker = CleanupClipImagesWorker()
        clip_requests = ctpi_worker.find_eligible_items()
        for clip_request in clip_requests:
            ctpi_worker.start(clip_request)
