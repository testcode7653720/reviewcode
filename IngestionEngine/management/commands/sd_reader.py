import logging
from django.core.management.base import BaseCommand
import os
from IngestionEngine.models import SDReader, SourceData
import uuid

logger = logging.getLogger("ingestion-engine")
invalid_chars = ["/", "\\", ":"]
valid_extensions = [
    ".img",
    ".tif",
    ".tiff",
    ".jp2",
    ".jpg",
    ".jpeg",
    ".png",
    ".geotiff",
    ".gtif",
    ".nitf",
    ".hdf",
    ".h5",
]


class Command(BaseCommand):
    help = "Start Ingesting Files"

    def handle(self, *args, **options):
        def is_valid_filename(file_name):
            for char in invalid_chars:
                if char in file_name:
                    return False

            file_extension = os.path.splitext(os.path.basename(file_name))[-1]
            print(f"File extension is {file_extension}")
            if file_extension.lower() not in valid_extensions:
                return False

            return True

        def read_sd_path(file_name):
            nas_drives = SDReader.objects.filter(Status="Active")

            if nas_drives.exists():
                nas_drive = nas_drives[0]

                file_extension = os.path.splitext(os.path.basename(file_name))[-1]
                uid = uuid.uuid4()
                file_name_without_extension = os.path.splitext(
                    os.path.basename(file_name)
                )[0]

                unique_file_name = (
                    f"{file_name_without_extension}_{uid}{file_extension}"
                )
                nas_drive_path = nas_drive.FolderPath

                full_file_path = os.path.join(nas_drive_path, unique_file_name)

            else:
                logger.info("There are no active NAS drives.")

            return full_file_path

        source_data = SourceData.objects.first()
        file_path = source_data.SourceDatasetPath
        file_name = os.path.basename(file_path)
        is_valid_file = is_valid_filename(file_name)
        if is_valid_file:
            destination_path = read_sd_path(file_name)
            logger.info(f"destination path is {destination_path}")
        else:
            logger.info(f"File name is not valid,{file_name}.")
