import logging
from django.core.management.base import BaseCommand
from IngestionEngine.workers.cleanup_logs_worker import CleanupLogsWorker


logger = logging.getLogger("ingestion-engine")


class Command(BaseCommand):
    help = "Start Cleanup to Logs"

    def handle(self, *args, **options):
        cleanup_worker = CleanupLogsWorker()
        log_items = cleanup_worker.find_eligible_items()
        for log_item in log_items:
            cleanup_worker.start(log_item)
