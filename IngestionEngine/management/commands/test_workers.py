from IngestionEngine.workers import (
    PreIngestionWorker,
    SDReaderWorker,
    LocalIngesterWorker,
    CogGeneratorWorker,
    IStacIngesterWorker,
)

from django.core.management.base import BaseCommand

schedule = {
    "PreIngestionWorker": True,
    "SDReaderWorker": True,
    "LocalIngesterWorker": True,
    "CogGeneratorWorker": True,
    "IStacIngesterWorker": True,
    "ItemValidatorWorker": True,
    "CTPIIngesterWorker": True,
}


class Command(BaseCommand):
    help = "Start Ingesting Files"

    def handle(self, *args, **options):
        if schedule["PreIngestionWorker"]:
            start_process_worker = PreIngestionWorker()
            source_datas = start_process_worker.find_eligible_items()
            for source_data in source_datas:
                start_process_worker.start(source_data)

            # time.sleep(10)

        if schedule["SDReaderWorker"]:
            path_reader_worker = SDReaderWorker()
            source_datas = path_reader_worker.find_eligible_items()
            for source_data in source_datas:
                path_reader_worker.start(source_data)

            # time.sleep(10)

        if schedule["LocalIngesterWorker"]:
            ingester_worker = LocalIngesterWorker()
            source_datas = ingester_worker.find_eligible_items()
            for source_data in source_datas:
                ingester_worker.start(source_data)

            # time.sleep(5)

        if schedule["CogGeneratorWorker"]:
            cog_worker = CogGeneratorWorker()
            source_datas = cog_worker.find_eligible_items()
            for source_data in source_datas:
                cog_worker.start(source_data)

            # time.sleep(10)
                
                
        """
        if schedule["IStacIngesterWorker"]:
            stac_worker = IStacIngesterWorker()
            source_datas = stac_worker.find_eligible_items()
            for source_data in source_datas:
                stac_worker.start(source_data)

       

        if schedule["CTPIIngesterWorker"]:
            ctpi_worker = CTPIIngesterWorker()
            source_datas = ctpi_worker.find_eligible_items()
            for source_data in source_datas:
                ctpi_worker.start(source_data)

        if schedule["CTPIClipperWorker"]:
            ctpi_worker = CTPIClipperWorker()
            clip_request_datas = ctpi_worker.find_eligible_items()
            for clip_request_data in clip_request_datas:
                ctpi_worker.start(source_data)
        """
