import logging
from django.core.management.base import BaseCommand

from SatProductCurator.workers.sat_product_search_worker import SatProductSearchWorker


logger = logging.getLogger("ingestion-engine")


class Command(BaseCommand):
    help = "Start Pre ingestion to source data"

    def handle(self, *args, **options):
        worker = SatProductSearchWorker()
        collection_requests = worker.find_eligible_items()
        for collection_request in collection_requests:
            worker.start(collection_request)
