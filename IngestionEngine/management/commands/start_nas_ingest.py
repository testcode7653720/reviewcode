import logging
from django.core.management.base import BaseCommand

from IngestionEngine.workers.nas_ingester_worker import IngesterWorker


logger = logging.getLogger("ingestion-engine")


class Command(BaseCommand):
    help = "Start Pre ingestion to source data"

    def handle(self, *args, **options):
        worker = IngesterWorker()
        source_data_instances = worker.find_eligible_items()

        for source_data in source_data_instances:
            worker.start(source_data=source_data)
