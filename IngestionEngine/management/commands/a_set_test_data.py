import os
import pystac
import shutil

from django.core.management.base import BaseCommand
from IngestionEngine.models import HotFile, IngestedResource


class Command(BaseCommand):
    help = "Closes the specified poll for voting"

    def handle(self, *args, **options):
        HotFile.objects.all().delete()
        IngestedResource.objects.all().delete()

        hot_files = []
        for file in os.listdir('media/hot_folder'):
            if os.path.splitext(file)[-1] in HotFile.ALLOWED_EXTENSIONS:
                hot_files.append(HotFile(path=f'./hot_folder/{file}', is_ready_for_ingestion=True))
        HotFile.objects.bulk_create(hot_files)

        # Delete the STAC folder
        shutil.rmtree('stac-catalog')
        os.mkdir('stac-catalog')

        # Create the catalog
        catalog = pystac.Catalog(id='iads-catalog', description='IADS Catalog by CodeRize Tech')
        catalog.normalize_hrefs("./stac-catalog")
        catalog.save(catalog_type=pystac.CatalogType.SELF_CONTAINED)
