import logging
from django.core.management.base import BaseCommand
from IngestionEngine.workers.cleanup_hot_files_worker import CleanupHotFilesWorker


logger = logging.getLogger("ingestion-engine")


class Command(BaseCommand):
    help = "Start Cleanup to source data"

    def handle(self, *args, **options):
        cleanup_worker = CleanupHotFilesWorker()
        source_datas = cleanup_worker.find_eligible_items()
        for source_data in source_datas:
            cleanup_worker.start(source_data)
