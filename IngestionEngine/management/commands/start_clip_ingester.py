from IngestionEngine.workers.clip_ingester_worker import ClipIngesterWorker
import logging
from django.core.management.base import BaseCommand

logger = logging.getLogger("ingestion-engine")


class Command(BaseCommand):
    help = "Start Pre ingestion to source data"

    def handle(self, *args, **options):
        clip_worker = ClipIngesterWorker()
        clip_requests = clip_worker.find_eligible_items()
        for clip_request in clip_requests:
            clip_worker.start(clip_request)
