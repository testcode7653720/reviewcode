from ._base_logger import Logger
from ._base_worker import BaseWorker
from IngestionEngine.models import SourceData
import logging

log = Logger("ResetFailedItemsWorker").get_logger()

WorkerName = "ResetFailedItemsWorker"
InProgress = "InProgress"
Completed = "Completed"
Failed = "Failed"
Scheduled = "Scheduled"
Pause = "Pause"


class ResetFailedItemsWorker(BaseWorker):

    """ResetFailedItemsWorker resets statuses for failed items."""

    def __init__(self) -> None:
        super().__init__()

    def get_failed_items(self):
        ingester_failed_items = SourceData.objects.filter(
            IADSCoreStatus__icontains="IngesterWorker_Failed"
        )

        cog_failed_items = SourceData.objects.filter(
            IADSCoreStatus__icontains="CogGeneratorWorker_Failed"
        )

        return ingester_failed_items, cog_failed_items

    def reset_statuses(self, failed_items):
        for item in failed_items:
            if "IngesterWorker" in item.IADSCoreStatus:
                item.IADSCoreStatus = "SDReaderWorker_Completed"
                item.save()
            elif "CogGeneratorWorker" in item.IADSCoreStatus:
                item.IADSCoreStatus = "IngesterWorker_Completed"
                item.save()

    def start(self):
        log("ResetFailedItems Worker Started.", level=logging.INFO)

        ingester_failed_items, cog_failed_items = self.get_failed_items()

        self.reset_statuses(ingester_failed_items)
        self.reset_statuses(cog_failed_items)

        log("ResetFailedItems Worker Completed.", level=logging.INFO)
