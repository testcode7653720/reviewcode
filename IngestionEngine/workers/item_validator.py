from ._base_logger import Logger
from ._base_worker import BaseWorker
from IngestionEngine.models import BusinessMetadata, SourceData, TargetData
import logging
import rasterio

log = Logger("ItemValidatorWorker").get_logger()

PrevoiusWorkerStatus = "IStacIngesterWorker_Completed"
WorkerName = "ItemValidatorWorker"
InProgress = "InProgress"
Completed = "Completed"
Failed = "Failed"
Scheduled = "Scheduled"
Pause = "Pause"


class ItemValidatorWorker(BaseWorker):
    """ItemValidatorWorker validates item."""
    
    def __init__(self) -> None:
        super().__init__()

    def eligible_item_reader(self, source_data: SourceData):
        previous_worker_status = source_data.IADSCoreStatus
        if previous_worker_status == PrevoiusWorkerStatus:
            self.process_started_worker(source_data)
        elif previous_worker_status == WorkerName + "_" + InProgress:
            self.process_started_worker(source_data)
        else:
            log(
                "IStacIngesterWorker either not completed or failed",
                source_data=source_data,
                level=logging.WARNING,
            )

    def is_valid_cog(self, file_path):
        with rasterio.open(file_path) as dataset:
            if dataset.is_tiled and len(dataset.overviews(1)) > 0:
                return True

    def process_started_worker(self, source_data: SourceData):
        log("ItemValidatorWorker Started", source_data=source_data, level=logging.INFO)
        IADSCoreStatus = WorkerName + "_" + InProgress
        source_data.IADSCoreStatus = IADSCoreStatus
        source_data.save()

        target_data = TargetData.objects.get(SourceData=source_data)

        business_metadata = BusinessMetadata.objects.get(TargetData=target_data)

        if business_metadata.extent and business_metadata.num_layers:
            log(
                "Business metadata is present and containing extent and no of layers values"
            )

        result = self.is_valid_cog(target_data.TargetDatasetPath)
        if result:
            log("File is a valid COG", source_data=source_data, level=logging.INFO)
        else:
            log("File is not a valid COG", source_data=source_data, level=logging.ERROR)

        IADSCoreStatus = WorkerName + "_" + Completed
        source_data.IADSCoreStatus = IADSCoreStatus
        source_data.save()
        log(
            "ItemValidatorWorker Completed", source_data=source_data, level=logging.INFO
        )

    def start(self, source_data: SourceData):
        self.eligible_item_reader(source_data)
