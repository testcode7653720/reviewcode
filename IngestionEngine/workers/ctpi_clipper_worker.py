import os
import traceback
from IngestionEngine.models import TargetImage
from SatProductCurator.models import NewClipRequest
from ._base_logger import Logger
from ._base_worker import BaseWorker
import logging
from django.db.models import Q
from django.conf import settings
from Processor.services import ClipperService


log = Logger("CTPIClipperWorker").get_logger()
WorkerName = "CTPIClipperWorker"
InProgress = "InProgress"
Completed = "Completed"
Failed = "Failed"
Scheduled = "Scheduled"
Pause = "Pause"
IngestionInProgress = "IngestionInProgress"


class CTPIClipperWorker(BaseWorker):
    """CTPIClipperWorker is clip image and saved clipped image url in NewClipRequest Table in Database."""

    def __init__(self) -> None:
        super().__init__()

    def find_eligible_items(self):
        try:
            # Finding clip requests that are not completed and not ingested.
            clip_requests = NewClipRequest.objects.filter(
                Q(IsClipCompleted="False") & Q(ClipIngestionStatus="NA")
            )
            return clip_requests

        except Exception as e:
            log(
                f"Error while finding eligible items to start worker: {str(e)} and {traceback.format_exc()}",
                level=logging.ERROR,
            )
            raise e

    def process_started_worker(self, new_request: NewClipRequest):
        log("CTPIClipperWorker Started.", clip_request=new_request, level=logging.INFO)

        # Getting Target image for clip request.
        target_image: TargetImage = new_request.TargetImageID

        log(
            "Method for Clip image is About to Start.",
            clip_request=new_request,
            level=logging.DEBUG,
        )
        clip_service = ClipperService()

        # Calling polygon_to_dataframe method
        log("Generating DataFrame.", clip_request=new_request, level=logging.DEBUG)
        gdf = clip_service.polygon_to_dataframe(new_request.Extent, new_request)
        log("Dataframe Generated.", clip_request=new_request, level=logging.DEBUG)

        # Path on which clipped image will be stored
        output_path = os.path.join(
            settings.CLIPPED_IMAGES_FOLDER,
            os.path.splitext(os.path.split(target_image.Path)[1])[0]
            + f"clipped_request_{new_request.pk}"
            + ".tif",
        )

        log(
            "Started Clipping for tif file.",
            clip_request=new_request,
            level=logging.DEBUG,
        )

        # Calling clip_using_dataframe method to clip image.
        clip_service.clip_using_dataframe(
            input_path=target_image.Path,
            input_gdf=gdf,
            output_path=output_path,
            clip_request=new_request,
        )
        log(
            f"Clipped tif image Saved to {output_path}",
            clip_request=new_request,
            level=logging.DEBUG,
        )

        new_request.ClippedTiffPath = output_path
        new_request.save()

        log(
            "Clipped tif image Path Saved to NewClipRequest table.",
            clip_request=new_request,
            level=logging.DEBUG,
        )

        # Generating clipped download url.
        clipped_download_url = settings.CLIP_SERVER_URL + output_path.replace(
            settings.CLIP_SERVER_PATH, ""
        )

        new_request.ClippedDownloadURL = clipped_download_url
        new_request.save()

        if clipped_download_url:
            log(
                f"Clipped Download URL is: {clipped_download_url}",
                clip_request=new_request,
                level=logging.DEBUG,
            )
        else:
            log(
                "CTPIClipperWorker Failed Oops Error might be Occurred in 'clip_using_dataframe' Method.",
                clip_request=new_request,
                level=logging.ERROR,
            )

        # Path on which clipped COG image will be stored
        output_cog_path = os.path.join(
            settings.CLIPPED_IMAGES_FOLDER,
            os.path.splitext(os.path.split(target_image.COGPath)[1])[0]
            + f"clipped_request_{new_request.pk}"
            + ".tif",
        )
        log(
            "Started Clipping for COG file.",
            clip_request=new_request,
            level=logging.DEBUG,
        )
        clip_service.clip_using_dataframe(
            input_path=target_image.COGPath,
            input_gdf=gdf,
            output_path=output_cog_path,
            clip_request=new_request,
        )
        log(
            f"Clipped COG image saved to {output_path}",
            clip_request=new_request,
            level=logging.DEBUG,
        )

        new_request.ClippedCOGPath = output_cog_path
        new_request.save()

        log(
            "Clipped COG image Path Saved to NewClipRequest table.",
            clip_request=new_request,
            level=logging.DEBUG,
        )

        # Generating clipped visualization url.
        clipped_image_url = settings.CLIP_SERVER_URL + output_cog_path.replace(
            settings.CLIP_SERVER_PATH, ""
        )

        if clipped_image_url:
            log(
                "Saving Clipped COG Image URL to NewClipRequest Table in Database.",
                clip_request=new_request,
                level=logging.DEBUG,
            )
            new_request.ClippedVisualizationURL = clipped_image_url
            new_request.ClipIngestionStatus = "Ready_For_ClipIngestion"
            new_request.IsClipCompleted = True
            new_request.save()

            log(
                "Set Status IsClipCompleted to True and ClipIngestionStatus as Ready_For_ClipIngestion.",
                clip_request=new_request,
                level=logging.DEBUG,
            )

            log(
                "Well Done!! CTPIClipperWorker Completed Successfully.",
                clip_request=new_request,
                level=logging.INFO,
            )

        else:
            log(
                "CTPIClipperWorker Failed Oops Error might be occurred in clip_image method.",
                clip_request=new_request,
                level=logging.ERROR,
            )

    def start(self, clip_request: NewClipRequest):
        self.process_started_worker(clip_request)
