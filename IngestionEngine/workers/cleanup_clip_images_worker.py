from datetime import timedelta
import os
import traceback
from SatProductCurator.models import NewClipRequest
from ._base_logger import Logger
from ._base_worker import BaseWorker
import logging
from django.db.models import Q
from django.conf import settings
from django.utils import timezone


log = Logger("CleanupClipImagesWorker").get_logger()
WorkerName = "CleanupClipImagesWorker"
InProgress = "InProgress"
Completed = "Completed"
Failed = "Failed"
Scheduled = "Scheduled"
Pause = "Pause"
IngestionInProgress = "IngestionInProgress"


class CleanupClipImagesWorker(BaseWorker):
    """CleanupClipImagesWorker is deleted clipped tiff and cog images from clipped_images folder."""

    def __init__(self) -> None:
        super().__init__()

    def find_eligible_items(self):
        try:
            # Calculate the threshold datetime
            threshold_datetime = timezone.now() - timedelta(
                days=settings.DELETE_CLIP_IMAGE_AFTER_DAYS
            )
            # Finding clip requests that are ingested into mna.
            clip_requests = NewClipRequest.objects.filter(
                Q(Is_deleted_from_nas="False")
                & Q(ClipIngestionStatus="ClipIngestion_Completed")
                & Q(ClipIngestedDate__lte=threshold_datetime)
            )

            if len(clip_requests) == 0:
                log(f"No Clip Request found for {WorkerName}", level=logging.WARN)
            return clip_requests

        except Exception as e:
            log(
                f"Error while finding eligible items to start worker: {str(e)} and {traceback.format_exc()}",
                level=logging.ERROR,
            )
            raise e

    def process_started_worker(self, new_request: NewClipRequest):
        log(
            "CleanupClipImagesWorker Started.",
            clip_request=new_request,
            level=logging.INFO,
        )

        # Path on which clipped image is stored
        clipped_tif_path = new_request.ClippedTiffPath

        log(
            f"Clipped tiff image path is: {clipped_tif_path}",
            clip_request=new_request,
            level=logging.DEBUG,
        )

        # Path on which clipped Cog image is stored
        clipped_cog_path = new_request.ClippedCOGPath

        log(
            f"Clipped COG image path is: {clipped_cog_path}",
            clip_request=new_request,
            level=logging.DEBUG,
        )

        if os.path.exists(clipped_tif_path):
            try:
                # Delete clipped images
                os.remove(clipped_tif_path)

                log(
                    "Clipped tiff image Successfully deleted from folder.",
                    clip_request=new_request,
                    level=logging.DEBUG,
                )
            except Exception as e:
                log(
                    f"Error occurred while deleting Clpipped tiff image: {str(e)} and {traceback.format_exc()}",
                    clip_request=new_request,
                    level=logging.ERROR,
                )
                raise e
        else:
            log(
                "Clpipped tiff image already deleted.",
                clip_request=new_request,
                level=logging.WARNING,
            )

        if os.path.exists(clipped_cog_path):
            try:
                # Delete clipped images
                os.remove(clipped_cog_path)

                log(
                    "Clipped COG image Successfully deleted from folder.",
                    clip_request=new_request,
                    level=logging.DEBUG,
                )
            except Exception as e:
                log(
                    f"Error occurred while deleting Clpipped COG image: {str(e)} and {traceback.format_exc()}",
                    clip_request=new_request,
                    level=logging.ERROR,
                )
                raise e
        else:
            log(
                "Clpipped COG image already deleted.",
                clip_request=new_request,
                level=logging.WARNING,
            )

        # Update NewClipRequest
        new_request.Is_deleted_from_nas = True
        new_request.save()

        log(
            "Set Is_deleted_from_nas flag to True.",
            clip_request=new_request,
            level=logging.DEBUG,
        )

        # Update worker status
        log(
            "CleanupClipImagesWorker Completed.",
            clip_request=new_request,
            level=logging.INFO,
        )

    def start(self, clip_request: NewClipRequest):
        self.process_started_worker(clip_request)
