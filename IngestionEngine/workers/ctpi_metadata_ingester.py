import traceback
import requests
from django.conf import settings
from ._base_logger import Logger
from ._base_worker import BaseWorker
from IngestionEngine.models import SourceData
import logging
from django.db.models import Q
from MnaApp.serializers import TargetDataCTPISerializer
from django.utils import timezone

log = Logger("CTPIIngesterWorker").get_logger()
PrevoiusWorkerStatus = "IngesterWorker_Completed"
WorkerName = "CTPIIngesterWorker"
InProgress = "InProgress"
Completed = "Completed"
Failed = "Failed"
Scheduled = "Scheduled"
Pause = "Pause"
IngestionInProgress = "Ingestion_InProgress"


class CTPIIngesterWorker(BaseWorker):
    """CTPIIngesterWorker is added item in MnASystems Database Table."""
    # It adds the data  into FGIC OSP
    def __init__(self) -> None:
        super().__init__()

    # Method to find eligible items for processing.
    def find_eligible_items(self):
        try:
            log(
                "Searching items for which ingestion process is completed.",
                level=logging.DEBUG,
            )
            source_datas = SourceData.objects.filter(
                Q(IADSIngestionStatus="Ingestion_Completed")
                & (
                    Q(CTPIIngestionStatus="Ready_For_CTPIIngestion")
                    | Q(CTPIIngestionStatus="CTPIIngestion_Failed")
                )
            )

            return source_datas
        except Exception as e:
            log(
                f"Error while finding eligible items to start worker: {str(e)} and {traceback.format_exc()}",
                level=logging.ERROR,
            )
            raise e

    # Method to check eligibility of an item for processing.
    def eligible_item_reader(self, source_data: SourceData):
        try:
            previous_worker_status = source_data.IADSCoreStatus
            if previous_worker_status == PrevoiusWorkerStatus:
                log(
                    "Eligible to Start CTPIIngesterWorker.",
                    source_data=source_data,
                    level=logging.INFO,
                )
                return True
            elif previous_worker_status == WorkerName + "_" + Failed:
                log(
                    "CogGeneratorWorker Failed hence CTPIIngesterWorker cannot be Start.",
                    level=logging.WARNING,
                    source_data=source_data,
                )
                return False
            else:
                log(
                    "CTPIIngesterWorker Already Completed for this file",
                    source_data=source_data,
                    level=logging.INFO,
                )
                return False
        except Exception as e:
            log(
                f"Error while checking eligibility of worker: {str(e)} and {traceback.format_exc()}",
                source_data=source_data,
                level=logging.ERROR,
            )
            raise e

    def get_access_token(self, source_data: SourceData):
        # This is for debugging only
        # if settings.DEBUG and not settings.IS_STAGING:
        #     return "DemoToken"

        url = settings.KEYCLOAK_TOKEN_URL

        # Request payload
        payload = {
            "grant_type": settings.PENTA_GRANT_TYPE,
            "client_id": settings.PENTA_CLIENT_ID,
            "client_secret": settings.PENTA_CLIENT_SECRET,
            "username": settings.PENTA_API_USERNAME,
            "password": settings.PENTA_AUTH_PASSWORD,
        }

        try:
            log(
                "Making request to get access token.",
                source_data=source_data,
                level=logging.DEBUG,
            )
            response = requests.post(url, data=payload, verify=False)
            response.raise_for_status()  # Raise an exception for 4xx/5xx status codes

            # Extracting access token from the response
            log(
                "Extracting access token from the response.",
                source_data=source_data,
                level=logging.DEBUG,
            )

            access_token = response.json().get("access_token")

            if access_token:
                return access_token
            else:
                # If access token is not found in the response
                log(
                    "Access token not found in response.",
                    source_data=source_data,
                    level=logging.WARNING,
                )
                return None

        except requests.exceptions.RequestException as e:
            log(
                f"Error: {str(e)} and {traceback.format_exc()}",
                source_data=source_data,
                level=logging.ERROR,
            )
            return None

    # Method to insert data into the MNA API.
    def insert_into_mna_api(self, source_data: SourceData):
        # Get the target data record.

        try:
            target_data_record = source_data.target_data
        except Exception as e:
            log(
                f"Target data record does not exist for source data and {traceback.format_exc()}",
                level=logging.ERROR,
                source_data=source_data,
            )
            # Set IADSCoreStatus as Failed.
            IADSCoreStatus = WorkerName + "_" + Failed
            source_data.IADSCoreStatus = IADSCoreStatus
            source_data.IADSIngestionStatus = "CTPIIngestion_Failed"
            source_data.CTPIIngestionStatus = "CTPIIngestion_Failed"
            source_data.target_data.CTPIIngestionStatus = "CTPIIngestion_Failed"

            source_data.save()
            log(
                f"Oops!! CTPIIngesterWorker Failed and error is: {str(e)} and {traceback.format_exc()}",
                source_data=source_data,
                level=logging.ERROR,
            )
            raise e

        access_token = self.get_access_token(source_data)
        if access_token:
            log(
                f"Access Token: {access_token}",
                source_data=source_data,
                target_data=target_data_record,
                level=logging.DEBUG,
            )

            url_from_mna = settings.MNA_METADATA_INSERT_URL

            headers = {
                "accept": "*/*",
                "Authorization": access_token,
                "PentaOrgID": settings.PENTA_ORG_ID,
                "PentaUserRole": settings.PENTA_USER_ROLE,
                "Content-Type": "application/json",
            }

            # Serialize the target data.
            log(
                "Serializing Target Data.",
                level=logging.DEBUG,
                source_data=source_data,
                target_data=target_data_record,
            )
            log(
                "Extracting Target Data from Serializer.",
                source_data=source_data,
                target_data=target_data_record,
                level=logging.DEBUG,
            )
            serializer = TargetDataCTPISerializer(target_data_record)
            data_to_send = serializer.data
            log(
                f"Data Extraction Successful from Serializer and Data to send is: {data_to_send}.",
                source_data=source_data,
                target_data=target_data_record,
                level=logging.DEBUG,
            )
            log(
                "Successfully Serialized the Data.",
                source_data=source_data,
                target_data=target_data_record,
                level=logging.DEBUG,
            )

            try:
                # Make API Requests.
                log(
                    "Creating API request to MnA.",
                    source_data=source_data,
                    target_data=target_data_record,
                    level=logging.DEBUG,
                )

                # For debugging
                # if settings.DEBUG and not settings.IS_STAGING:

                #     class DummyResponse:
                #         def __init__(self) -> None:
                #             self.status_code = 201
                #         def json(self):
                #             return  {'id': 'aafd66e8-44d2-4636-a464-dd43cf60da34', 'createdDate': '1710311891321'}
                #     response = DummyResponse()

                # else:

                response = requests.post(
                    url_from_mna, headers=headers, json=data_to_send, verify=False
                )

                log(
                    f"Request url is: {response.request.url}",
                    source_data=source_data,
                    target_data=target_data_record,
                    level=logging.DEBUG,
                )
                log(
                    f"Request body is: {response.request.body}",
                    source_data=source_data,
                    target_data=target_data_record,
                    level=logging.DEBUG,
                )
                log(
                    f"Request headers is: {response.request.headers}",
                    source_data=source_data,
                    target_data=target_data_record,
                    level=logging.DEBUG,
                )

                log(
                    f"Response from MnA in text format is : {response.text}",
                    source_data=source_data,
                    target_data=target_data_record,
                    level=logging.DEBUG,
                )

                log(
                    f"Response from MnA is: {response.json()}",
                    source_data=source_data,
                    target_data=target_data_record,
                    level=logging.DEBUG,
                )

                log(
                    f"Response code from MnA: {response.status_code}",
                    source_data=source_data,
                    target_data=target_data_record,
                    level=logging.DEBUG,
                )

                if response.status_code == 201:
                    # Get MnAID from the response.
                    log(
                        "Getting MnA ID from response.",
                        source_data=source_data,
                        target_data=target_data_record,
                        level=logging.DEBUG,
                    )
                    mna_id = response.json().get("id")

                    log(
                        f"Type of MnAID is: {type(mna_id)}",
                        source_data=source_data,
                        target_data=target_data_record,
                        level=logging.DEBUG,
                    )

                    # Update TargetData target_data_records with mna_id
                    # which is came in response from MNA.

                    # Update MnAID in TargetData.
                    log(
                        f"Updating MnA ID in Target Data table and MnA ID is: {mna_id}",
                        source_data=source_data,
                        target_data=target_data_record,
                        level=logging.DEBUG,
                    )

                    target_data_record.MnAID = mna_id

                    target_data_record.save()

                    log(
                        "Updated MnA ID in Target Data table Successfully.",
                        source_data=source_data,
                        target_data=target_data_record,
                        level=logging.DEBUG,
                    )

                    # Update ClipIngestedDate after successful completion
                    source_data.CTPIIngestedDate = timezone.now()
                    source_data.save()
                    log(
                        "Updated CTPIIngestedDate with current Datetime.",
                        source_data=source_data,
                        target_data=target_data_record,
                        level=logging.DEBUG,
                    )

                    # Set status as ctpi ingestion completed
                    source_data.CTPIIngestionStatus = "CTPIIngestion_Completed"
                    target_data_record.CTPIIngestionStatus = "CTPIIngestion_Completed"
                    log(
                        "Updated CTPI Ingestion Status as CTPIIngestion_Completed.",
                        source_data=source_data,
                        target_data=target_data_record,
                        level=logging.DEBUG,
                    )
                    source_data.save()
                    target_data_record.save()
                    return True, response.json()
                else:
                    return False, response.json()

            except requests.RequestException as e:
                log(
                    f"Request failed: {str(e)} and {traceback.format_exc()}",
                    source_data=source_data,
                    target_data=target_data_record,
                    level=logging.ERROR,
                )
                # Set IADSCoreStatus as Failed.
                IADSCoreStatus = WorkerName + "_" + Failed
                source_data.IADSCoreStatus = IADSCoreStatus
                source_data.IADSIngestionStatus = "CTPIIngestion_Failed"
                source_data.CTPIIngestionStatus = "CTPIIngestion_Failed"
                source_data.target_data.CTPIIngestionStatus = "CTPIIngestion_Failed"
                source_data.target_data.save()
                source_data.save()
                log(
                    f"Oops!! API Request failed and error is: {str(e)} and {traceback.format_exc()}",
                    source_data=source_data,
                    target_data=target_data_record,
                    level=logging.ERROR,
                )
                raise e
        else:
            log(
                "Failed to fetch access token.",
                source_data=source_data,
                target_data=target_data_record,
                level=logging.DEBUG,
            )
            return False

    def process_started_worker(self, source_data: SourceData):
        log("CTPIIngesterWorker Started.", source_data=source_data, level=logging.INFO)

        # Get the target data record.
        log(
            "Getting the related Target Data record for Source Data.",
            level=logging.DEBUG,
            source_data=source_data,
        )
        try:
            source_data.target_data
        except Exception as e:
            log(
                f"Target data record does not exist for source data and {traceback.format_exc()}",
                level=logging.ERROR,
                source_data=source_data,
            )
            # Set IADSCoreStatus as Failed.
            IADSCoreStatus = WorkerName + "_" + Failed
            source_data.IADSCoreStatus = IADSCoreStatus
            source_data.CTPIIngestionStatus = "CTPIIngestion_Failed"
            source_data.target_data.CTPIIngestionStatus = "CTPIIngestion_Failed"
            source_data.target_data.save()
            source_data.save()

            log(
                f"Oops!! CTPIIngesterWorker Failed and error is: {str(e)} and {traceback.format_exc()}",
                source_data=source_data,
                level=logging.ERROR,
            )
            raise e

        IADSCoreStatus = WorkerName + "_" + InProgress
        source_data.IADSCoreStatus = IADSCoreStatus
        source_data.CTPIIngestionStatus = "CTPIIngestion_InProgress"
        source_data.target_data.CTPIIngestionStatus = "CTPIIngestion_InProgress"
        source_data.target_data.save()
        source_data.save()

        # calling method insert_into_mna to insert metadata into mna system.

        log(
            "Reached at the 'insert_into_mna' Method & About to Execute.",
            source_data=source_data,
            level=logging.DEBUG,
        )
        insert_into_mna, response_status = self.insert_into_mna_api(source_data)
        log(
            "'insert_into_mna_api' Method executed Successfully.",
            source_data=source_data,
            level=logging.DEBUG,
        )

        if insert_into_mna:
            log(
                "Well Done!! CTPIIngesterWorker Completed Successfully.",
                source_data=source_data,
                level=logging.INFO,
            )
            log(
                f"Operation Successful and response is: {response_status}",
                source_data=source_data,
                level=logging.DEBUG,
            )

            # Set status as ctpi ingestion completed
            IADSCoreStatus = WorkerName + "_" + Completed
            source_data.IADSCoreStatus = IADSCoreStatus
            source_data.CTPIIngestionStatus = "CTPIIngestion_Completed"
            source_data.target_data.CTPIIngestionStatus = "CTPIIngestion_Completed"
            source_data.target_data.save()
            source_data.save()

        else:
            log(
                "Oops!! CTPIIngesterWorker Failed.",
                source_data=source_data,
                level=logging.INFO,
            )
            log(
                f"Operation Failed and response is: {response_status}",
                source_data=source_data,
                level=logging.DEBUG,
            )
            IADSCoreStatus = WorkerName + "_" + Failed
            source_data.IADSCoreStatus = IADSCoreStatus
            source_data.CTPIIngestionStatus = "CTPIIngestion_Failed"
            source_data.target_data.CTPIIngestionStatus = "CTPIIngestion_Failed"
            source_data.target_data.save()
            source_data.save()

    def start(self, source_data: SourceData):
        log(
            "Checking if Item is Eligible to Start CTPIIngesterWorker.",
            level=logging.DEBUG,
        )
        # Calling eligible_item_reader to find is item is eligible to start or not.
        is_eligible = self.eligible_item_reader(source_data)
        if is_eligible:
            log(
                "Now Starting CTPIIngesterWorker.",
                level=logging.INFO,
            )
            # Calling process_started_worker method.
            self.process_started_worker(source_data)
