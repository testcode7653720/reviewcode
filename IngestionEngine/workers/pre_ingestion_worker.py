import os
from ._base_logger import Logger
from ._base_worker import BaseWorker
from IngestionEngine.models import SourceData
import logging
import traceback
from django.db.models import Q

# Create a logger instance for the PreIngestionWorker.
log = Logger("PreIngestionWorker").get_logger()

# Define constants for worker status and file extensions.
PreviousWorkerStatus = "FileWatcherWorker_Completed"
WorkerName = "PreIngestionWorker"
InProgress = "InProgress"
Completed = "Completed"
Failed = "Failed"
Scheduled = "Scheduled"
Pause = "Pause"
IngestionInProgress = "Ingestion_InProgress"

# List of allowed file extensions for the PreIngestionWorker.
ALLOWED_EXTENSIONS = {
    "img",
    "tif",
    "tiff",
    "jp2",
    "jpg",
    "jpeg",
    "png",
    "geotiff",
    "gtif",
    "nitf",
    "hdf",
    "h5",
}

# List of Invalid characters are not allowed in the File Name.
INVALID_CHARS = {"/", "\\", ":", "?", "|", "<", ">"}


class PreIngestionWorker(BaseWorker):

    """Pre-Ingestion Worker checks validation to decide whether the item is valid for ingestion or not."""

    def __init__(self) -> None:
        super().__init__()

    # Method to find eligible items for processing.
    def find_eligible_items(self):
        """Find items that are ready for Pre-Ingestion Worker"""
        try:
            log("Searching eligible items.", level=logging.DEBUG)
            # filter all source data objects from Source Data table which has IADSIngestionStatus is ReadyForIngestion.
            source_datas = SourceData.objects.filter(
                Q(
                    Q(IADSIngestionStatus="Ready_For_Ingestion")
                    & Q(SourceDatasetType="Hot Folder 1")
                )
                | Q(IADSCoreStatus=f"{WorkerName}_{Failed}")
            )

            if len(source_datas) == 0:
                log(f"No SourceData found for {WorkerName}", level=logging.WARN)

            return source_datas
        except Exception as e:
            log(
                f"Oops!! Got an Error while finding eligible items and error is: {str(e)} and {traceback.format_exc()}",
                level=logging.ERROR,
            )
            raise e

    # This method is used to find wheather item is eligible to start Worker.
    def eligible_item_reader(self, source_data: SourceData) -> bool:
        """Find eligibility of item to start Pre-Ingestion Worker"""
        try:
            log(
                "Reached at the 'eligible_item_reader' Method & About to Execute.",
                source_data=source_data,
                level=logging.DEBUG,
            )

            # Getting the IADSCoreStatus for Source Data instance from database.
            previous_worker_status = source_data.IADSCoreStatus

            # Check if Previous Worker Status is Completed or not.
            if previous_worker_status == PreviousWorkerStatus:
                log(
                    "Well Done!! Item is Eligible to Execute PreIngestionWorker.",
                    source_data=source_data,
                    level=logging.INFO,
                )
                return True
            # Check if CurrentWorker status is Failed.
            elif previous_worker_status == WorkerName + "_" + Failed:
                log(
                    "Oops!! Not an Eligible Item to execute PreIngestionWorker.",
                    level=logging.WARNING,
                    source_data=source_data,
                )
                return True
            else:
                log(
                    "Oops!!.Not an Eligible Item & item InProgress or might be Already Processed.",
                    source_data=source_data,
                    level=logging.WARN,
                )
                return False
        except Exception as e:
            log(
                f"Oops!! Error has Occured while checking Eligibility of PreIngestionWorker and error is: {str(e)} and {traceback.format_exc()}",
                level=logging.ERROR,
                source_data=source_data,
            )
            raise e

    # Method to check Filename is Valid or not.
    def is_valid_filename(self, file_name, source_data: SourceData):
        log(
            "Reached at the 'is_valid_filename' Method & About to Execute.",
            source_data=source_data,
            level=logging.DEBUG,
        )

        # Checking every character in INVALID_CHARS and if it contains in file name then it will return false.
        for char in INVALID_CHARS:
            if char in file_name:
                return False

        log(
            "Reached at the End of 'is_valid_filename' Method & Completed Execution Successfully.",
            source_data=source_data,
            level=logging.DEBUG,
        )

        # If Filename does not contain any invalid characters then it will return True.
        return True

    # Method to start the processing of an eligible item.
    def process_started_worker(self, source_data: SourceData):
        """Process started for PreIngestionWorker"""

        log(
            "Reached at the 'process_started_worker' Method & About to Execute.",
            source_data=source_data,
            level=logging.DEBUG,
        )

        log(
            "Well Done!! PreIngestionWorker Started.",
            source_data=source_data,
            level=logging.INFO,
        )

        # Set IADSCoreStatus PreIngestionWorkerInProgress.
        IADSCoreStatus = WorkerName + "_" + InProgress
        source_data.IADSCoreStatus = IADSCoreStatus
        source_data.save()
        try:
            log(
                "Reading Path of a File from Database.",
                source_data=source_data,
                level=logging.DEBUG,
            )
            # Getting File Path form SourceData table.
            file_path = source_data.SourceDatasetPath
        except Exception as e:
            log(
                f"Oops!! Error has Occured and error is: {str(e)} and {traceback.format_exc()}",
                source_data=source_data,
                level=logging.ERROR,
            )

            # Set IADSCoreStatus PreIngestionWorkerFailed.
            IADSCoreStatus = WorkerName + "_" + Failed
            source_data.IADSCoreStatus = IADSCoreStatus
            source_data.IADSIngestionStatus = "Ingestion_Failed"
            source_data.save()
            log(
                "Oops!! Error has Occured. File Path might not be Exist in Database & PreIngestionWorker Failed.",
                source_data=source_data,
                level=logging.ERROR,
            )
            raise e

        log(
            "Getting Current Size of a File.",
            source_data=source_data,
            level=logging.DEBUG,
        )

        # Getting current size of a File from System.
        try:
            file_size = os.path.getsize(file_path)
        except Exception as e:
            log(
                f"Oops!! Error has Occured, {str(e)} and {traceback.format_exc()}",
                source_data=source_data,
                level=logging.ERROR,
            )

            # Set IADSCoreStatus PreIngestionWorkerFailed.
            IADSCoreStatus = WorkerName + "_" + Failed
            source_data.IADSCoreStatus = IADSCoreStatus
            source_data.IADSIngestionStatus = "Ingestion_Failed"
            source_data.save()
            log(
                "Oops!! PreIngestionWorker Failed.",
                source_data=source_data,
                level=logging.ERROR,
            )
            raise e

        log(
            f"Current Size of a File is : {file_size} bytes.",
            source_data=source_data,
            level=logging.DEBUG,
        )

        log(
            "Updating the Size of a File.",
            source_data=source_data,
            level=logging.DEBUG,
        )

        # Update File Size in SourceData table.
        source_data.SourceDatasetSize = file_size

        log(
            "Great!! The Size of a File Updated Successfully.",
            source_data=source_data,
            level=logging.DEBUG,
        )

        log(
            "Reading Name of a File from File Path.",
            source_data=source_data,
            level=logging.DEBUG,
        )

        # Reading Name of a File from File Path.
        file_name = os.path.basename(file_path)

        log(f"File Name is: {file_name}.", source_data=source_data, level=logging.DEBUG)

        # Getting Extension from File Path.
        file_extension = file_path.split(".")[-1]

        # This validation specifically write to check if filename contains
        # more than 2 dots in name like "abc.image.pqr.tiff" then
        # it will consider invalid file name.
        # Split File Path using '.' separator.
        list = file_path.split(".")
        log(
            f"Reading Extension of a File & Extension is: {file_extension}.",
            source_data=source_data,
            level=logging.DEBUG,
        )

        # Count the number of items in a list.
        count = len(list)

        log(
            "Checking if the File has a Valid Name.",
            source_data=source_data,
            level=logging.DEBUG,
        )

        # Check if count is greater than 3.
        if count > 3:
            IADSCoreStatus = WorkerName + "_" + Failed
            source_data.IADSCoreStatus = IADSCoreStatus
            source_data.IADSIngestionStatus = "Ingestion_Failed"
            source_data.save()
            log(
                "Oops!! Error has Occured. File Name might not be Valid.",
                source_data=source_data,
                level=logging.DEBUG,
            )
            log(
                "PreIngestionWorker Failed & File Name might not be Valid.",
                source_data=source_data,
                level=logging.ERROR,
            )
            raise ValueError("Error. Invalid file name.")

        # This validation checks if File has allowed  exetension.
        # Checks file extension is present in ALLOWED_EXTENSIONS list.
        elif file_extension not in ALLOWED_EXTENSIONS:
            log(
                "Oops!! Invalid Extension of a File to Execute PreIngestionWorker.",
                source_data=source_data,
                level=logging.ERROR,
            )

            # Set IADSCoreStatus PreIngestionWorkerFailed.
            IADSCoreStatus = WorkerName + "_" + Failed
            source_data.IADSCoreStatus = IADSCoreStatus
            source_data.IADSIngestionStatus = "Ingestion_Failed"
            source_data.save()
            log(
                "Oops!! PreIngestionWorker Failed due to Invalid File Extension.",
                source_data=source_data,
                level=logging.ERROR,
            )
            raise ValueError(
                "Error. Found an Invalid File Extension.",
                source_data=source_data,
                level=logging.DEBUG,
            )

        # This validation checks that File Name is valid or not.
        # Checking if is_valid_filename is returning True or not.
        elif self.is_valid_filename(file_name, source_data):
            log(
                "Great!! File has Valid Name.",
                source_data=source_data,
                level=logging.INFO,
            )
            log(
                "Well Done!! PreIngestionWorker Completed Successfully.",
                source_data=source_data,
                level=logging.INFO,
            )

            # Set IADSCoreStatus PreIngestionWorkerCompleted.
            IADSCoreStatus = WorkerName + "_" + Completed
            source_data.IADSCoreStatus = IADSCoreStatus
            source_data.IADSIngestionStatus = IngestionInProgress
            source_data.save()
        else:
            log(
                "Oops!! Found an Invalid File Name.",
                source_data=source_data,
                level=logging.ERROR,
            )

            # Set IADSCoreStatus PreIngestionWorkerFailed.
            IADSCoreStatus = WorkerName + "_" + Failed
            source_data.IADSCoreStatus = IADSCoreStatus
            source_data.IADSIngestionStatus = "Ingestion_Failed"
            source_data.save()

            raise ValueError("Error. File has Invalid Name.")

    def start(self, source_data: SourceData):
        log(
            "Checking if Item is Eligible to Start PreIngestionWorker.",
            source_data=source_data,
            level=logging.DEBUG,
        )

        # Calling eligible_item_reader to find is item is eligible to start or not.
        is_eligible = self.eligible_item_reader(source_data)
        if is_eligible:
            log(
                "Now Starting PreIngestionWorker.",
                source_data=source_data,
                level=logging.INFO,
            )
            # Calling process_started_worker method.
            self.process_started_worker(source_data)
