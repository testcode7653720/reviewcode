# Importing necessary modules and classes.
import traceback
from ._base_logger import Logger
from ._base_worker import BaseWorker
from datetime import datetime
import os
from IngestionEngine.models import SDReader, SourceData, TargetData
from django.db.models import Q
import logging


# Creating a logger instance.
log = Logger("SDReaderWorker").get_logger()

# Constants for worker status and date format.
PrevoiusWorkerStatus = "PreIngestionWorker_Completed"
WorkerName = "SDReaderWorker"
InProgress = "InProgress"
Completed = "Completed"
Failed = "Failed"
Scheduled = "Scheduled"
Pause = "Pause"
DATE_FORMAT = "%Y-%m-%d_%H-%M-%S"


class SDReaderWorker(BaseWorker):

    """SDReaderWorker creates a new Destination Path."""

    def __init__(self) -> None:
        super().__init__()

    # This function finds eligible items from Database.
    def find_eligible_items(self):
        """Find items that are ready for SDReader Worker"""
        try:
            log("Searching eligible items.", level=logging.DEBUG)
            # filter all source data objects from Source Data table which has
            # IADSCoreStatus is PreIngestionWorker_Completed.
            source_datas = SourceData.objects.filter(
                Q(
                    IADSCoreStatus__in=(
                        "PreIngestionWorker_Completed",
                        "SatDownloadWorker_Completed",
                        # f"{WorkerName}_{Failed}",
                    )
                )
                | Q(
                    IADSCoreStatus="FileWatcherWorker_Completed",
                    SourceDatasetType="Hot Folder 2",
                )
            )
            if len(source_datas) == 0:
                log(f"No SourceData found for {WorkerName}", level=logging.WARN)
            return source_datas

        except Exception as e:
            log(
                f"Oops!! Got an Error while finding eligible items {str(e)} and {traceback.format_exc()}",
                level=logging.ERROR,
            )
            raise e

    # This method is used to find wheather item is eligible to start Worker.
    def eligible_item_reader(self, source_data: SourceData):
        """Find eligibility of item to start SDReader Worker"""
        try:
            log(
                "Reached at the 'eligible_item_reader' Method & About to Execute.",
                source_data=source_data,
                level=logging.DEBUG,
            )

            if source_data.IADSCoreStatus in (
                "PreIngestionWorker_Completed",
                "SatDownloadWorker_Completed",
                # f"{WorkerName}_{Failed}",
            ) or (
                source_data.IADSCoreStatus == "FileWatcherWorker_Completed"
                and source_data.SourceDatasetType == "Hot Folder 2"
            ):
                log(
                    "Well Done!! Item is Eligible to execute SDReaderWorker.",
                    source_data=source_data,
                    level=logging.INFO,
                )
                return True

            else:
                log(
                    "Oops!!.Not an Eligible Item & item InProgress or might be Already Processed.",
                    source_data=source_data,
                    level=logging.WARNING,
                )
                return False

        except Exception as e:
            log(
                f"Oops!! Error has Occured while checking Eligibility of SDReaderWorker and error is: {str(e)} and {traceback.format_exc()}",
                level=logging.ERROR,
                source_data=source_data,
            )
            raise e

    def create_destination_folder_path(
        self, file_path: str, target_data_id, source_data: SourceData
    ):
        """
        Create the destination folder path for SDReaderWorker.

        Args:
        - file_path: The path of the file.

        Returns:
        - The generated destination folder path.
        """
        log(
            "Reached at the 'create_destination_folder_path' Method & About to Execute.",
            source_data=source_data,
            level=logging.DEBUG,
        )

        log(
            "Creating Destination Folder Path.",
            file_path=file_path,
            source_data=source_data,
            level=logging.DEBUG,
        )

        log(
            "Obtaining Extension of a File.",
            source_data=source_data,
            file_path=file_path,
            level=logging.DEBUG,
        )

        # Extracting the file extension.
        extension = file_path.split(".")[-1]

        log(
            f"Extension of a File is: {extension}.",
            source_data=source_data,
            file_path=file_path,
            extension=extension,
            level=logging.INFO,
        )

        log(
            "Obtaining Filename Without Extension.",
            source_data=source_data,
            file_path=file_path,
            level=logging.DEBUG,
        )
        try:
            # Extracting the filename without extension.
            file_name_without_extension = os.path.splitext(os.path.basename(file_path))[
                0
            ]
            log(
                f"Obtained Filename Without Extension & FileName is: {file_name_without_extension}",
                file_path=file_path,
                source_data=source_data,
                file_name_without_extension=file_name_without_extension,
                level=logging.DEBUG,
            )
        except Exception as e:
            log(
                f"Oops!! Error while Searching Filename Without Extension {str(e)} and {traceback.format_exc()}",
                source_data=source_data,
                level=logging.ERROR,
            )
            raise e

        # Fetch the First Active NAS drive from the SDReader table.
        nas_drive = SDReader.objects.filter(Status="Active").first()

        if nas_drive is None:
            log(
                "There are No Active Drives Present.",
                source_data=source_data,
                level=logging.ERROR,
            )
            raise Exception("There are No Active Drives Present.")

        # Fetch NAS drive path.
        folder_path = nas_drive.FolderPath

        log(
            "Obtaining Current DateTime.",
            source_data=source_data,
            file_name=file_path,
            level=logging.DEBUG,
        )

        # Obtaining the current date and time.
        current_datetime = datetime.now().strftime(DATE_FORMAT)

        log(
            f"Current DateTime is: {current_datetime}.",
            file_name=file_path,
            source_data=source_data,
            level=logging.INFO,
        )

        # Creating the destination path by combining folder path, filename, and current datetime.
        destination_path = os.path.join(
            folder_path,
            f"{target_data_id}_{current_datetime}.{extension}",
        )

        log(
            "Destination Folder Path Created Successfully.",
            source_data=source_data,
            destination_path=destination_path,
            level=logging.INFO,
        )
        log(
            "Reached at the End of 'create_destination_folder_path' Method & Completed Execution Successfully.",
            source_data=source_data,
            level=logging.DEBUG,
        )
        return destination_path

    def process_started_worker(self, source_data: SourceData):
        """Process started for SDReader Worker"""
        log(
            "Reached at the 'process_started_worker' Method & About to Execute.",
            source_data=source_data,
            level=logging.DEBUG,
        )

        log(
            "Well Done!! SDReaderWorker Started.",
            source_data=source_data,
            level=logging.INFO,
        )

        # Set IADSCoreStatus SDReaderWorkerInProgress.
        IADSCoreStatus = WorkerName + "_" + InProgress
        source_data.IADSCoreStatus = IADSCoreStatus
        source_data.IADSIngestionStatus = "Ingestion_InProgress"
        source_data.save()

        log(
            "Checking if File Exists at Specific Location.",
            source_data=source_data,
            level=logging.DEBUG,
        )

        log(
            "Reading Path of a File from Database.",
            source_data=source_data,
            level=logging.DEBUG,
        )
        # Getting File Path form SourceData table.
        file_path = source_data.SourceDatasetPath

        # Checking if the file exists at the specified FilePath.
        if os.path.isfile(file_path):
            # Getting current size of a File from System.
            try:
                file_size = os.path.getsize(file_path)
            except Exception as e:
                log(
                    f"Oops!! Error has Occured, {str(e)} and {traceback.format_exc()}",
                    source_data=source_data,
                    level=logging.ERROR,
                )

                # Set IADSCoreStatus SDReaderWorkerFailed.
                IADSCoreStatus = WorkerName + "_" + Failed
                source_data.IADSCoreStatus = IADSCoreStatus
                source_data.IADSIngestionStatus = "Ingestion_Failed"
                source_data.save()
                log(
                    "Oops!! SDReaderWorker Failed.",
                    source_data=source_data,
                    level=logging.ERROR,
                )
                raise e

            # Update File Size in SourceData table.
            source_data.SourceDatasetSize = file_size

            source_data.save()

            try:
                log(
                    "Getting Target Data from Database.",
                    source_data=source_data,
                    level=logging.DEBUG,
                )
                # Getting TargetData Object from TargetData table.
                target_data = TargetData.objects.get(SourceData=source_data)
            except TargetData.DoesNotExist:
                log(
                    "Target Data not Exists in Database.",
                    source_data=source_data,
                    level=logging.DEBUG,
                )
                # Creating a new TargetData object.
                target_data = TargetData()
                # Added SourceDataID in the TargetData Table.
                target_data.SourceData = source_data
                target_data.save()
                log(
                    "Target Data Created Successfully.",
                    source_data=source_data,
                    target_data=target_data,
                    level=logging.INFO,
                )

        else:
            # Set IADSCoreStatus SDReaderWorkerFailed.
            IADSCoreStatus = WorkerName + "_" + Failed
            source_data.IADSCoreStatus = IADSCoreStatus
            source_data.IADSIngestionStatus = "Ingestion_Failed"
            source_data.save()
            # log msg for file not exixts error.
            log(
                "Oops!! Got an Error & File might not be exist.",
                source_data=source_data,
                level=logging.DEBUG,
            )
            log(
                "Oops!! SDReaderWorker Failed & File might not be Exist at Location.",
                source_data=source_data,
                level=logging.ERROR,
            )
            raise ValueError("Error. File might not be Exist.")

        try:
            log(
                "Obtaining Path of a File.",
                source_data=source_data,
                target_data=target_data,
                level=logging.DEBUG,
            )
            # Extracting Path of a File from database.
            file_path = source_data.SourceDatasetPath

        except Exception as e:
            log(
                f"Oops!! Error has Occured & File Path Found {str(e)} and {traceback.format_exc()}",
                level=logging.ERROR,
            )
            IADSCoreStatus = WorkerName + "_" + Failed
            source_data.IADSCoreStatus = IADSCoreStatus
            source_data.IADSIngestionStatus = "Ingestion_Failed"
            source_data.save()
            log(
                "SDReader Worker Failed & File might not be Exist at Path.",
                source_data=source_data,
                level=logging.ERROR,
            )
            raise e

        log(
            "Creating the Destination Folder Path.",
            source_data=source_data,
            level=logging.DEBUG,
        )

        # Calling create_destination_folder_path.
        destination_path = self.create_destination_folder_path(
            file_path, target_data.TargetDataID, source_data
        )

        # check if the destination path is none.
        if destination_path is None:
            IADSCoreStatus = WorkerName + "_" + Failed
            source_data.IADSCoreStatus = IADSCoreStatus
            source_data.IADSIngestionStatus = "Ingestion_Failed"
            source_data.save()
            log(
                "SDReaderWorker Failed & Destination Server Path might not be Created.",
                source_data=source_data,
                level=logging.ERROR,
            )
        else:
            log(
                "Well Done!! Destination Server Path is Created Successfully.",
                source_data=source_data,
                target_data=target_data,
                level=logging.INFO,
            )

            log(
                f"Destination Server Path is : {destination_path}",
                source_data=source_data,
                target_data=target_data,
                level=logging.INFO,
            )

        # Update the Target Dataset Path in Target Data table.
        target_data.TargetDatasetPath = destination_path
        log(
            "Great!! Destination Server Path Saved in Database Successfully.",
            destination_path=destination_path,
            source_data=source_data,
            target_data=target_data,
            level=logging.DEBUG,
        )

        try:
            # Update the Target Dataset Name in Target Data table.
            target_data.TargetDatasetName = os.path.basename(destination_path)
            target_data.save()
            log(
                "Destination Server Path File Name Saved in Database Successfully.",
                source_data=source_data,
                target_data=target_data,
                level=logging.DEBUG,
            )
        except Exception as e:
            log(
                f"Oops!! Error while Saving Destination Server Path File Name in Database {str(e)} and {traceback.format_exc()}",
                level=logging.ERROR,
                source_data=source_data,
            )
            raise e

        IADSCoreStatus = WorkerName + "_" + Completed
        source_data.IADSCoreStatus = IADSCoreStatus
        source_data.target_data.IADSIngestionStatus = "Ingestion_InProgress"
        source_data.target_data.save()
        source_data.save()
        log(
            "Well Done!! SDReaderWorker Completed Successfully.",
            source_data=source_data,
            target_data=target_data,
            level=logging.INFO,
        )
        return destination_path

    def start(self, source_data: SourceData):
        log(
            "Checking if Item is Eligible to Start SDReaderWorker.",
            source_data=source_data,
            level=logging.DEBUG,
        )

        # Calling eligible_item_reader to find is item is eligible to start or not.
        is_eligible = self.eligible_item_reader(source_data)
        if is_eligible:
            log(
                "Now starting SDReaderWorker.",
                source_data=source_data,
                level=logging.INFO,
            )
            # Calling process_started_worker method.
            self.process_started_worker(source_data)
