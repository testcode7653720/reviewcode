from ._base_eligibility_worker import BaseEligibilityWorker


class BaseWorker(BaseEligibilityWorker):

    def read_admin_controller_flags():
        raise NotImplementedError

    def eligible_item_reader():
        raise NotImplementedError

    def process_started_worker():
        raise NotImplementedError

    def process_completed_worker():
        raise NotImplementedError

    def start(self):
        raise NotImplementedError
