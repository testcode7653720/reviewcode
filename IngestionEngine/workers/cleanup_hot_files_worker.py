import os
import traceback
from IngestionEngine.models import SourceData
from ._base_logger import Logger
from ._base_worker import BaseWorker
import logging
from django.db.models import Q
from django.conf import settings
from django.utils import timezone
from datetime import timedelta


log = Logger("CleanupHotFilesWorker").get_logger()
WorkerName = "CleanupHotFilesWorker"
InProgress = "InProgress"
Completed = "Completed"
Failed = "Failed"
Scheduled = "Scheduled"
Pause = "Pause"
IngestionInProgress = "IngestionInProgress"


class CleanupHotFilesWorker(BaseWorker):
    """CleanupClipImagesWorker is deleted clipped tiff and cog images from clipped_images folder."""

    def __init__(self) -> None:
        super().__init__()

    def find_eligible_items(self):
        try:
            # Calculate the threshold datetime
            threshold_datetime = timezone.now() - timedelta(
                days=settings.DELETE_HOT_FOLDER_IMAGE_AFTER_DAYS
            )

            log(
                f"Threshold date to delete images is: {threshold_datetime}",
                level=logging.DEBUG,
            )

            # Finding source datas that are ingested into mna.
            source_data_items = SourceData.objects.filter(
                Q(Is_deleted_from_hot_folder="False")
                & Q(CTPIIngestionStatus="CTPIIngestion_Completed")
                & Q(CTPIIngestedDate__lte=threshold_datetime)
            )

            if len(source_data_items) == 0:
                log(f"No hot files found for {WorkerName}", level=logging.WARNING)
            return source_data_items

        except Exception as e:
            log(
                f"Error while finding eligible items to start worker: {str(e)} and {traceback.format_exc()}",
                level=logging.ERROR,
            )
            raise e

    def generate_excel_file_paths(self, input_path, source_data: SourceData):
        """
        Generate file paths with the extensions xlsx, xls, and csv.

        Args:
        input_path (str): The input file path.

        Returns:
        list: A list containing file paths with extensions xlsx, xls, and csv.
        """

        log(
            "Inside 'generate_excel_file_paths' method.",
            source_data=source_data,
            level=logging.DEBUG,
        )
        # Split the input path into directory path and filename
        directory, filename = os.path.split(input_path)

        log(
            f"Generated filename from path and filename is: {filename}.",
            source_data=source_data,
            level=logging.DEBUG,
        )

        file_extension = input_path.split(".")[-1]
        log(
            f"File extension is: {file_extension}.",
            source_data=source_data,
            level=logging.DEBUG,
        )

        # Get the filename without the extension
        filename_without_extension, extension = os.path.splitext(filename)

        log(
            f"File Name without extension is: {filename_without_extension}",
            source_data=source_data,
            level=logging.DEBUG,
        )

        # Initialize an empty list to store the paths
        paths = []

        # Generate paths for xlsx, xls, and csv extensions
        for ext in [".xlsx", ".xls", ".csv"]:
            paths.append(
                os.path.join(
                    directory, f"{filename_without_extension}_{file_extension}{ext}"
                )
            )

        log(
            f"Possible excel paths are : {paths}.",
            source_data=source_data,
            level=logging.DEBUG,
        )

        log(
            " 'generate_excel_file_paths' method completed successfully.",
            source_data=source_data,
            level=logging.DEBUG,
        )

        return paths

    def process_started_worker(self, source_data: SourceData):
        log(
            "CleanupHotFilesWorker Started.",
            source_data=source_data,
            level=logging.INFO,
        )

        # Update the worker status in the database.
        IADSCoreStatus = WorkerName + "_" + InProgress
        source_data.IADSCoreStatus = IADSCoreStatus
        source_data.save()

        # Getting files path from which we are deleting file.
        hot_file_path = source_data.SourceDatasetPath

        log(
            f"file path from which we are deleting file is: {hot_file_path}",
            source_data=source_data,
            level=logging.INFO,
        )

        log(
            "Checking if file is related to hot folder 1.",
            source_data=source_data,
            level=logging.DEBUG,
        )

        if (
            source_data.SourceDatasetType == "Hot Folder 1"
            and source_data.IsExcelPresent
        ):
            log(
                "File is related to hot folder 1.",
                source_data=source_data,
                level=logging.DEBUG,
            )
            log(
                "Generating possible excel paths for it.",
                source_data=source_data,
                level=logging.DEBUG,
            )
            excel_paths = self.generate_excel_file_paths(hot_file_path, source_data)
            for excel_path in excel_paths:
                if os.path.exists(excel_path):
                    try:
                        os.remove(excel_path)
                        log(
                            f"Excel file deleted from HOT Folder which is related to Source Data"
                            f" or Hot File and excel file path is: {excel_path}.",
                            source_data=source_data,
                            level=logging.DEBUG,
                        )
                    except Exception as e:
                        log(
                            f"Error occurred while deleting images: {str(e)} and {traceback.format_exc()}",
                            source_data=source_data,
                            level=logging.ERROR,
                        )
                        IADSCoreStatus = WorkerName + "_" + Failed
                        source_data.IADSCoreStatus = IADSCoreStatus
                        source_data.save()
                        raise e

        if os.path.exists(hot_file_path):
            try:
                # Deleting HOT file
                os.remove(hot_file_path)

                log(
                    "Image Successfully deleted from HOT Folder.",
                    source_data=source_data,
                    level=logging.DEBUG,
                )

                # Update Source Data
                source_data.Is_deleted_from_hot_folder = True
                source_data.save()
                log(
                    "Set Is_deleted_from_hot_folder flag to True in SourceData table.",
                    source_data=source_data,
                    level=logging.DEBUG,
                )

            except Exception as e:
                log(
                    f"Error occurred while deleting images: {str(e)} and {traceback.format_exc()}",
                    source_data=source_data,
                    level=logging.ERROR,
                )
                IADSCoreStatus = WorkerName + "_" + Failed
                source_data.IADSCoreStatus = IADSCoreStatus
                source_data.save()
                raise e
        else:
            source_data.Is_deleted_from_hot_folder = True
            source_data.save()
            log(
                "Hot File is already deleted from HotFolder.",
                source_data=source_data,
                level=logging.DEBUG,
            )

        # Update the worker status in the database.
        IADSCoreStatus = WorkerName + "_" + Completed
        source_data.IADSCoreStatus = IADSCoreStatus
        source_data.save()
        # Update worker status
        log(
            "CleanupHotFilesWorker Completed.",
            source_data=source_data,
            level=logging.INFO,
        )

    def start(self, source_data: SourceData):
        self.process_started_worker(source_data)
