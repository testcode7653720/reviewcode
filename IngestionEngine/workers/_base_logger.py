import json
import logging
from django.conf import settings
from django.utils import timezone

from IngestionEngine.models import IngestionLogs

HostID = settings.HOST_ID
logger = logging.getLogger("ingestion-engine")


class Logger:
    def __init__(self, worker) -> None:
        self.worker = worker

    def log(
        self,
        msg: str,
        source_data=None,
        target_data=None,
        clip_request=None,
        collection_request=None,
        sat_image_tile=None,
        sat_item=None,
        level=logging.INFO,
        **kwargs
    ):
        if level == logging.INFO:
            msg_dict = self._get_msg_dict(
                msg,
                level="INFO",
                source_data=source_data,
                target_data=target_data,
                clip_request=clip_request,
                collection_request=collection_request,
                sat_image_tile=sat_image_tile,
                sat_item=sat_item,
                **kwargs
            )
            logger.info(json.dumps(msg_dict))
            IngestionLogs.objects.create(
                LogType="INFO",
                Timestamp=timezone.now(),
                LogMessage=msg,
                AffectedSourceID=source_data,
                AffectedTargetID=target_data,
                AffectedClipID=clip_request,
                AffectedCollectionID=collection_request,
                AffectedSatTileID=sat_image_tile,
                AffectedSatID=sat_item,
                WorkerName=self.worker,
                HostID=HostID,
            )
        elif level == logging.DEBUG:
            msg_dict = self._get_msg_dict(
                msg,
                level="DEBUG",
                source_data=source_data,
                target_data=target_data,
                clip_request=clip_request,
                collection_request=collection_request,
                sat_image_tile=sat_image_tile,
                sat_item=sat_item,
                **kwargs
            )
            logger.debug(json.dumps(msg_dict))
            IngestionLogs.objects.create(
                LogType="DEBUG",
                Timestamp=timezone.now(),
                LogMessage=msg,
                AffectedSourceID=source_data,
                AffectedTargetID=target_data,
                WorkerName=self.worker,
                HostID=HostID,
                AffectedClipID=clip_request,
                AffectedCollectionID=collection_request,
                AffectedSatTileID=sat_image_tile,
                AffectedSatID=sat_item,
            )
        elif level == logging.WARNING:
            msg_dict = self._get_msg_dict(
                msg,
                level="WARNING",
                source_data=source_data,
                target_data=target_data,
                clip_request=clip_request,
                collection_request=collection_request,
                sat_image_tile=sat_image_tile,
                sat_item=sat_item,
                **kwargs
            )
            logger.warning(json.dumps(msg_dict))
            IngestionLogs.objects.create(
                LogType="WARNING",
                Timestamp=timezone.now(),
                LogMessage=msg,
                AffectedSourceID=source_data,
                AffectedTargetID=target_data,
                WorkerName=self.worker,
                HostID=HostID,
                AffectedClipID=clip_request,
                AffectedCollectionID=collection_request,
                AffectedSatTileID=sat_image_tile,
                AffectedSatID=sat_item,
            )
        elif level == logging.ERROR:
            msg_dict = self._get_msg_dict(
                msg,
                level="ERROR",
                source_data=source_data,
                target_data=target_data,
                clip_request=clip_request,
                collection_request=collection_request,
                sat_image_tile=sat_image_tile,
                sat_item=sat_item,
                **kwargs
            )
            logger.error(json.dumps(msg_dict))
            IngestionLogs.objects.create(
                LogType="ERROR",
                Timestamp=timezone.now(),
                LogMessage=msg,
                AffectedSourceID=source_data,
                AffectedTargetID=target_data,
                WorkerName=self.worker,
                HostID=HostID,
                AffectedClipID=clip_request,
                AffectedCollectionID=collection_request,
                AffectedSatTileID=sat_image_tile,
                AffectedSatID=sat_item,
            )

    def get_logger(self):
        return self.log

    def _get_msg_dict(
        self,
        msg: str,
        level: str,
        source_data=None,
        target_data=None,
        clip_request=None,
        collection_request=None,
        sat_image_tile=None,
        sat_item=None,
        **kwargs
    ):
        """Create logs in the database as well as file."""
        msg_dict = {"worker": self.worker, "msg": msg, "level": level}
        if source_data is not None:
            msg_dict["source_data"] = str(source_data)
        if target_data is not None:
            msg_dict["target_data"] = str(target_data)
        if clip_request is not None:
            msg_dict["clip_request"] = str(clip_request)
        if collection_request is not None:
            msg_dict["collection_request"] = str(collection_request)
        if sat_image_tile is not None:
            msg_dict["sat_image_tile"] = str(sat_image_tile)
        if sat_item is not None:
            msg_dict["sat_item"] = str(sat_item)

        msg_dict.update(kwargs)
        return msg_dict
