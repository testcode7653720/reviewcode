import os
import shutil
import traceback

from django.conf import settings

from ._base_logger import Logger
from ._base_worker import BaseWorker
from IngestionEngine.models import SourceData, TargetData, TargetImage
import logging


log = Logger("IngesterWorker").get_logger()


WorkerName = "IngesterWorker"
InProgress = "InProgress"
Completed = "Completed"
Failed = "Failed"
Scheduled = "Scheduled"
Pause = "Pause"


class IngesterWorker(BaseWorker):

    """IngesterWorker copies tiff and cog files on NAS Drive."""

    def __init__(self) -> None:
        super().__init__()

    def find_eligible_items(self):
        """Find items that are ready for IngesterWorker."""
        try:
            log("Searching eligible items for IngesterWorker.", level=logging.DEBUG)
            source_datas = SourceData.objects.filter(
                IADSCoreStatus__in=(
                    "CogGeneratorWorker_Completed",
                    f"{WorkerName}_{Failed}",
                )
            )
            if len(source_datas) == 0:
                log(f"No SourceData found for {WorkerName}", level=logging.WARN)
            return source_datas
        except Exception as e:
            log(
                f"Oops!! Error has Occured while searching eligible items and error is: {str(e)} and {traceback.format_exc()}",
                level=logging.ERROR,
            )
            raise e

    def cleanup_file(
        self, input_cog_path, source_data: SourceData, target_data: TargetData
    ):
        """Cleanup local COG file."""
        try:
            os.remove(input_cog_path)
            return True
        except Exception as e:
            log(
                f"Failed to delete local COG file: {str(e)} and {traceback.format_exc()}",
                source_data=source_data,
                target_data=target_data,
                level=logging.ERROR,
            )
            return False

    def cleanup_folder_contents(
        self, folder_path, source_data: SourceData, target_data: TargetData
    ):
        """Cleanup all folders inside the specified directory."""
        try:
            shutil.rmtree(folder_path)
            log(
                f"Folder at Path: {folder_path} deleted successfully.",
                source_data=source_data,
                target_data=target_data,
                level=logging.INFO,
            )
        except Exception as e:
            log(
                f"Failed to delete folders at: {folder_path}: {str(e)} and {traceback.format_exc()}",
                source_data=source_data,
                target_data=target_data,
                level=logging.ERROR,
            )
            raise e

    def copy_file_on_nas(
        self,
        input_path,
        output_path,
        source_data: SourceData,
        target_data: TargetData,
    ):
        if os.path.exists(output_path):
            log(
                f"File already exists at destination: {output_path}. Skipping copy.",
                level=logging.DEBUG,
            )
            return True
        else:
            # Copy input COG file to NAS Drive
            log(
                "Copying Local file to NAS Drive.",
                source_data=source_data,
                target_data=target_data,
                level=logging.DEBUG,
            )

            try:
                shutil.copy2(
                    input_path,
                    output_path,
                )
                log(
                    "Local file Copied to NAS Drive.",
                    source_data=source_data,
                    target_data=target_data,
                    level=logging.INFO,
                )
                return True

            except Exception as e:
                log(
                    f"Failed to copy local file to NAS Drive: {str(e)} and {traceback.format_exc()}",
                    source_data=source_data,
                    target_data=target_data,
                    level=logging.ERROR,
                )
                return False

    def process_started_worker(self, source_data: SourceData):
        """Process started for FindFailedItem Worker."""

        log(
            "Ingester Worker Started.",
            source_data=source_data,
            level=logging.INFO,
        )
        source_data.IADSCoreStatus = WorkerName + "_" + InProgress
        source_data.save()
        target_data = TargetData.objects.get(SourceData=source_data)

        # target_image = TargetImage.objects.filter(TargetData=target_data)

        nas_drive_path = settings.SERVER_PATH
        log(
            f"NAS Drive path is: {nas_drive_path}",
            source_data=source_data,
            target_data=target_data,
            level=logging.DEBUG,
        )

        target_image: TargetImage
        target_images = target_data.TargetImages.all()

        # Get the first image
        first_image = target_images.first()

        log(
            f"Need to Copy {len(target_images)} Images.",
            source_data=source_data,
            target_data=target_data,
            level=logging.DEBUG,
        )

        for i, target_image in enumerate(target_images):
            input_cog_path = target_image.LocalCOGPath
            input_tiff_path = target_image.LocalTiffPath

            log(
                f"Local Tiff path is:{input_tiff_path}",
                source_data=source_data,
                target_data=target_data,
                level=logging.INFO,
            )

            log(
                f"Local COG path is:{input_cog_path}",
                source_data=source_data,
                target_data=target_data,
                level=logging.INFO,
            )

            output_tiff_path = target_image.Path

            log(
                f"Nas Drive Tiff path is:{output_tiff_path}",
                source_data=source_data,
                target_data=target_data,
                level=logging.INFO,
            )

            output_cog_path = target_image.COGPath

            log(
                f"Nas Drive COG path is:{output_cog_path}",
                source_data=source_data,
                target_data=target_data,
                level=logging.INFO,
            )

            is_tiff_copied = self.copy_file_on_nas(
                input_tiff_path, output_tiff_path, source_data, target_data
            )

            log(
                "Local tiff file copied to NAS Drive Successfully.",
                source_data=source_data,
                target_data=target_data,
                level=logging.INFO,
            )

            is_cog_copied = self.copy_file_on_nas(
                input_cog_path, output_cog_path, source_data, target_data
            )

            log(
                "Local COG file copied to NAS Drive Successfully.",
                source_data=source_data,
                target_data=target_data,
                level=logging.INFO,
            )

            if is_tiff_copied and is_cog_copied:
                target_image.is_tiff_copied_on_nas = True
                target_image.is_cog_copied_on_nas = True
                target_image.save()

                is_tiff_deleted_from_local = self.cleanup_file(
                    input_tiff_path, source_data, target_data
                )

                is_cog_deleted_from_local = self.cleanup_file(
                    input_cog_path, source_data, target_data
                )

                if is_tiff_deleted_from_local and is_cog_deleted_from_local:
                    log(
                        "Local tiff and COG files deleted successfully.",
                        source_data=source_data,
                        target_data=target_data,
                        level=logging.DEBUG,
                    )
                    target_image.is_deleted_from_local = True
                    target_image.save()

                else:
                    log(
                        "Failed to delete local tiff and COG file.",
                        source_data=source_data,
                        target_data=target_data,
                        level=logging.ERROR,
                    )
                    source_data.IADSCoreStatus = WorkerName + "_" + Failed
                    source_data.IADSIngestionStatus = "Ingestion_Failed"
                    source_data.target_data.IADSIngestionStatus = "Ingestion_Failed"
                    source_data.target_data.save()
                    source_data.save()
            else:
                log(
                    "Failed to copy Local tiff and COG file to NAS Drive.",
                    source_data=source_data,
                    target_data=target_data,
                    level=logging.ERROR,
                )
                source_data.IADSCoreStatus = WorkerName + "_" + Failed
                source_data.IADSIngestionStatus = "Ingestion_Failed"
                source_data.target_data.IADSIngestionStatus = "Ingestion_Failed"
                source_data.target_data.save()
                source_data.save()

        local_cog_path = first_image.LocalCOGPath

        log(
            f"Local COG file path for first file is:{local_cog_path}",
            source_data=source_data,
            target_data=target_data,
            level=logging.DEBUG,
        )

        # Extract folder path from LocalCOGPath
        folder_path = os.path.dirname(local_cog_path)

        log(
            f"Local COG folder path is: {folder_path}",
            source_data=source_data,
            target_data=target_data,
            level=logging.DEBUG,
        )

        # Get the last folder name
        last_folder_name = os.path.basename(folder_path)
        log(
            f"Last folder name from path is: {last_folder_name}",
            source_data=source_data,
            target_data=target_data,
            level=logging.DEBUG,
        )

        log(
            "If last folder is not 'Local_Ingester_Folder' then delete that folder",
            source_data=source_data,
            target_data=target_data,
            level=logging.DEBUG,
        )

        if not last_folder_name == "Local_Ingester_Folder":
            self.cleanup_folder_contents(folder_path, source_data, target_data)

        # deleting zip file from local ingester folder

        input_zip_path = target_data.LocalTargetDatasetPath

        if ".zip" in input_zip_path:
            is_zip_deleted = self.cleanup_file(input_zip_path, source_data, target_data)

            if is_zip_deleted:
                log(
                    "Local zip file deleted successfully.",
                    source_data=source_data,
                    target_data=target_data,
                    level=logging.DEBUG,
                )
            else:
                log(
                    "Failed to delete local zip file.",
                    source_data=source_data,
                    target_data=target_data,
                    level=logging.ERROR,
                )
                source_data.IADSCoreStatus = WorkerName + "_" + Failed
                source_data.IADSIngestionStatus = "Ingestion_Failed"
                source_data.target_data.IADSIngestionStatus = "Ingestion_Failed"
                source_data.target_data.save()
                source_data.save()

        else:
            log(
                "Local files already deleted.",
                source_data=source_data,
                target_data=target_data,
                level=logging.DEBUG,
            )

        source_data.IADSCoreStatus = WorkerName + "_" + Completed
        source_data.IADSIngestionStatus = "Ingestion_Completed"
        source_data.CTPIIngestionStatus = "Ready_For_CTPIIngestion"
        source_data.target_data.IADSIngestionStatus = "Ingestion_Completed"
        source_data.target_data.CTPIIngestionStatus = "Ready_For_CTPIIngestion"
        source_data.target_data.save()
        source_data.save()

        log(
            "Updated IADS Core Staus and IADS Ingestion Status.",
            source_data=source_data,
            target_data=target_data,
            level=logging.INFO,
        )

        log(
            "Ingester Worker Completed Successfully.",
            source_data=source_data,
            target_data=target_data,
            level=logging.INFO,
        )

    def start(self, source_data: SourceData):
        self.process_started_worker(source_data)
