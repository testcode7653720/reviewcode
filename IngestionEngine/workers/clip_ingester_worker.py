import traceback
import requests
from IADS import settings
from MnaApp.serializers.clip_request_serializer import ClipRequestInsertMnASerializer
from SatProductCurator.models import NewClipRequest
from ._base_logger import Logger
from ._base_worker import BaseWorker
import logging
from django.db.models import Q
from django.utils import timezone


# Initializing logger with the class name 'ClipIngesterWorker'
log = Logger("ClipIngesterWorker").get_logger()

# Defining constants for the worker statuses
WorkerName = "ClipIngesterWorker"
InProgress = "InProgress"
Completed = "Completed"
Failed = "Failed"
Scheduled = "Scheduled"
Pause = "Pause"
IngestionInProgress = "IngestionInProgress"
url_from_mna = settings.MNA_CLIP_INSERT_URL


class ClipIngesterWorker(BaseWorker):
    """CTPIIngesterWorker is added Clipped image URL in MnA System."""

    def __init__(self) -> None:
        super().__init__()

    # Method to find eligible items for processing.
    def find_eligible_items(self):
        try:
            # Finding completed clip requests that are not ingested.
            log(
                "Searching items which completed clipping and also not ingested.",
                level=logging.DEBUG,
            )
            clip_requests = NewClipRequest.objects.filter(
                Q(IsClipCompleted="True")
                & (
                    Q(ClipIngestionStatus="Ready_For_ClipIngestion")
                    | Q(ClipIngestionStatus="ClipIngestion_Failed")
                )
            )

            return clip_requests

        except Exception as e:
            # Logging error if encountered while finding eligible items
            log(
                f"Error while finding eligible items to start worker: {str(e)} and {traceback.format_exc()}",
                level=logging.ERROR,
            )
            raise e

    def get_access_token(self, clip_request: NewClipRequest):
        url = settings.KEYCLOAK_TOKEN_URL

        # Request payload
        payload = {
            "grant_type": settings.PENTA_GRANT_TYPE,
            "client_id": settings.PENTA_CLIENT_ID,
            "client_secret": settings.PENTA_CLIENT_SECRET,
            "username": settings.PENTA_API_USERNAME,
            "password": settings.PENTA_AUTH_PASSWORD,
        }

        # Headers
        headers = {"Content-Type": settings.CONTENT_TYPE}

        try:
            log(
                "Making request to get access token.",
                clip_request=clip_request,
                level=logging.DEBUG,
            )
            response = requests.post(
                url,
                data=payload,
                headers=headers,
                verify=False,
            )
            response.raise_for_status()  # Raise an exception for 4xx/5xx status codes

            # Extracting access token from the response
            log(
                "Extracting access token from the response.",
                clip_request=clip_request,
                level=logging.DEBUG,
            )
            access_token = response.json().get("access_token")

            if access_token:
                return access_token
            else:
                # If access token is not found in the response
                log(
                    "Access token not found in response",
                    clip_request=clip_request,
                    level=logging.WARNING,
                )
                return None

        except requests.exceptions.RequestException as e:
            log(
                f"Error while fetching Access Token: {str(e)} and {traceback.format_exc()}",
                clip_request=clip_request,
                level=logging.ERROR,
            )
            return None

    def insert_clip_image_into_mna(self, clip_request: NewClipRequest):
        access_token = self.get_access_token(clip_request)
        if access_token:
            log(
                f"Access Token: {access_token}",
                clip_request=clip_request,
                level=logging.DEBUG,
            )

            url_from_mna = settings.MNA_CLIP_INSERT_URL

            headers = {
                "Authorization": access_token,
                "PentaOrgID": settings.PENTA_ORG_ID,
                "PentaUserRole": settings.PENTA_USER_ROLE,
                "PentaSelectedLocale": settings.PENTA_SELECTED_LOCALE,
            }

            # Serialize the target data.
            log(
                "Serializing Target Data.",
                clip_request=clip_request,
                level=logging.DEBUG,
            )

            serializer = ClipRequestInsertMnASerializer(clip_request)

            log(
                "Extracting data from serializer.",
                clip_request=clip_request,
                level=logging.DEBUG,
            )
            data_to_send = serializer.data
            log(
                f"Data extraction successful. Data to send:{data_to_send}.",
                clip_request=clip_request,
                level=logging.DEBUG,
            )
            log(
                "Successfully serialized the data.",
                clip_request=clip_request,
                level=logging.DEBUG,
            )

            try:
                # Make API Requests.
                log(
                    f"Creating API request to MnA. URL: {url_from_mna} | Headers: {headers} | JSON: {data_to_send}",
                    clip_request=clip_request,
                    level=logging.DEBUG,
                )

                response = requests.post(
                    url_from_mna,
                    headers=headers,
                    json=data_to_send,
                    verify=False,
                )

                log(
                    f"Request url is: {response.request.url}",
                    clip_request=clip_request,
                    level=logging.DEBUG,
                )
                log(
                    f"Request body is: {response.request.body}",
                    clip_request=clip_request,
                    level=logging.DEBUG,
                )
                log(
                    f"Request headers is: {response.request.headers}",
                    clip_request=clip_request,
                    level=logging.DEBUG,
                )

                log(
                    f"Response from MnA in text format is : {response.text}",
                    clip_request=clip_request,
                    level=logging.DEBUG,
                )

                log(
                    f"Response code from MnA: {response.status_code}",
                    clip_request=clip_request,
                    level=logging.DEBUG,
                )
                log(
                    f"Response from MnA is: {response.text}",
                    clip_request=clip_request,
                    level=logging.DEBUG,
                )
            except requests.RequestException as e:
                log(
                    f"Request failed: {e} and {traceback.format_exc()}",
                    clip_request=clip_request,
                    level=logging.DEBUG,
                )

            if response.status_code == 200:
                # Updating ClipIngestionStatus to 'ClipIngestionCompleted' if successful
                log(
                    "Set ClipIngestionStatus as Completed.",
                    clip_request=clip_request,
                    level=logging.DEBUG,
                )
                clip_request.ClipIngestionStatus = "ClipIngestion_Completed"
                log(
                    "ClipIngestionStatus updated Successfully.",
                    clip_request=clip_request,
                    level=logging.DEBUG,
                )
                clip_request.save()

                return True, response.status_code
            else:
                return False, response.status_code

    def process_started_worker(self, clip_request: NewClipRequest):
        # Logging the initiation of ClipIngesterWorker
        log(
            "ClipIngesterWorker Started.",
            clip_request=clip_request,
            level=logging.INFO,
        )
        clip_request.ClipIngestionStatus = "ClipIngestion_InProgress"
        clip_request.save()

        # Initiating the process to insert clipped image into MnA System
        log(
            "Reached at the 'insert_clip_image_into_mna' Method & About to Execute.",
            clip_request=clip_request,
            level=logging.DEBUG,
        )
        insert_into_mna, response_status = self.insert_clip_image_into_mna(clip_request)
        log(
            "'insert_clip_image_into_mna' Method executed Sucessfully.",
            clip_request=clip_request,
            level=logging.DEBUG,
        )

        # Checking the result of inserting clipped image into MnA System
        if insert_into_mna:
            # Update ClipIngestedDate after successful completion
            clip_request.ClipIngestedDate = timezone.now()
            clip_request.save()
            log(
                "Updated ClipIngestedDate with current Datetime.",
                clip_request=clip_request,
                level=logging.DEBUG,
            )
            # Logging successful completion of ClipIngesterWorker
            log(
                "Well Done!! ClipIngesterWorker Successfully Completed.",
                clip_request=clip_request,
                level=logging.INFO,
            )
            log(
                f"Operation Successful and response code is: {response_status}",
                level=logging.DEBUG,
            )
        else:
            # Logging failure of ClipIngesterWorker
            log(
                "Clip IngesterWorker Failed.",
                clip_request=clip_request,
                level=logging.INFO,
            )
            log(
                f"Operation Failed and response code is: {response_status}",
                level=logging.DEBUG,
            )
            clip_request.ClipIngestionStatus = "ClipIngestion_Failed"
            clip_request.save()

    def start(self, clip_request: NewClipRequest):
        # Initiating the process for ClipIngesterWorker
        log(
            "Now Starting ClipIngesterWorker.",
            clip_request=clip_request,
            level=logging.INFO,
        )
        self.process_started_worker(clip_request)
