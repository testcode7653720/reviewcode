

from IngestionEngine.models import SourceData

from .._base_logger import Logger
from .._base_worker import BaseWorker

log = Logger('ItemValidatorWorker').get_logger()


class ItemValidatorWorker(BaseWorker):

    def __init__(self) -> None:
        super().__init__()

    def eligible_item_reader(self, source_data: SourceData) -> bool:
        log('Reading Eligibility', file=str(source_data))
        log('Eligible', file=str(source_data))
        return True

    def process_started_worker(self, source_data: SourceData):
        log('Starting Worker Process', file=str(source_data))

    def start(self, source_data: SourceData):
        if self.eligible_item_reader(source_data):
            self.process_started_worker(source_data)
