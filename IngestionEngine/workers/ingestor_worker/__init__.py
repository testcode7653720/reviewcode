from .._base_logger import Logger
from .._base_worker import BaseWorker

from .item_validator_worker import ItemValidatorWorker
from IngestionEngine.models import SourceData


log = Logger('IngestorWorker').get_logger()


class IngestorWorker(BaseWorker):

    def __init__(self) -> None:
        super().__init__()

    def process_started_worker(self,  hot_file: SourceData):
        log('Starting Worker Process', file=str(hot_file))
        validator_worker = ItemValidatorWorker()
        validator_worker.start(hot_file)

    def start(self, hot_file: SourceData):
        self.process_started_worker(hot_file)
