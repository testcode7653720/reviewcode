import traceback
from IngestionEngine.models import IngestionLogs
from ._base_logger import Logger
from ._base_worker import BaseWorker
import logging
from django.conf import settings
from django.utils import timezone
from datetime import timedelta


log = Logger("CleanupLogsWorker").get_logger()
WorkerName = "CleanupHotLogsWorker"
InProgress = "InProgress"
Completed = "Completed"
Failed = "Failed"
Scheduled = "Scheduled"
Pause = "Pause"
IngestionInProgress = "IngestionInProgress"


class CleanupLogsWorker(BaseWorker):
    """CleanupLogsWorker is deleted logs related to Source Data."""

    def __init__(self) -> None:
        super().__init__()

    def find_eligible_items(self):
        try:
            # Calculate the threshold datetime
            threshold_datetime = timezone.now() - timedelta(
                days=settings.DELETE_LOGS_AFTER_DAYS
            )

            # Finding log_items to delete.
            logs_items = IngestionLogs.objects.filter(
                ModifiedOn__lte=threshold_datetime
            )[:1000]

            if len(logs_items) == 0:
                log(f"No logs found for {WorkerName}", level=logging.WARNING)
            return logs_items

        except Exception as e:
            log(
                f"Error while finding eligible items to start worker: {str(e)} and {traceback.format_exc()}",
                level=logging.ERROR,
            )
            raise e

    def process_started_worker(self, log_item: IngestionLogs):
        # deleting log_item
        try:
            log_item.delete()

        except Exception as e:
            raise e

    def start(self, log_item: IngestionLogs):
        self.process_started_worker(log_item)
