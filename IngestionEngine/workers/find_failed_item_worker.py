import traceback

from django.conf import settings

from ._base_logger import Logger
from ._base_worker import BaseWorker
from IngestionEngine.models import SourceData
import logging
from django.utils import timezone
from datetime import timedelta


log = Logger("FindFailedItemWorker").get_logger()


WorkerName = "FindFailedItemWorker"
InProgress = "InProgress"
Completed = "Completed"
Failed = "Failed"
Scheduled = "Scheduled"
Pause = "Pause"


class FindFailedItemWorker(BaseWorker):

    """FindFailedItemWorker finds items that are InProgress or Failed."""

    def __init__(self) -> None:
        super().__init__()

    def find_eligible_items(self):
        # List of worker names
        worker_names = [
            # "PreIngestionWorker",
            # "SDReaderWorker",
            "IngesterWorker",
            "CogGeneratorWorker",
            "SatDownloadWorker",
        ]
        workers_in_progress = []  # List to store items in progress

        for worker_name in worker_names:
            log(
                "Finding items which is having status IngesterWorker_InProgress and CogGeneratorWorker_InProgress",
                level=logging.DEBUG,
            )
            # Query for items in progress
            worker_in_progress = SourceData.objects.filter(
                IADSCoreStatus__icontains=f"{worker_name}_InProgress"
            )
            workers_in_progress.extend(worker_in_progress)

        return workers_in_progress  # return a list of InProgress items.

    def process_started_worker(self, source_data: SourceData):
        """Process started for FindFailedItem Worker."""

        log(
            "FindFailedItem Worker Started.",
            source_data=source_data,
            level=logging.INFO,
        )
        try:
            # Calculate time difference since last modification
            time_difference = timezone.now() - source_data.ModifiedOn

            log(
                f"Calculated time difference since last modification and time difference is: {time_difference}",
                source_data=source_data,
                level=logging.DEBUG,
            )

            # Define time differences for different workers
            log(
                "Defining time differences for different workers.",
                source_data=source_data,
                level=logging.DEBUG,
            )
            worker_time_diff = {
                "CogGeneratorWorker": timedelta(
                    seconds=settings.FAIL_WORKER_AFTER_SECONDS
                ),
                "IngesterWorker": timedelta(seconds=settings.FAIL_WORKER_AFTER_SECONDS),
            }

            # Iterate over workers and their respective time differences
            log(
                "Iterating over workers and their respective time differences.",
                source_data=source_data,
                level=logging.DEBUG,
            )

            for worker_name, time_diff in worker_time_diff.items():
                if worker_name in source_data.IADSCoreStatus:
                    # Check if time difference exceeds threshold
                    log(
                        f"Check if time difference exceeds threshold and threshold time is:{time_diff}",
                        source_data=source_data,
                        level=logging.DEBUG,
                    )

                    if time_difference >= time_diff:
                        source_data.IADSCoreStatus = source_data.IADSCoreStatus.replace(
                            "_InProgress", "_Failed"
                        )
                        source_data.IADSIngestionStatus = "Ingestion_Failed"
                        source_data.save()
                        log(
                            "Update status to Failed.",
                            source_data=source_data,
                            level=logging.DEBUG,
                        )
                    else:
                        # Log a message indicating that the time difference is not greater than the threshold
                        log(
                            "Time difference is not greater than the threshold. No action taken.",
                            source_data=source_data,
                            level=logging.DEBUG,
                        )
        except Exception as e:
            log(
                f"An error occurred while processing started worker: {str(e)} and {traceback.format_exc()}",
                source_data=source_data,
                level=logging.ERROR,
            )

        log(
            "FindFailedItem Worker Completed.",
            source_data=source_data,
            level=logging.INFO,
        )

    def start(self, source_data: SourceData):
        self.process_started_worker(source_data)
