# Import necessary libraries and modules.
import traceback
from django.conf import settings
import pandas as pd
import rasterio
from django.contrib.gis.geos import Polygon
from ._base_logger import Logger
from ._base_worker import BaseWorker
import os
from IngestionEngine.models import SourceData, TargetData, BusinessMetadata, TargetImage
from SatProductCurator.services import SatelliteImageTileService
import logging
import shutil
from rasterio.crs import CRS
from dateutil.parser import parse as parse_datetime
from datetime import datetime
from SatProductCurator.models.constants import PRODUCT_SENTINEL2


# Create a logger instance.
log = Logger("LocalIngesterWorker").get_logger()

# Define constants for worker status.
PrevoiusWorkerStatus = ("SDReaderWorker_Completed", "SatDownloadWorker_Completed")
WorkerName = "LocalIngesterWorker"
InProgress = "InProgress"
Completed = "Completed"
Failed = "Failed"
Scheduled = "Scheduled"
Pause = "Pause"


class LocalIngesterWorker(BaseWorker):
    # Ingester Worker is fetching metadata of item from rasterio as well as
    # from excel/csv file and copy to destination path.

    def __init__(self) -> None:
        super().__init__()

    # Method to find items that are ready for Ingester Worker.
    def find_eligible_items(self):
        """Find items that are ready for Ingester Worker"""
        try:
            log("Find eligible items for LocalIngesterWorker.", level=logging.DEBUG)
            source_datas = SourceData.objects.filter(
                IADSCoreStatus__in=(
                    "SDReaderWorker_Completed",
                    f"{WorkerName}_{Failed}",
                )
            )
            if len(source_datas) == 0:
                log(f"No SourceData found for {WorkerName}", level=logging.WARN)
            return source_datas
        except Exception as e:
            log(
                f"Error while finding eligible items to start worker: {str(e)} and {traceback.format_exc()}",
                level=logging.ERROR,
            )

    # Method to check eligibility of an item for Ingester Worker.
    def eligible_item_reader(self, source_data: SourceData):
        """Find eligibility of item to start Ingester Worker"""
        try:
            log(
                "Reached at the 'eligible_item_reader' Method & About to Execute.",
                source_data=source_data,
                level=logging.DEBUG,
            )
            # get the previous worker status from the database.
            previous_worker_status = source_data.IADSCoreStatus

            # Check if status from database is "SDReaderWorker_Completed".
            if previous_worker_status in (
                "SDReaderWorker_Completed",
                f"{WorkerName}_{Failed}",
            ):
                log(
                    "Well Done!! Item is Eligible to execute LocalIngesterWorker.",
                    source_data=source_data,
                    level=logging.INFO,
                )
                return True

            else:
                log(
                    "Oops!!.Not an Eligible Item & item InProgress or might be Already Processed.",
                    source_data=source_data,
                    level=logging.WARNING,
                )
                return False
        except Exception as e:
            log(
                f"Oops!! Error has Occured while checking Eligibility of LocalIngesterWorker and error is: {str(e)} and {traceback.format_exc()}",
                source_data=source_data,
                level=logging.ERROR,
            )
            raise e

    # Method to start the Ingester Worker process.
    def process_started_worker(self, source_data: SourceData):
        """Process started for Ingester Worker"""

        log(
            "Reached at the 'process_started_worker' Method & About to Execute.",
            source_data=source_data,
            level=logging.DEBUG,
        )

        log(
            "Well Done!! LocalIngesterWorker Started.",
            source_data=source_data,
            level=logging.INFO,
        )
        IADSCoreStatus = WorkerName + "_" + InProgress
        source_data.IADSCoreStatus = IADSCoreStatus
        source_data.save()
        log(
            "Obtaining Metadata of a File.", source_data=source_data, level=logging.INFO
        )

        # Fetch Metadata
        if source_data.IsZippedFile and source_data.SatelliteImageTile:
            # Fetch metadata for zipped file
            metadata = SatelliteImageTileService().fetch_metadata(
                tile=source_data.SatelliteImageTile, source_data=source_data
            )
        else:
            log(
                "Fetching Metadata with Rasterio.",
                source_data=source_data,
                level=logging.DEBUG,
            )

            # Fetch metadata for non-zipped files like tif, jpeg files
            log("Fetching Metadata.", source_data=source_data, level=logging.INFO)
            log(
                "Getting Metadata with Rasterio.",
                source_data=source_data,
                level=logging.DEBUG,
            )
            log(
                "Searching if File exists at Specific Location.",
                source_data=source_data,
                level=logging.DEBUG,
            )
            # Check if file exists at specific location.
            if os.path.isfile(source_data.SourceDatasetPath):
                try:
                    log(
                        "Rasterio About to Read the Files to Retrieve the Metadata.",
                        source_data=source_data,
                        level=logging.DEBUG,
                    ),
                    # Open file with rasterio to fetch the metadata.
                    with rasterio.open(source_data.SourceDatasetPath) as dataset:
                        log(
                            "Rasterio Started Reading the Files.",
                            source_data=source_data,
                            level=logging.DEBUG,
                        ),
                        # set epsg code None.
                        EpsgCode = None
                        log(
                            "Checking Raster Dataset whether it has CRS assigned or defined.",
                            source_data=source_data,
                            level=logging.DEBUG,
                        )
                        # Check if the dataset has a CRS information.
                        if dataset.crs:
                            # EpsgCode = dataset.crs.to_string()
                            log(
                                "Create a CRS object Using the Information From dataset.crs.",
                                source_data=source_data,
                                level=logging.DEBUG,
                            )
                            # Create a CRS object using the information from dataset.crs
                            crs = CRS(dataset.crs)
                            log(
                                "Checking if the CRS object exists and if it has an EPSG code Associated with it.",
                                source_data=source_data,
                                level=logging.DEBUG,
                            )
                            # Check if the CRS (Coordinate Reference System) object exists and has an EPSG code
                            if crs and crs.is_epsg_code:
                                log(
                                    "Getting the EPSG code from the CRS object.",
                                    source_data=source_data,
                                    level=logging.DEBUG,
                                )
                                # Get the EPSG code from the CRS object
                                EpsgCode = crs.to_epsg()
                            else:
                                log(
                                    "Epsg code is not available.",
                                    source_data=source_data,
                                    level=logging.WARNING,
                                )
                        # Get the width of the dataset, representing the number of pixels
                        NumPixels = dataset.width
                        log(
                            f"Obtained Number of pixels: {NumPixels}",
                            source_data=source_data,
                            level=logging.DEBUG,
                        )

                        # Get the height of the dataset, representing the number of layers
                        NumLayers = dataset.height
                        log(
                            f"Obtained Number of layers: {NumLayers}",
                            source_data=source_data,
                            level=logging.DEBUG,
                        )

                    HorizontalResolution = dataset.res[0]
                    log(
                        f"Obtained Horizontal Resolution: {HorizontalResolution}",
                        source_data=source_data,
                        level=logging.DEBUG,
                    )

                    extent = Polygon(
                        [
                            (dataset.bounds.left, dataset.bounds.bottom),
                            (dataset.bounds.right, dataset.bounds.bottom),
                            (dataset.bounds.right, dataset.bounds.top),
                            (dataset.bounds.left, dataset.bounds.top),
                            (dataset.bounds.left, dataset.bounds.bottom),
                        ],
                        srid=dataset.crs.to_epsg(),
                    )

                    log(
                        "Transforming extent to EPSG: 4326",
                        source_data=source_data,
                        level=logging.DEBUG,
                    )
                    extent.transform(4326)

                    log(
                        f"Obtained extent of a file: {extent}.",
                        source_data=source_data,
                        level=logging.DEBUG,
                    )
                    metadata = {
                        "EpsgCode": EpsgCode,
                        "NumPixels": NumPixels,
                        "NumLayers": NumLayers,
                        "HorizontalResolution": HorizontalResolution,
                        "Extent": extent,
                    }
                    log(
                        f"Well Done!! Metadata obtained Successfully from Rasterio: {metadata}.",
                        source_data=source_data,
                        level=logging.DEBUG,
                    )

                except Exception as e:
                    log(
                        f"Oops!! Error has Occured in Metadata Fetching from Rasterio: {str(e)} and {traceback.format_exc()}",
                        source_data=source_data,
                        file=str(source_data.SourceDatasetName),
                        level=logging.WARNING,
                    )
            else:
                IADSCoreStatus = WorkerName + "_" + Failed
                source_data.IADSCoreStatus = IADSCoreStatus
                source_data.IADSIngestionStatus = "Ingestion_Failed"
                source_data.save()
                source_data.target_data.IADSIngestionStatus = "Ingestion_Failed"
                source_data.target_data.save()

                raise ValueError(
                    "Ingestion Failed because file does not exist at HOT FOLDER."
                )

        log(
            f"Metadata fetched: {metadata}.",
            source_data=source_data,
            file=str(source_data.SourceDatasetName),
        )

        target_data = TargetData.objects.get(SourceData=source_data)
        log(
            "Getting Target data which is Related to Source Data. ",
            source_data=source_data,
            target_data=target_data,
            level=logging.DEBUG,
        )

        # Feed metadata to database
        try:
            log(
                "Checking if Business Metadata is exists in Database or not.",
                source_data=source_data,
                target_data=target_data,
                level=logging.DEBUG,
            )
            business_metadata = BusinessMetadata.objects.get(TargetData=target_data)

            log(
                "Updating existing Business metadata",
                source_data=source_data,
                target_data=target_data,
                level=logging.DEBUG,
            )
            business_metadata.__dict__.update(metadata)
            business_metadata.save()
        except BusinessMetadata.DoesNotExist:
            # Metadata doesn't exist, create new metadata.
            log(
                "Creating new Business Metadata object for a Target Data.",
                source_data=source_data,
                target_data=target_data,
                level=logging.DEBUG,
            )
            business_metadata = BusinessMetadata(TargetData=target_data, **metadata)
            business_metadata.save()
            log(
                "Metadata of a File which is Fetched from Rasterio Added in Database Successfully.",
                source_data=source_data,
                target_data=target_data,
                level=logging.INFO,
            )

        source_data_path = source_data.SourceDatasetPath
        log(
            f"Source Data File Path is: {source_data_path}",
            source_data=source_data,
            target_data=target_data,
            level=logging.INFO,
        )
        directory, filename = os.path.split(source_data_path)

        filename_without_extension, extension = os.path.splitext(filename)

        # Combine file path creation for .xlsx and .xls
        excel_filenames = [
            filename_without_extension + extension.replace(".", "_") + ext
            for ext in [".xlsx", ".xls", ".csv"]
        ]
        excel_paths = [
            os.path.join(directory, excel_filename)
            for excel_filename in excel_filenames
        ]

        log(
            f"Created Possible Excel and CSV File Paths for Source Data: {excel_paths}",
            source_data=source_data,
            target_data=target_data,
            level=logging.DEBUG,
        )

        # Check if any of the Excel file paths exists in the hot folder
        available_excel_path = None
        log(
            "Checking if any of the Excel/CSV file paths exists in the hot folder.",
            source_data=source_data,
            target_data=target_data,
            level=logging.DEBUG,
        )
        for excel_path in excel_paths:
            if os.path.isfile(excel_path):
                available_excel_path = excel_path
                break

        # Check if we found a valid Excel file path in the previous loop.
        log(
            "Checking if we found a Valid Excel/CSV file path.",
            source_data=source_data,
            target_data=target_data,
            level=logging.DEBUG,
        )
        if available_excel_path:
            log(
                f"Found a valid Excel/CSV file in HotFolder and file path is: {available_excel_path}",
                source_data=source_data,
                target_data=target_data,
                level=logging.DEBUG,
            )

            log(
                "Define a list of Column Names to parse as Dates.",
                source_data=source_data,
                target_data=target_data,
                level=logging.DEBUG,
            )
            date_columns = ["AcquisitionDate", "Date", "ImageAcquisitionDateRange"]

            log(
                "Reading Excel/CSV File into a DataFrame.",
                source_data=source_data,
                target_data=target_data,
                level=logging.DEBUG,
            )
            log(
                "Parsing Specified Columns as Dates.",
                source_data=source_data,
                target_data=target_data,
                level=logging.DEBUG,
            )
            # df = pd.read_excel(available_excel_path, parse_dates=date_columns)
            try:
                df = (
                    pd.read_excel(available_excel_path, parse_dates=date_columns)
                    if available_excel_path.endswith((".xls", ".xlsx"))
                    else pd.read_csv(available_excel_path, parse_dates=date_columns)
                )
                log(
                    "Reading Excel/CSV File Successfully.",
                    source_data=source_data,
                    target_data=target_data,
                    level=logging.DEBUG,
                )
            except Exception as e:
                log(
                    f"Oops!! There might be an Error while Reading the Excel/CSV File: {str(e)} and {traceback.format_exc()}",
                    source_data=source_data,
                    target_data=target_data,
                    level=logging.ERROR,
                )
                raise e

            # Get the fields (attributes) of the BusinessMetadata model
            try:
                fields = BusinessMetadata._meta.get_fields()
            except Exception as e:
                log(
                    f"Oops!! There might be an Error while Reading the Attributes of BusinessMetadata: {str(e)} and {traceback.format_exc()}",
                    source_data=source_data,
                    target_data=target_data,
                    level=logging.ERROR,
                )
                raise e

            # Extract the attribute names in list.
            try:
                business_metadata_attributes = [field.name for field in fields]
                log(
                    f"Business Metadata Attribute List is: {business_metadata_attributes}.",
                    source_data=source_data,
                    target_data=target_data,
                    level=logging.DEBUG,
                )
            except Exception as e:
                log(
                    f"Error while Getting BusinessMetadata Arributes List, {str(e)} and {traceback.format_exc()}",
                    source_data=source_data,
                    target_data=target_data,
                    level=logging.ERROR,
                )
                raise e

            # Checking each column header in excel file.
            try:
                log(
                    f"Columns from Excel/CSV File: {df.columns}",
                    source_data=source_data,
                    target_data=target_data,
                    level=logging.DEBUG,
                )

                for column in df.columns:
                    cleaned_column = (
                        column.strip()
                    )  # Removed trailing and leading spaces from excel file column headers.

                    log(
                        f"Column Name After Removing White Spaces: '{cleaned_column}'.",
                        source_data=source_data,
                        target_data=target_data,
                        level=logging.DEBUG,
                    )
                    # If the column name in excel/csv file present in business metatable table attribut/column.
                    if cleaned_column in business_metadata_attributes:
                        # Getting value for column in excel/csv file.
                        attribute_value = df[column].iloc[0]

                        # If value of column in excel/csv file is not empty.
                        if not pd.isnull(attribute_value):
                            log(
                                f"Column Value is not null in Excel/CSV File and Value is: '{attribute_value}'.",
                                source_data=source_data,
                                target_data=target_data,
                                level=logging.DEBUG,
                            )

                            try:
                                # Get the field object for the cleaned_column
                                field_obj = BusinessMetadata._meta.get_field(
                                    cleaned_column
                                )

                                # Validate data type
                                expected_data_type = field_obj.get_internal_type()
                                log(
                                    f"Expected Data Type is: {expected_data_type}.",
                                    source_data=source_data,
                                    target_data=target_data,
                                    level=logging.DEBUG,
                                )
                                if expected_data_type == "BigIntegerField":
                                    try:
                                        attribute_value = int(attribute_value)
                                    except ValueError:
                                        log(
                                            f"Error: Value for '{cleaned_column}' must be a valid integer."
                                            f" Skipping update.",
                                            source_data=source_data,
                                            target_data=target_data,
                                            level=logging.ERROR,
                                        )
                                        continue

                                elif expected_data_type == "FloatField":
                                    try:
                                        # Try converting the attribute value to a float
                                        attribute_value = float(attribute_value)
                                    except ValueError:
                                        log(
                                            f"Error: Value for '{cleaned_column}' must be a float or an integer."
                                            f" Skipping Update.",
                                            source_data=source_data,
                                            target_data=target_data,
                                            level=logging.ERROR,
                                        )
                                        continue

                                elif (
                                    expected_data_type == "CharField"
                                    or expected_data_type == "TextField"
                                ):
                                    if not isinstance(attribute_value, str):
                                        if isinstance(attribute_value, pd.Timestamp):
                                            # Convert timestamp to string and update
                                            attribute_value = str(attribute_value)
                                        else:
                                            log(
                                                f"Error: Value for '{cleaned_column}' must be a string or a timestamp."
                                                f" Skipping update.",
                                                source_data=source_data,
                                                target_data=target_data,
                                                level=logging.ERROR,
                                            )
                                            continue

                                elif expected_data_type == "DateTimeField":
                                    if isinstance(attribute_value, datetime):
                                        # Attribute value is already a datetime object, no need to parse
                                        pass
                                    elif isinstance(attribute_value, str):
                                        try:
                                            attribute_value = parse_datetime(
                                                attribute_value
                                            )
                                        except ValueError:
                                            log(
                                                f"Error: Value for '{cleaned_column}' must be a valid datetime string."
                                                f" Skipping Update.",
                                                source_data=source_data,
                                                target_data=target_data,
                                                level=logging.ERROR,
                                            )
                                            continue
                                    else:
                                        log(
                                            f"Error: Value for '{cleaned_column}' must be a Valid Datetime."
                                            f" Skipping Update.",
                                            source_data=source_data,
                                            target_data=target_data,
                                            level=logging.ERROR,
                                        )
                                        continue

                                setattr(
                                    business_metadata, cleaned_column, attribute_value
                                )
                                log(
                                    f"Updating Value of '{cleaned_column}' and Value is: '{attribute_value}'.",
                                    source_data=source_data,
                                    target_data=target_data,
                                    level=logging.DEBUG,
                                )
                            except Exception as e:
                                log(
                                    f"Oops!! Error occurred while updating metadata for '{cleaned_column}': {str(e)} and {traceback.format_exc()}",
                                    source_data=source_data,
                                    target_data=target_data,
                                    level=logging.ERROR,
                                )

                            try:
                                business_metadata.save()
                                log(
                                    f"'{cleaned_column}' Updated Successfully in Business Metadata Table.",
                                    source_data=source_data,
                                    target_data=target_data,
                                    level=logging.DEBUG,
                                )
                            except Exception as e:
                                log(
                                    f"Oops!! Error occurred while saving metadata for '{cleaned_column}': {str(e)} and {traceback.format_exc()}",
                                    source_data=source_data,
                                    target_data=target_data,
                                    level=logging.ERROR,
                                )
                                continue

                        else:
                            # Log a message if the column in Excel/CSV file has a null value
                            log(
                                f"Warning: Null value found for column '{cleaned_column}'"
                                f" in Excel/CSV file. Skipping Update for this Column.",
                                source_data=source_data,
                                target_data=target_data,
                                level=logging.WARNING,
                            )
                    else:
                        # Log an error if the column in Excel/CSV file doesn't match
                        # any attribute in BusinessMetadata table.
                        log(
                            f"Error: Column '{cleaned_column}' in Excel/CSV file does not match any Attribute"
                            f" in BusinessMetadata table hence we Cannot Update Metadata for '{cleaned_column}'"
                            f" Column.",
                            source_data=source_data,
                            target_data=target_data,
                            level=logging.ERROR,
                        )
            except IndexError as index_error:
                # Handle the "single positional indexer is out-of-bounds" error
                log(
                    "Oops!! There are no values in Excel File to update.",
                    source_data=source_data,
                    target_data=target_data,
                    level=logging.WARNING,
                )
            except Exception as e:
                # If something other exception occurs during updating it is handled here.
                log(
                    f"Oops!! Error has Occured while Updating Metadata from Excel File: {str(e)} and {traceback.format_exc()}",
                    source_data=source_data,
                    target_data=target_data,
                    level=logging.ERROR,
                )

        else:
            # log warning message if file does not exists at perticular location.
            log(
                "Oops!! Excel File might not be Exist.",
                source_data=source_data,
                target_data=target_data,
                level=logging.WARNING,
            )

        log(
            "Starting Copying Process of a File to Destination Folder.",
            source_data=source_data,
            target_data=target_data,
            level=logging.INFO,
        )
        # Obtaining Source File Path from Source Data Table.
        src_path = source_data.SourceDatasetPath
        log(
            "Obtaining Source File Path from Source Data Table.",
            source_data=source_data,
            target_data=target_data,
            level=logging.DEBUG,
        )
        try:
            # Getting Destination Path from Target Data Table.
            target_data = TargetData.objects.get(SourceData=source_data)
            dest_path = target_data.TargetDatasetPath
            local_dest_path = dest_path.replace(
                settings.SERVER_PATH, settings.LOCAL_INGESTED_FOLDER
            )
            log(
                f"Local Destination Path is: {local_dest_path}",
                source_data=source_data,
                target_data=target_data,
                level=logging.DEBUG,
            )
            target_data.LocalTargetDatasetPath = local_dest_path
            target_data.save()

            log(
                "Local Destination Path Saved in Target Data.",
                source_data=source_data,
                target_data=target_data,
                level=logging.DEBUG,
            )

        except Exception as e:
            log(
                f"Error while Getting Destination Path from Target Data: {str(e)} and {traceback.format_exc()}",
                source_data=source_data,
                target_data=target_data,
                level=logging.ERROR,
            )
            IADSCoreStatus = WorkerName + "_" + Failed
            source_data.IADSCoreStatus = IADSCoreStatus
            source_data.IADSIngestionStatus = "Ingestion_Failed"
            source_data.target_data.IADSIngestionStatus = "Ingestion_Failed"
            source_data.target_data.save()
            source_data.save()
            log(
                "Error. Failed to Get Destination Path from Target Data.",
                source_data=source_data,
                target_data=target_data,
                level=logging.DEBUG,
            )
            log(
                "IngetserWorker Failed & Destination Path might not be Get from Target Data.",
                source_data=source_data,
                target_data=target_data,
                level=logging.ERROR,
            )
            raise e
        try:
            # used copy2 method to copy file from source to destination folder.
            shutil.copy2(src_path, local_dest_path)
            log(
                f"Good Job!! File Copied to Destination Folder & Destination Path is: {local_dest_path}",
                source_data=source_data,
                target_data=target_data,
                destination_path=dest_path,
                level=logging.INFO,
            )

            # saved source data and target data instances.
            target_data.save()
            source_data.save()

        except Exception as e:
            log(
                "Oops!! Error has Occured while copying file to Destination Folder.",
                source_data=source_data,
                target_data=target_data,
                level=logging.DEBUG,
            )
            log(
                f"Error while copying file to Destination Folder: {str(e)} and {traceback.format_exc()}",
                source_data=source_data,
                target_data=target_data,
                level=logging.ERROR,
            )
            IADSCoreStatus = WorkerName + "_" + Failed
            source_data.IADSCoreStatus = IADSCoreStatus
            source_data.IADSIngestionStatus = "Ingestion_Failed"
            source_data.save()
            log(
                "IngetserWorker Failed to Copy File at Destination Folder.",
                source_data=source_data,
                target_data=target_data,
                level=logging.ERROR,
            )
            raise e
        target_data.save()
        source_data.save()

        # Extracting Data if needed
        if source_data.IsZippedFile:
            # Extract according to preexisting logic because they are sentinel or landsat tile
            if source_data.SatelliteImageTile:
                sat_image_service = SatelliteImageTileService()
                extracted_path = sat_image_service.extract(
                    source_data.SatelliteImageTile, source_data
                )
                source_data.ExtractedPath = extracted_path
                source_data.save()
                log(
                    "Data extracted Sucessfully from Zip File.",
                    source_data=source_data,
                    target_data=target_data,
                    level=logging.INFO,
                )

                target_images = sat_image_service.fetch_target_images(
                    source_data.SatelliteImageTile, source_data
                )

                target_data.TargetImages.all().delete()

                for i, target_image in enumerate(target_images):
                    if not TargetImage.objects.filter(
                        Path=target_image["Path"]
                    ).exists():
                        TargetImage.objects.create(
                            TargetData=target_data,
                            LocalTiffPath=target_image["Path"],
                            BandName=target_image["BandName"],
                        )

                    # This is hot fix, fix this later
                    if (
                        i == 0
                        and source_data.SatelliteImageTile.Product == PRODUCT_SENTINEL2
                    ):
                        try:
                            log(
                                "This is a sentinel 2 tile, hence finding EPSG code now.",
                                source_data=source_data,
                                target_data=target_data,
                                level=logging.INFO,
                            )
                            business_metadata.EpsgCode = (
                                sat_image_service.find_epsg_for_sentinel(
                                    source_data.SatelliteImageTile,
                                    target_image_path=target_image["Path"],
                                )
                            )
                            business_metadata.save()
                            log(
                                f"EPSG Code saved: {business_metadata.EpsgCode}",
                                source_data=source_data,
                                target_data=target_data,
                                level=logging.INFO,
                            )
                        except Exception as e:
                            log(
                                f"Error finding epsg code: {e} and {traceback.format_exc()}",
                                source_data=source_data,
                                target_data=target_data,
                                level=logging.ERROR,
                            )

            # target_data.TargetDatasetPath
            # TODO: Or extract them without logic if they are general compressed files
            else:
                extracted_path = ""
        else:
            if not target_data.TargetImages.exists():
                local_tiff_path = source_data.target_data.LocalTargetDatasetPath
                log(
                    f"Local Tiff Path is: {local_tiff_path}",
                    source_data=source_data,
                    target_data=target_data,
                    level=logging.DEBUG,
                )
                TargetImage.objects.create(
                    TargetData=target_data,
                    LocalTiffPath=local_tiff_path,
                    BandName="Single_Band_Image",
                )
                log(
                    "Target Image object for single band image created successfully.",
                    source_data=source_data,
                    target_data=target_data,
                    level=logging.DEBUG,
                )

        IADSCoreStatus = WorkerName + "_" + Completed
        source_data.IADSCoreStatus = IADSCoreStatus
        source_data.save()

        log(
            "LocalIngesterWorker Completed Successfully.",
            source_data=source_data,
            target_data=target_data,
            level=logging.INFO,
        )
        return metadata

    def start(self, source_data: SourceData):
        log(
            "Checking if the Item is Eligible to Start LocalIngesterWorker.",
            source_data=source_data,
            level=logging.DEBUG,
        )
        # Calling eligible_item_reader to find is item is eligible to start or not.
        is_eligible = self.eligible_item_reader(source_data)
        if is_eligible:
            log(
                "Now Starting LocalIngesterWorker.",
                source_data=source_data,
                level=logging.INFO,
            )
            # Calling process_started_worker method.
            self.process_started_worker(source_data)
