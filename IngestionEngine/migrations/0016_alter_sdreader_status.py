# Generated by Django 4.2.4 on 2024-02-19 12:12

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("IngestionEngine", "0015_targetdata_targetdataid_targetimage_imageid"),
    ]

    operations = [
        migrations.AlterField(
            model_name="sdreader",
            name="Status",
            field=models.CharField(
                blank=True,
                choices=[("Active", "Active"), ("Inactive", "Inactive")],
                max_length=10,
                null=True,
            ),
        ),
    ]
