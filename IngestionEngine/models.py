import uuid
from django.db import models

from django.contrib.gis.db import models


# from django.contrib.postgres.fields import ArrayField


class BaseModel(models.Model):
    CreatedOn = models.DateTimeField(auto_now_add=True)
    CreatedBy = models.CharField(max_length=50, blank=True, null=True, default="NA")
    ModifiedOn = models.DateTimeField(auto_now=True)
    ModifiedBy = models.CharField(max_length=50, blank=True, null=True, default="NA")

    class Meta:
        abstract = True


class SourceData(BaseModel):
    """Source Data Information"""

    SourceDataID = models.AutoField(
        auto_created=True,
        primary_key=True,
        verbose_name="SourceDataID",
    )
    SourceDatasetName = models.TextField(null=True, blank=True)
    SourceDatasetPath = models.TextField(null=True, blank=True)
    SourceDatasetType = models.TextField(null=True, blank=True)
    SourceDatasetSize = models.BigIntegerField(null=True, blank=True)
    SourceDatasetVersion = models.TextField(null=True, blank=True)
    SourceDataLocation = models.TextField(null=True, blank=True)
    DataProvider = models.TextField(null=True, blank=True)
    DataSchema = models.TextField(null=True, blank=True)
    DataVolume = models.TextField(null=True, blank=True)
    DataFrequency = models.TextField(null=True, blank=True)
    DataExtractionDateTime = models.DateTimeField(blank=True, null=True)
    DataOwnership = models.TextField(null=True, blank=True)
    DataLicensing = models.TextField(null=True, blank=True)
    IngestionType = models.CharField(
        max_length=50,
        blank=True,
        null=True,
        choices=[("manual", "Manual"), ("automated", "Automated")],
    )
    IADSIngestionStatus = models.CharField(
        max_length=50, blank=True, null=True, default="NA"
    )
    CTPIIngestionStatus = models.CharField(
        max_length=50, blank=True, null=True, default="NA"
    )

    IADSCoreStatus = models.CharField(max_length=50, blank=True, null=True)
    IsExcelPresent = models.BooleanField(default=False)
    MnAID = models.TextField(null=True, blank=True)

    IsZippedFile = models.BooleanField()
    ExtractedPath = models.TextField(blank=True, null=True)
    CTPIIngestedDate = models.DateTimeField(null=True, blank=True)
    Is_deleted_from_hot_folder = models.BooleanField(default=False)

    def __str__(self) -> str:
        return self.SourceDatasetName

    class Meta:
        db_table = "SourceData"


class TargetData(BaseModel):

    """Target Data Information"""

    ID = models.AutoField(
        auto_created=True, primary_key=True, verbose_name="TargetDataID"
    )
    TargetDataID = models.UUIDField(default=uuid.uuid4, unique=True)
    MnAID = models.TextField(null=True, blank=True)
    SourceData = models.OneToOneField(
        SourceData, on_delete=models.CASCADE, related_name="target_data"
    )

    TargetDatasetName = models.TextField(null=True, blank=True)
    TargetDatasetPath = models.TextField(null=True, blank=True)
    TargetDataLocation = models.TextField(null=True, blank=True)
    LocalTargetDatasetPath = models.TextField(null=True, blank=True)
    DataSchema = models.TextField(null=True, blank=True)
    DataVolume = models.TextField(null=True, blank=True)
    DataStorageFormat = models.TextField(null=True, blank=True)
    DataRetentionPolicy = models.TextField(null=True, blank=True)
    DataQualityProfile = models.TextField(null=True, blank=True)
    DataSecurityMeasures = models.TextField(null=True, blank=True)

    IADSIngestionStatus = models.CharField(max_length=50, blank=True, null=True)
    CTPIIngestionStatus = models.CharField(
        max_length=50, blank=True, null=True, default="NA"
    )

    def __str__(self) -> str:
        return str(self.TargetDatasetPath)

    class Meta:
        db_table = "TargetData"


class TargetImage(BaseModel):
    """Band information for individual images"""

    ID = models.AutoField(primary_key=True)
    ImageID = models.UUIDField(default=uuid.uuid4, unique=True)
    TargetData = models.ForeignKey(
        TargetData, on_delete=models.CASCADE, related_name="TargetImages"
    )
    SpatialResolution = models.CharField(max_length=50, blank=True, null=True)

    Path = models.TextField(blank=True, null=True)
    COGPath = models.TextField(blank=True, null=True)
    DownloadURL = models.TextField(blank=True, null=True)
    VisualizationURL = models.TextField(blank=True, null=True)
    BandName = models.TextField(blank=True, null=True)
    IsCOGGenerated = models.BooleanField(default=False)
    SalePrice = models.FloatField(blank=True, null=True)
    LocalCOGPath = models.TextField(blank=True, null=True)
    LocalTiffPath = models.TextField(blank=True, null=True)
    is_tiff_copied_on_nas = models.BooleanField(default=False)
    is_cog_copied_on_nas = models.BooleanField(default=False)
    is_deleted_from_local = models.BooleanField(default=False)

    class Meta:
        db_table = "TargetImage"


class BusinessMetadata(BaseModel):
    BusinessMetadataID = models.AutoField(
        auto_created=True,
        primary_key=True,
        verbose_name="BusinessMetadataID",
    )
    TargetData = models.OneToOneField(
        TargetData, on_delete=models.CASCADE, related_name="BusinessMetadata"
    )

    Extent = models.PolygonField(null=True, blank=True)
    # extent = models.PolygonField(null=True, blank=True)
    # Keywords = ArrayField(models.CharField(max_length=50, null=True,blank=True))
    EpsgCode = models.CharField(max_length=14, null=True, blank=True)
    NumPixels = models.BigIntegerField(null=True, blank=True)
    NumLayers = models.BigIntegerField(null=True, blank=True)
    HorizontalResolution = models.FloatField(null=True, blank=True)
    Keywords = models.TextField(null=True, blank=True)
    Type = models.TextField(blank=True, null=True)
    Thesaurus = models.TextField(blank=True, null=True)
    ImagePlatforms = models.TextField(blank=True, null=True)
    CRSCoordinateReference = models.TextField(blank=True, null=True)
    HorizontalDatum = models.TextField(blank=True, null=True)
    VerticalDatum = models.TextField(blank=True, null=True)
    SensorType = models.TextField(blank=True, null=True)
    AcquisitionDate = models.DateTimeField(blank=True, null=True)
    SourceName = models.TextField(blank=True, null=True)
    ProductType = models.TextField(blank=True, null=True)
    ProcessingLevel = models.TextField(blank=True, null=True)
    Custodian = models.TextField(blank=True, null=True)
    LicensingLevel = models.TextField(blank=True, null=True)
    Date = models.DateTimeField(blank=True, null=True)
    TileID = models.TextField(blank=True, null=True)
    DownloadURL = models.TextField(blank=True, null=True)
    VisualizationURL = models.TextField(blank=True, null=True)
    Abstract = models.TextField(blank=True, null=True)
    AccessConstraints = models.TextField(blank=True, null=True)
    AlternateTitle = models.TextField(blank=True, null=True)
    Capabilities = models.TextField(blank=True, null=True)
    CloudCover = models.TextField(blank=True, null=True)
    ContactPersonName = models.TextField(blank=True, null=True)
    DataThemes = models.TextField(blank=True, null=True)
    DataSets = models.TextField(blank=True, null=True)
    PlatformDescription = models.TextField(blank=True, null=True)
    DistributionFormat = models.TextField(blank=True, null=True)
    DistributionLimitations = models.TextField(blank=True, null=True)
    Distributor = models.TextField(blank=True, null=True)
    EmailAddress = models.TextField(blank=True, null=True)
    ImageAcquisitionDateRange = models.TextField(blank=True, null=True)
    ImageFormat = models.TextField(blank=True, null=True)
    ImageType = models.TextField(blank=True, null=True)
    Industries = models.TextField(blank=True, null=True)
    IndustryUsage = models.TextField(blank=True, null=True)
    LicenseBasedConstraints = models.TextField(blank=True, null=True)
    OrganizationName = models.TextField(blank=True, null=True)
    OtherConstraints = models.TextField(blank=True, null=True)
    PlatformIdentifier = models.TextField(blank=True, null=True)
    Resolution = models.TextField(blank=True, null=True)
    ResolutionRange = models.TextField(blank=True, null=True)
    SensorMode = models.TextField(blank=True, null=True)
    SourceName = models.TextField(blank=True, null=True)
    SpatialResolution = models.TextField(blank=True, null=True)
    SpecializedConstraints = models.TextField(blank=True, null=True)
    Title = models.TextField(blank=True, null=True)
    TransferSizeUnit = models.TextField(blank=True, null=True)
    UseConstraints = models.TextField(blank=True, null=True)
    SpectralBands = models.TextField(blank=True, null=True)
    LayerName = models.TextField(blank=True, null=True)
    Description = models.TextField(blank=True, null=True)
    LayerDisplayName = models.TextField(blank=True, null=True)
    Identifier = models.TextField(blank=True, null=True)
    Scale = models.BigIntegerField(blank=True, null=True)
    RasterDataType = models.TextField(blank=True, null=True)
    Sensors = models.TextField(blank=True, null=True)
    ContactNumber = models.BigIntegerField(blank=True, null=True)
    PlatformName = models.TextField(blank=True, null=True)
    ProviderOrganizationName = models.TextField(blank=True, null=True)
    DataLicensing = models.TextField(null=True, blank=True)
    PlatformType = models.TextField(null=True, blank=True)
    IsEditable = models.BooleanField(default=True)

    def __str__(self) -> str:
        return str(self.BusinessMetadataID)

    class Meta:
        db_table = "BusinessMetadata"


class OperationalMetadata(BaseModel):
    BusinessMetadataID = models.IntegerField(primary_key=True)
    TargetDataID = models.ForeignKey(TargetData, on_delete=models.CASCADE)

    class Meta:
        db_table = "OperationalMetadata"


class IngestionConfiguration(BaseModel):
    """Ingestion Configuration"""

    ConfigurationID = models.IntegerField(primary_key=True)
    ParameterName = models.TextField(blank=True, null=True)
    ParameterType = models.TextField(blank=True, null=True)
    ParameterValue = models.TextField(blank=True, null=True)
    ParameterDescription = models.TextField(blank=True, null=True)
    ParameterCategory = models.TextField(blank=True, null=True)
    AffectedSourceID = models.ForeignKey(
        SourceData, on_delete=models.CASCADE, blank=True, null=True
    )
    AffectedTargetID = models.ForeignKey(
        TargetData, on_delete=models.CASCADE, blank=True, null=True
    )
    DataType = models.TextField(blank=True, null=True)

    class Meta:
        db_table = "IngestionConfiguration"


class IngestionLogs(BaseModel):
    """Ingestion Logs and Metrics"""

    # LogID = models.IntegerField(primary_key=True)
    LogID = models.AutoField(
        auto_created=True, primary_key=True, serialize=False, verbose_name="LogID"
    )
    Timestamp = models.DateTimeField(blank=True, null=True)
    LogType = models.CharField(max_length=10, blank=True, null=True)
    LogMessage = models.TextField(null=True, blank=True)
    WorkerName = models.CharField(max_length=50, blank=True, null=True)
    HostID = models.TextField(null=True, blank=True)

    AffectedSourceID = models.ForeignKey(
        SourceData, on_delete=models.CASCADE, blank=True, null=True
    )
    AffectedTargetID = models.ForeignKey(
        TargetData, on_delete=models.CASCADE, blank=True, null=True
    )
    ResourceUtilization = models.TextField(blank=True, null=True)
    DataVolume = models.TextField(blank=True, null=True)
    DataThroughput = models.TextField(blank=True, null=True)
    AffectedClipID = models.ForeignKey(
        "SatProductCurator.NewClipRequest",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    AffectedCollectionID = models.ForeignKey(
        "SatProductCurator.NewCollectionRequest",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    AffectedSatTileID = models.ForeignKey(
        "SatProductCurator.SatelliteImageTile",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    AffectedSatID = models.ForeignKey(
        "SatProductCurator.SatelliteUsageDetails",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )

    class Meta:
        db_table = "IngestionLogs"


class MnAOperationInfo(BaseModel):
    """MnA Operational Information
    For Saving the Clipped Request
    """

    MnAOpID = models.AutoField(auto_created=True, primary_key=True)
    TargetDataID = models.ForeignKey(TargetData, on_delete=models.CASCADE)
    TargetDatasetName = models.TextField(blank=True, null=True)
    TargetDataLocation = models.TextField(blank=True, null=True)
    DataSchema = models.TextField(blank=True, null=True)
    DataVolume = models.TextField(blank=True, null=True)
    DataStorageFormat = models.TextField(blank=True, null=True)
    MnAID = models.TextField(blank=True, null=True)

    class Meta:
        db_table = "MnAOperationInfo"


class SDReader(BaseModel):
    STATUS_CHOICES = [
        ("Active", "Active"),
        ("Inactive", "Inactive"),
    ]
    FolderName = models.TextField(blank=True, null=True)
    FolderPath = models.TextField(blank=True, null=True)
    Status = models.CharField(
        max_length=10, choices=STATUS_CHOICES, blank=True, null=True
    )
    TotalCapacity = models.BigIntegerField(blank=True, null=True)
    FreeCapacity = models.BigIntegerField(blank=True, null=True)
    ReservedCapacity = models.BigIntegerField(blank=True, null=True)
    UsedCapacity = models.BigIntegerField(blank=True, null=True)
    LastUpdated = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = "SDReader"
