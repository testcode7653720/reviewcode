# middleware.py

from django.http import JsonResponse


class Custom404Middleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)

        # Check if the response status is 404 and the content type is 'text/html'
        if (
            response.status_code == 404
            and response.get("Content-Type") == "text/html; charset=utf-8"
        ):
            # Handle 404 error
            return JsonResponse(
                {
                    "error": "Endpoint not found.",
                    "message": "Please enter a valid URL.",
                },
                status=404,
            )

        return response
