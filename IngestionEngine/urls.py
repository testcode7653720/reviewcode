from django.urls import path, include
from rest_framework import routers

from .views import BusinessMetadataViewSet, SourceDataViewSet
from IngestionEngine import views


router = routers.SimpleRouter()
router.register(r"source_data", SourceDataViewSet)
router.register(r"metadata", BusinessMetadataViewSet)
urlpatterns = [
    path("", include(router.urls)),
    path(
        "ingest_source_data/<int:source_data_id>/",
        views.ingest_source_data_view,
        name="ingest_source_data_view",
    ),
    path("api/cog-image-paths/", views.get_cog_image_paths, name="cog-image-paths"),
]
