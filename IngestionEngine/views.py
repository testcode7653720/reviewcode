import logging
from rest_framework import viewsets
from rest_framework.decorators import action
import os
from IngestionEngine.models import (
    SourceData,
    TargetData,
    BusinessMetadata,
)
from IngestionEngine.serializers import SourceDataSerializer, TargetDataSerializer
from IngestionEngine.serializers.metadata_serializer import BusinessMetadataSerializer
from IngestionEngine.services import TargetDataService
from .tasks import ingest_source_data_async
from django.http import HttpResponse, JsonResponse
from django.conf import settings


logger = logging.getLogger("ingestion-engine")


class SourceDataViewSet(viewsets.ModelViewSet):
    serializer_class = SourceDataSerializer
    queryset = SourceData.objects.all()

    def list(self, request, *args, **kwargs):
        """View all the files."""
        return super().list(request, *args, **kwargs)


class BusinessMetadataViewSet(viewsets.ModelViewSet):
    serializer_class = BusinessMetadataSerializer
    queryset = BusinessMetadata.objects.all()

    def list(self, request, *args, **kwargs):
        """View all the files."""
        return super().list(request, *args, **kwargs)


class TargetDataViewSet(viewsets.ModelViewSet):
    serializer_class = TargetDataSerializer
    queryset = TargetData.objects.all()

    def list(self, request, *args, **kwargs):
        """View all the ingested files."""
        return super().list(request, *args, **kwargs)

    @action(["GET"], detail=False, url_path="search")
    def search(self, request, query):
        """Search though all the ingested files"""
        service = TargetDataService()
        service.search(query)
        pass

    @action(["GET"], detail=False, url_path="deep-search")
    def deep_search(self, request, query):
        """This also searches wether the resource is available or not"""
        service = TargetDataService()
        service.search(query)


def ingest_source_data_view(request, source_data_id):
    # Call the asynchronous task using delay() method
    try:
        print("task Asynchronized")
        source_data = SourceData.objects.get(id=source_data_id)
        task_result = ingest_source_data_async.delay(source_data.id)
        task_status = task_result.status
        return JsonResponse({"task_id": task_result.id, "task_status": task_status})

    except SourceData.DoesNotExist:
        return HttpResponse("source_data not found", status=404)


def get_cog_image_paths(request):
    folder_path = os.path.join(settings.BASE_DIR, "Ingested_folder")
    filenames = os.listdir(folder_path)

    if settings.IS_STAGING:
        base_url = "http://89.233.104.4/resources/"
    else:
        base_url = "http://127.0.0.1:5500/Ingested_folder/"

    paths = [os.path.join(base_url, filename) for filename in filenames]

    return JsonResponse({"paths": paths})
