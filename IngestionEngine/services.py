import logging
import time
from django.utils import timezone
import os
import rasterio
from rio_cogeo.cogeo import cog_translate
from rio_cogeo.profiles import cog_profiles
import shutil
import json
import rasterio
import urllib.request
import pystac
from datetime import datetime
from django.conf import settings
from datetime import datetime
from shapely.geometry import Polygon, mapping
from tempfile import TemporaryDirectory

from .models import SourceData, TargetData

logger = logging.getLogger('ingestion-engine')


class TargetDataService():

    def search(self, query):
        """ Search though all the ingested files """
        pass

    def deep_search(self, query) -> list[TargetData]:
        """This also searches wether the resource is available or not"""
        pass

    def search_by_point(self, point: str) -> list[TargetData]:
        """Search resource by type"""

    def search_by_polygon(self, polygon: str) -> list[TargetData]:
        """Search resource by type"""


class SourceDataService():

    def copy_file(self, src_path: str, dest_path: str):
        logger.info(f"Copying file from {src_path} to {dest_path}")
        shutil.copy(src_path, dest_path)
        logger.info(f"File copied to {dest_path}")
        time.sleep(2)
        return True

    def fetch_metadata(self, source_data: SourceData):
        logger.info(f"Fetching metadata")
        with rasterio.open(source_data.path) as dataset:
            epsg_code = None
            if dataset.crs:
                epsg_code = dataset.crs.to_string()

            num_pixels = dataset.width
            num_layers = dataset.height

            horizontal_resolution = dataset.res[0]
            extent = {
                "xmin": dataset.bounds.left,
                "xmax": dataset.bounds.right,
                "ymin": dataset.bounds.bottom,
                "ymax": dataset.bounds.top,
            }
            metadata = {
                "epsg_code": epsg_code,
                "num_pixels": num_pixels,
                "num_layers": num_layers,
                "horizontal_resolution": horizontal_resolution,
                "extent": extent,
            }

        return metadata

    @staticmethod
    def get_bbox_and_footprint(raster):
        with rasterio.open(raster) as r:
            bounds = r.bounds
            bbox = [bounds.left, bounds.bottom, bounds.right, bounds.top]
            footprint = Polygon([
                [bounds.left, bounds.bottom],
                [bounds.left, bounds.top],
                [bounds.right, bounds.top],
                [bounds.right, bounds.bottom]
            ])

        return (bbox, mapping(footprint))

    def add_to_catalog(self, img_path):
        print(type(img_path), img_path)
        catalog = pystac.Catalog.from_file('./stac-catalog/catalog.json')
        bbox, footprint = self.get_bbox_and_footprint(img_path)
        image_name = os.path.split(img_path)[-1]
        item = pystac.Item(id=f'{image_name}-item',
                           geometry=footprint,
                           bbox=bbox,
                           datetime=datetime.now(),
                           properties={})
        catalog.add_item(item)
        item.add_asset(
            key='image',
            asset=pystac.Asset(
                href=img_path,
                media_type=pystac.MediaType.GEOTIFF
            )
        )

        catalog.normalize_hrefs("./stac-catalog")
        catalog.save(catalog_type=pystac.CatalogType.SELF_CONTAINED)
        return True

    def get_server_path(self, source_data: SourceData):
        file_name = source_data.path
        extension = file_name.split(os.path.sep)[-1].split(".")[-1]
        current_datetime = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        file_name_without_extension = os.path.splitext(
            os.path.basename(file_name))[0]

        destination_path = os.path.join(
            settings.INGESTED_FOLDER, f"{file_name_without_extension}_{current_datetime}.{extension}")

        return destination_path

    def get_cog_path(self, source_data: SourceData):
        input_tiff_path = source_data.target_data.path
        if os.path.exists(input_tiff_path) and not input_tiff_path.endswith("_cog.tif"):
            cog_path = os.path.splitext(input_tiff_path)[0] + "_cog.tif"
            return cog_path
        return None

    def convert_source_data_to_cog(self, input_tiff_path, cog_path):
        if not os.path.exists(cog_path):
            with rasterio.open(input_tiff_path) as src_ds:
                cog_profile = cog_profiles.get("deflate")
                cog_translate(src_ds, cog_path, cog_profile)
                logger.info(f"File converted to cog at {cog_path}")
            return True

    def ingest(self, source_data: SourceData):

        ALLOWED_EXTENSIONS = {'img', 'tif', 'tiff', 'jp2'}
        if source_data.IsReadyForIngestion:
            if source_data.IsIngestionCompleted:
                return False, 'Ingestion is already complete.'
            file_path = source_data.path
            file_extension = file_path.split(
                os.path.sep)[-1].split(".")[-1] if '.' in file_path else ''

            if file_extension not in ALLOWED_EXTENSIONS:
                return False, 'Unsupported Image format to ingest.'

            logger.info(f"Starting ingestion of {source_data.path}")
            time.sleep(2)
            try:
                target_data = TargetData.objects.get(
                    parent_file=source_data)
            except TargetData.DoesNotExist:
                target_data = TargetData()
                target_data.parent_file = source_data

            # source_data.is_ingestion_started = True
            # source_data.ingestion_started_at = timezone.now()
            # source_data.ingestion_start_attempt += 1

            target_data.save()
            source_data.save()

            logger.info(f"Fetching metadata {source_data.path}")
            if not source_data.is_metadata_read:
                metadata = self.fetch_metadata(source_data)

            if not target_data.business_metadata:
                target_data.business_metadata = Metadata.objects.create(
                    **metadata)
                target_data.save()
            source_data.is_metadata_read = True
            source_data.save()

            logger.info(f"Fetching destination path {source_data.path}")
            destination_path = self.get_server_path(source_data)
            logger.info(f"destination path {destination_path}")
            target_data.path = destination_path
            target_data.save()
            source_data.is_destination_path_created = True
            source_data.save()

            logger.info(f"Copying file {source_data.path}")
            file_copied = self.copy_file(
                src_path=source_data.path, dest_path=target_data.path)

            if file_copied:
                target_data.is_copied = True
                target_data.copied_at = timezone.now()
                logger.info(f"File Copied to {destination_path}")
                source_data.save()
            else:
                logger.error(f"Error copying file {source_data.path}")

            logger.info(f"Getting cog_path to convert image into cog")

            cog_path = self.get_cog_path(source_data)
            source_data.is_cog_path_created = True
            source_data.save()
            target_data.cog_path = cog_path
            target_data.save()
            logger.info(f"Converting file into cog and saved into cog_path")

            input_tiff_path = source_data.path
            converted_to_cog = self.convert_source_data_to_cog(
                input_tiff_path, cog_path)
            if converted_to_cog:
                source_data.is_converted_to_cog = True
                source_data.save()
            time.sleep(5)

            logger.info(f"add file to STAC Catalog")

            added_to_catalog = self.add_to_catalog(cog_path)
            if added_to_catalog:
                source_data.is_added_to_stac = True
                source_data.is_ingestion_completed = True
                source_data.ingestion_completed_at = timezone.now()
                source_data.save()
            logger.info(f"added file to STAC Catalog")
            time.sleep(2)

            return True, 'Successfully Ingested'

    def cleanup(self):
        """Delete files"""

    def are_same_files(self, source_data_1: SourceData, source_data_2: SourceData) -> bool:
        """Checks the hash of two files if they are same.

        Args:
            source_data_1 (SourceData)
            source_data_2 (SourceData)

        Returns:
            bool
        """
        import hashlib

        hasher1 = hashlib.md5()
        with source_data_1.path.open('rb') as f1:
            hasher1.update(f1.read())
            a = hasher1.hexdigest()

        hasher2 = hashlib.md5()
        with source_data_2.path.open('rb') as f2:
            hasher2.update(f2.read())
            b = hasher2.hexdigest()

        return str(a) == str(b)
