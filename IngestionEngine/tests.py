import unittest
from unittest.mock import MagicMock, patch
from .models import SourceData
from IngestionEngine.workers.pre_ingestion_worker import PreIngestionWorker


class TestPreIngestionWorker(unittest.TestCase):
    @patch("IngestionEngine.workers.pre_ingestion_worker.SourceData.objects.filter")
    def setUp(self, mock_filter):
        self.worker = PreIngestionWorker()
        self.source_data_instance = MagicMock()

        # Mock the filter method to return the source data instance
        mock_filter.return_value = [self.source_data_instance]

    def test_find_eligible_items(self):
        # Mock the SourceData queryset and its filter method
        self.source_data_instance.filter.return_value = [self.source_data_instance]
        SourceData.objects.filter.return_value = [self.source_data_instance]

        result = self.worker.find_eligible_items()
        self.assertEqual(result, [self.source_data_instance])

    def test_eligible_item_reader(self):
        # Mocking source_data attributes
        self.source_data_instance.IADSCoreStatus = "FileWatcherWorker_Completed"

        result = self.worker.eligible_item_reader(self.source_data_instance)
        self.assertTrue(result)

    def test_process_started_worker(self):
        # Mocking source_data attributes
        self.source_data_instance.IADSCoreStatus = "FileWatcherWorker_Completed"
        self.source_data_instance.SourceDatasetPath = "/path/to/your/image.tif"

        # Mocking the logger
        with patch("IngestionEngine.log") as mock_logger:
            self.worker.process_started_worker(self.source_data_instance)

            # Check if the logger method was called with expected parameters
            mock_logger.assert_called_with(
                "PreIngestionWorker Completed Successfully.",
                source_data=self.source_data_instance,
                level=5,  # Replace with the expected log level
            )

    def test_start(self):
        # Mocking eligible_item_reader method to return True
        self.worker.eligible_item_reader = MagicMock(return_value=True)

        # Mocking process_started_worker method
        self.worker.process_started_worker = MagicMock()

        self.worker.start(self.source_data_instance)

        # Check if the process_started_worker method was called
        self.worker.process_started_worker.assert_called_with(self.source_data_instance)


if __name__ == "__main__":
    unittest.main()
