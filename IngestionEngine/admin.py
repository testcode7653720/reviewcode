import logging
from django.contrib import admin
from django.contrib import messages
from django.http.request import HttpRequest
from django_object_actions import DjangoObjectActions, action

from .services import SourceDataService

from .models import (
    IngestionConfiguration,
    IngestionLogs,
    MnAOperationInfo,
    SourceData,
    TargetData,
    TargetImage,
    BusinessMetadata,
    OperationalMetadata,
    SDReader,
)

logger = logging.getLogger("ingestion-engine")


class BookInline(admin.TabularInline):
    model = SourceData


class SourceDataAdmin(DjangoObjectActions, admin.ModelAdmin):
    @action(label="Ingest", description="Ingest this resource")
    def ingest_this(self, request, obj):
        logger.info(f"Ingesting:  {obj}")
        success, msg = SourceDataService().ingest(obj)
        if not success:
            messages.add_message(request, messages.ERROR, msg)
            logger.error(msg)
        else:
            messages.add_message(request, messages.INFO, msg)

    @action(label="Ingest All", description="Ingest all the hot resource")
    def ingest_all(self, request, obj):
        logger.info(f"Ingesting many: {obj}")

    def has_change_permission(self, request: HttpRequest, obj=None) -> bool:
        return True if request.user.is_superuser else False

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)

        # allowed_fields = []
        # for field_name, field in form.base_fields.items():
        #     if field_name not in allowed_fields:
        #         field.disabled = True

        return form

    list_display = (
        "__str__",
        "SourceDataID",
        "SourceDatasetType",
        "SourceDatasetName",
        "SourceDatasetPath",
        "IADSCoreStatus",
        "SourceDatasetSize",
        "SourceDatasetVersion",
        "SourceDataLocation",
        "DataProvider",
        "DataSchema",
        "DataVolume",
        "DataFrequency",
        "DataExtractionDateTime",
        "DataOwnership",
        "DataLicensing",
        "IngestionType",
        "CreatedOn",
        "CreatedBy",
        "ModifiedOn",
        "ModifiedBy",
        "IADSIngestionStatus",
        "CTPIIngestionStatus",
        "IsExcelPresent",
        "Is_deleted_from_hot_folder",
    )

    list_display_links = ("__str__",)
    change_actions = ("ingest_this",)
    changelist_actions = ("ingest_all",)

    """
    def ingestion_started_at_display(self, obj):
        if obj.ingestion_started_at:
            return obj.ingestion_started_at.strftime("%Y-%m-%d %H:%M:%S.%f")
        else:
            return "N/A"
    ingestion_started_at_display.short_description = 'Ingestion Started At (with nanoseconds)'

    def ingestion_completed_at_display(self, obj):
        if obj.ingestion_completed_at:
            return obj.ingestion_completed_at.strftime("%Y-%m-%d %H:%M:%S.%f")
        else:
            return "N/A"
    ingestion_completed_at_display.short_description = 'Ingestion Completed At (with nanoseconds)'
"""


"""class TargetDataAdmin(admin.ModelAdmin):
    def has_change_permission(self, request: HttpRequest, obj=None) -> bool:
        return True if request.user.is_superuser else False

    list_display = (
        "id",
        "__str__",
    )
    list_display_links = ("__str__",)
    # inlines = [
    #     BookInline,
    # ]"""


class TargetDataAdmin(admin.ModelAdmin):
    list_display = (
        "SourceData",
        "ID",
        "TargetDataID",
        "MnAID",
        "TargetDatasetName",
        "TargetDatasetPath",
        "TargetDataLocation",
        "DataSchema",
        "DataVolume",
        "DataStorageFormat",
        "DataRetentionPolicy",
        "DataQualityProfile",
        "DataSecurityMeasures",
        "IADSIngestionStatus",
        "CTPIIngestionStatus",
        "LocalTargetDatasetPath",
    )


class TargetImageAdmin(admin.ModelAdmin):
    list_display = (
        "ID",
        "ImageID",
        "TargetData",
        "SpatialResolution",
        "Path",
        "COGPath",
        "DownloadURL",
        "VisualizationURL",
        "BandName",
        "IsCOGGenerated",
        "LocalCOGPath",
        "LocalTiffPath",
        "is_tiff_copied_on_nas",
        "is_cog_copied_on_nas",
        "is_deleted_from_local",
    )


class BusinessMetadataAdmin(admin.ModelAdmin):
    list_display = (
        "BusinessMetadataID",
        "TargetData",
        "Extent",
        "Keywords",
        "Type",
        "EpsgCode",
        "NumPixels",
        "NumLayers",
        "HorizontalResolution",
        "Thesaurus",
        "ImagePlatforms",
        "OrganizationName",
        "CRSCoordinateReference",
        "HorizontalDatum",
        "VerticalDatum",
        "SensorType",
        "AcquisitionDate",
        "SourceName",
        "ProductType",
        "ProcessingLevel",
        "Custodian",
        "LicensingLevel",
        "Date",
        "TileID",
        "DownloadURL",
        "VisualizationURL",
        "EmailAddress",
        "ImageFormat",
        "SensorType",
        "Industries",
        "ImageType",
        "Abstract",
        "IsEditable",
    )


class IngestionLogsAdmin(admin.ModelAdmin):
    list_display = (
        "LogID",
        "Timestamp",
        "LogType",
        "WorkerName",
        "LogMessage",
        "AffectedSourceID",
        "AffectedTargetID",
        "AffectedClipID",
        "AffectedCollectionID",
        "AffectedSatTileID",
        "AffectedSatID",
        "ResourceUtilization",
        "DataVolume",
        "DataThroughput",
        "HostID",
    )


class SDReaderAdmin(admin.ModelAdmin):
    list_display = (
        "FolderName",
        "FolderPath",
        "Status",
        "TotalCapacity",
        "FreeCapacity",
        "ReservedCapacity",
        "UsedCapacity",
    )


admin.site.register(SourceData, SourceDataAdmin)
admin.site.register(TargetData, TargetDataAdmin)
admin.site.register(BusinessMetadata, BusinessMetadataAdmin)
admin.site.register(OperationalMetadata)
admin.site.register(TargetImage, TargetImageAdmin)
admin.site.register(IngestionConfiguration)
admin.site.register(IngestionLogs, IngestionLogsAdmin)
admin.site.register(MnAOperationInfo)
admin.site.register(SDReader, SDReaderAdmin)
