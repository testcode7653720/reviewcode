from rest_framework import serializers

from IngestionEngine.models import TargetData


class TargetDataSerializer(serializers.ModelSerializer):

    class Meta:
        model = TargetData
        fields = '__all__'
