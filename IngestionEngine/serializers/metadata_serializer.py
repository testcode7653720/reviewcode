from rest_framework import serializers

from IngestionEngine.models import BusinessMetadata


class BusinessMetadataSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessMetadata
        fields = "__all__"
