from django.apps import AppConfig


class IngestionengineConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'IngestionEngine'
